#!/usr/bin/env bash

echo "APP_ENV=${APP_ENV}" >> /root/.profile

if  [ "${APP_ENV}" != "dev" ] && [ "${APP_ENV}" != "prod" ] && [ "${APP_ENV}" != "local" ] && [ "${APP_ENV}" != "test" ]; then
    echo "Environment: ${APP_ENV} is not a valid environment"
    exit
fi

if  [ "${APP_ENV}" != "test" ]; then
    composer install
fi

if [ "${APP_ENV}" == "dev" ]; then
    # Remove previous remote hosts for xdebug
    sed -i '/xdebug.client_host=.*$/d' /etc/php/8.0/cli/php.ini
    sed -i '/xdebug.client_host=.*$/d' /etc/php/8.0/apache2/php.ini

    # Put current remote host for xdebug
    echo "xdebug.client_host=host.docker.internal" >> /etc/php/8.0/cli/php.ini
    echo "xdebug.client_host=host.docker.internal" >> /etc/php/8.0/apache2/php.ini
fi

# Remove previous remote hosts for xdebug
sed -i '/memory_limit=.*$/d' /etc/php/8.0/cli/php.ini
sed -i '/memory_limit=.*$/d' /etc/php/8.0/apache2/php.ini

echo "memory_limit=768M" >> /etc/php/8.0/cli/php.ini
echo "memory_limit=512M" >> /etc/php/8.0/apache2/php.ini

if [ "${APP_ENV}" == "prod" ]; then

    sed -i '/opcache.memory_consumption=.*$/d' /etc/php/8.0/cli/php.ini
    sed -i '/opcache.memory_consumption=.*$/d' /etc/php/8.0/apache2/php.ini

    sed -i '/opcache.max_accelerated_files=.*$/d' /etc/php/8.0/cli/php.ini
    sed -i '/opcache.max_accelerated_files=.*$/d' /etc/php/8.0/apache2/php.ini

    sed -i '/opcache.validate_timestamps=.*$/d' /etc/php/8.0/cli/php.ini
    sed -i '/opcache.validate_timestamps=.*$/d' /etc/php/8.0/apache2/php.ini

    sed -i '/realpath_cache_size=.*$/d' /etc/php/8.0/cli/php.ini
    sed -i '/realpath_cache_size=.*$/d' /etc/php/8.0/apache2/php.ini

    sed -i '/realpath_cache_ttl=.*$/d' /etc/php/8.0/cli/php.ini
    sed -i '/realpath_cache_ttl=.*$/d' /etc/php/8.0/apache2/php.ini

    echo "opcache.memory_consumption=256" >> /etc/php/8.0/cli/php.ini
    echo "opcache.memory_consumption=256" >> /etc/php/8.0/apache2/php.ini

    echo "opcache.max_accelerated_files=20000" >> /etc/php/8.0/cli/php.ini
    echo "opcache.max_accelerated_files=20000" >> /etc/php/8.0/apache2/php.ini

    echo "opcache.validate_timestamps=0" >> /etc/php/8.0/cli/php.ini
    echo "opcache.validate_timestamps=0" >> /etc/php/8.0/apache2/php.ini

    echo "realpath_cache_size=4096K" >> /etc/php/8.0/cli/php.ini
    echo "realpath_cache_size=4096K" >> /etc/php/8.0/apache2/php.ini

    echo "realpath_cache_ttl=600" >> /etc/php/8.0/cli/php.ini
    echo "realpath_cache_ttl=600" >> /etc/php/8.0/apache2/php.ini

    composer dump-autoload --optimize --no-dev --classmap-authoritative
fi

echo "session.save_path=/tmp" >> /etc/php/8.0/apache2/php.ini

# Clear cache
if [ "${APP_ENV}" == "prod" ]; then
    php apps/erpconnector/backend/bin/console --env=${APP_ENV} cache:clear
    php apps/erpconnector/backend/bin/console --env=${APP_ENV} cache:warmup
fi


# Change folder permissions for Symfony
setfacl -dR -m u:"www-data":rwX -m u:$(whoami):rwX /app/apps/erpconnector/backend/var
setfacl -R -m u:"www-data":rwX -m u:$(whoami):rwX /app/apps/erpconnector/backend/var

php apps/erpconnector/backend/bin/console --env=${APP_ENV} cyberclick:domain-events:rabbitmq:configure


# Run cron
/usr/sbin/cron

# Start apache
/usr/sbin/apache2ctl -D FOREGROUND
