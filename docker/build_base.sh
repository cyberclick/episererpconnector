#!/usr/bin/env bash

# Login into aws
 shellcheck disable=SC2046
#eval $(aws ecr get-login --no-include-email --region eu-west-1)

# Build image
docker build -t sl-api:8.0 -f app-base.docker . --no-cache

# Tag Image
#docker tag sl-api:8.0 175311104322.dkr.ecr.eu-west-1.amazonaws.com/sl-api:8.0

# Push image
#docker push 175311104322.dkr.ecr.eu-west-1.amazonaws.com/sl-api:8.0
