<?php

declare(strict_types=1);

namespace Cyberclick\Apps\Erpconnector\Backend\Command\Command\RabbitMq;

use Cyberclick\Shared\Infrastructure\Bus\Command\RabbitMq\RabbitMqCommandConsumer;
use Cyberclick\Shared\Infrastructure\Doctrine\DatabaseConnections;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function Lambdish\Phunctional\repeat;

final class ConsumeRabbitMqCommandsCommand extends Command
{
    protected static $defaultName = 'cyberclick:commands:rabbitmq:consume';
    private RabbitMqCommandConsumer $consumer;
    private DatabaseConnections $connections;

    public function __construct(
        RabbitMqCommandConsumer $consumer,
        DatabaseConnections $connections
    )
    {
        parent::__construct();
        $this->consumer    = $consumer;
        $this->connections = $connections;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Consume commands from the RabbitMQ')
            ->addArgument('queue', InputArgument::REQUIRED, 'Queue name')
            ->addArgument('quantity', InputArgument::REQUIRED, 'Quantity of commands to process');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $queueName       = (string)$input->getArgument('queue');
        $eventsToProcess = (int)$input->getArgument('quantity');

        repeat($this->consumer($queueName), $eventsToProcess);

        return 0;
    }

    private function consumer(string $queueName): callable
    {
        return function () use ($queueName) {
            $this->consumer->consume($queueName);

            $this->connections->clear();
        };
    }
}
