<?php

declare(strict_types=1);

namespace Cyberclick\Apps\Erpconnector\Backend\Controller\Company;

use Cyberclick\ERPConnector\Company\Application\UpdateCredit\UpdateCompanyCreditCommand;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

final class CompanyCreditPostController extends ApiController
{
    public function __invoke(Request $request): JsonResponse
    {
        $validationErrors = $this->validateRequest($request);

        return $validationErrors->count()
            ? $this->errorsResponse($validationErrors)
            : $this->updateCompanyCredit(Uuid::random()->value(), $request);
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {

        $constraint = new Assert\Collection(
            [
                'name'         => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'ERPCode'       => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'DUNScode'     => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'creditAssegurat'       => [new Assert\NotBlank(), new Assert\GreaterThanOrEqual(0)],
                'creditIntern' => [new Assert\NotBlank(), new Assert\GreaterThanOrEqual(0)],
                'notaCredit'   => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
            ]
        );

        $input = $request->request->all();

        return Validation::createValidator()->validate($input, $constraint);
    }

    private function updateCompanyCredit(string $id, Request $request): JsonResponse
    {
        $name         = $request->request->get('name');
        $erpCode         = $request->request->get('ERPCode');
        $DUNSCode         = $request->request->get('DUNScode');
        $creditAssegurat     = $request->request->getInt('creditAssegurat');
        $creditIntern       = $request->request->getInt('creditIntern');
        $notaCredit         = $request->request->get('notaCredit');

        $this->dispatch(
            new UpdateCompanyCreditCommand($id, $name, $erpCode, $DUNSCode, $creditAssegurat, $creditIntern, $notaCredit)
        );

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    protected function exceptions(): array
    {
        return [];
    }
}
