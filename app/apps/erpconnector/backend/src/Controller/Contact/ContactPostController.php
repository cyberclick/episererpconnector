<?php

declare(strict_types=1);

namespace Cyberclick\Apps\Erpconnector\Backend\Controller\Contact;

use Cyberclick\ERPConnector\Contact\Application\UpdateContact\UpdateContactCommand;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

final class ContactPostController extends ApiController
{
    public function __invoke(Request $request): JsonResponse
    {
        $validationErrors = $this->validateRequest($request);

        return $validationErrors->count()
            ? $this->errorsResponse($validationErrors)
            : $this->updateContact(Uuid::random()->value(), $request);
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {

        $constraint = new Assert\Collection(
            [
                //TODO : Assert proces in list of validated proceses (asi controlamos la entrada de procesos no definidos)
                'proces'     => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'guid' => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                "datetime"  => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                //TODO : properties not null, basicamente sin properties no hay nada que hacer
                'properties' => [new Assert\NotBlank()],
            ]
        );

        $input = $request->request->all();

        return Validation::createValidator()->validate($input, $constraint);
    }

    private function updateContact(string $id, Request $request): JsonResponse
    {
        $guid = $request->request->get("guid");
        $datetime = $request->request->get("datetime");

        $properties = $request->request->get("properties");

        $firstName     = $properties["firstName"];
        $lastName      = $properties["lastName"];
        $idContacto    = $properties["idContacto"];
        $email         = $properties["email"];
        $empresa       = $properties["empresa"];
        $fechaNac      = $properties["fechaNac"];
        $fechaUltCom   = $properties["fechaUltCom"];
        $genero        = $properties["genero"];
        $subscriptions = $properties["subscriptions"];
        $idPicUp       = $properties["idPicUp"];
        $nombreCliente = $properties["nombreCliente"];
        $sitCliente    = $properties["sitcliente"];
        $mercado       = $properties["mercado"];
        $tratamiento   = $properties["tratamiento"];
        $origenDigital = $properties["origenDigital"];
        $delegacion    = $properties["delegacion"];
        $nombreJRV     = $properties["nombreJRV"];
        $vendExt       = $properties["vendExt"];
        $vendInt       = $properties["vendInt"];
        $sectorTxt     = $properties["sectorTxt"];
        $zonaGeo       = $properties["zonaGeo"];
        $localidad     = $properties["localidad"];
        $fechaAltaCli  = $properties["fechaAltaCli"];
        $facturaPapel  = $properties["facturaPapel"];
        $catLibre1     = $properties["catLibre1"];
        $catLibre2     = $properties["catLibre2"];
        $language      = $properties["language"];
        $idPersCto     = $properties["idPersCto"];
        $sitPersCto    = $properties["sitPersCto"];
        $departamento  = $properties["departamento"];
        $fechaAltaPC   = $properties["fechaAltaPC"];
        $fechaUltVis   = $properties["fechaUltVis"];
        $nroContPC     = $properties["nroContPC"];
        $emailAlb      = $properties["emailAlb"];
        $emailAge      = $properties["emailAge"];
        $cliRefPre     = $properties["cliRefPre"];
        $consent       = $properties["consent"];
        $fechaConsent  = $properties["fechaConsent"];
        $horaConsent   = $properties["horaConsent"];
        $cellPhone     = $properties["cellPhone"];


        $this->dispatch(
            new UpdateContactCommand($id, $guid, $datetime, $firstName, $lastName, $idContacto, $email, $empresa, $fechaNac, $fechaUltCom, $genero,
                $subscriptions, $idPicUp, $nombreCliente, $sitCliente, $mercado, $tratamiento, $origenDigital,
                $delegacion, $nombreJRV, $vendExt, $vendInt, $sectorTxt, $zonaGeo, $localidad, $fechaAltaCli,
                $facturaPapel, $catLibre1, $catLibre2, $language, $idPersCto, $sitPersCto, $departamento,
                $fechaAltaPC, $fechaUltVis, $nroContPC, $emailAlb, $emailAge, $cliRefPre, $consent, $fechaConsent, $horaConsent, $cellPhone)
        );

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    protected function exceptions(): array
    {
        return [];
    }
}
