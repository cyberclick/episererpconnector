<?php

declare(strict_types=1);

namespace Cyberclick\Apps\Erpconnector\Backend\Controller;

use Cyberclick\Apps\Erpconnector\Backend\Controller\Contact\ContactPostController;
use Cyberclick\ERPConnector\Contact\Application\UpdateContact\UpdateContactCommand;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

final class UniquePostController extends AbstractController
{
    public function __invoke(Request $request): mixed
    {
        $validationErrors = $this->validateRequest($request);

       if ($validationErrors->count()){
           return new JsonResponse("Not valid proces.", HttpResponse::HTTP_BAD_REQUEST);;
       }

       switch ($request->request->get("proces")){
           case "alta_usuari":
               return $this->forward('Cyberclick\Apps\Erpconnector\Backend\Controller\Contact\ContactPostController::__invoke', [$request]);
           case "email_events":
               return $this->forward('Cyberclick\Apps\Erpconnector\Backend\Controller\EmailEvents\EmailEventsPostController::__invoke', [$request]);
           case "send_email":
               return $this->forward('Cyberclick\Apps\Erpconnector\Backend\Controller\EmailSender\EmailSenderPostController::__invoke', [$request]);

       }
       return null;
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {

        $constraint = new Assert\Collection(
            [
                //TODO : Assert proces in list of validated proceses (asi controlamos la entrada de procesos no definidos)
                'proces'         => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'guid' => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                "datetime"  => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                //TODO : properties not null, basicamente sin properties no hay nada que hacer
                'properties'    =>[new Assert\NotBlank()],

            ]
        );

        $input = $request->request->all();

        return Validation::createValidator()->validate($input, $constraint);
    }

    protected function exceptions(): array
    {
        return [];
    }
}
