<?php

namespace Cyberclick\Apps\Erpconnector\Backend\Controller\EmailSender;

use Cyberclick\ERPConnector\EmailSender\Application\SendEmail\EmailSendCommand;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

class EmailSenderPostController extends ApiController

{
    public function __invoke(Request $request): JsonResponse
    {
        $validationErrors = $this->validateRequest($request);

        return $validationErrors->count()
            ? $this->errorsResponse($validationErrors)
            : $this->sendEmail(Uuid::random()->value(),  $request);
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {

        $constraint = new Assert\Collection(
            [
                //TODO : Assert proces in list of validated proceses (asi controlamos la entrada de procesos no definidos)
                'proces'         => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'guid'       => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                "datetime"  => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                //TODO : properties not null, basicamente sin properties no hay nada que hacer
                'properties'    =>[new Assert\NotBlank()]
            ]
        );

        $input = $request->request->all();

        return Validation::createValidator()->validate($input, $constraint);
    }

    private function sendEmail(string $id, Request $request): JsonResponse
    {
        $guid = $request->request->get("guid");
        $datetime = $request->request->get("datetime");

        $properties = $request->request->get("properties");
        $message = $properties["message"];
        $contact         = $message["contact"];
        $sender        = $message["sender"];
        $sendId         = $message["sendId"];
        $replyTo      = $message["replyTo"];
        $CC     = $message["CC"];
        $BCC     = $message["BCC"];
        $contactProperties     = $properties["contactProperties"];
        $customProperties     = $properties["customProperties"];
        $templateId     = $properties["templateId"];

        $this->dispatch(
            new EmailSendCommand($id, $guid, $datetime, $contact, $sender, $sendId, $replyTo, $CC,
                                $BCC, $contactProperties, $customProperties, $templateId)
        );

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    protected function exceptions(): array
    {
        return [];
    }
}
