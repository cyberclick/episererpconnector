<?php
declare(strict_types=1);

namespace Cyberclick\Apps\Erpconnector\Backend\Controller\EmailEvents;

use Cyberclick\ERPConnector\EmailEvent\Application\GetEmailEvents\GetEmailEventsCommand;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;

class EmailEventsPostController extends ApiController
{
    public function __invoke(Request $request): JsonResponse
    {
        $validationErrors = $this->validateRequest($request);

        return $validationErrors->count()
            ? $this->errorsResponse($validationErrors)
            : $this->notifyEvent(Uuid::random()->value(),  $request);
    }

    private function validateRequest(Request $request): ConstraintViolationListInterface
    {
        /**
        $constraint = new Assert\Collection(
            [
                'fechaInicial'         => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'fechaFinal'       => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'emailType'     => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'email'         =>  [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'idContacto'       => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'guid'       => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
            ]
        );
         **/
        $constraint = new Assert\Collection(
            [
                //TODO : Assert proces in list of validated proceses (asi controlamos la entrada de procesos no definidos)
                'proces'         => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                'guid'       => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                "datetime"  => [new Assert\NotBlank(), new Assert\Length(['min' => 1, 'max' => 255])],
                //TODO : properties not null, basicamente sin properties no hay nada que hacer
                'properties'    =>[new Assert\NotBlank()]
            ]
        );

        $input = $request->request->all();

        return Validation::createValidator()->validate($input, $constraint);
    }

    private function notifyEvent(string $id, Request $request): JsonResponse
    {
        $guid = $request->request->get("guid");
        $datetime = $request->request->get("datetime");

        $properties = $request->request->get("properties");
        $fechaInicial         = $properties["fechaInicial"];
        $fechaFinal        = $properties["fechaFinal"];
        $emailType         = $properties["emailType"];
        $email      = $properties["email"];
        $idContacto     = $properties["idContacto"];

        $this->dispatch(
            new GetEmailEventsCommand($id, $guid, $datetime, $fechaInicial, $fechaFinal, $emailType, $email, $idContacto )
        );

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    protected function exceptions(): array
    {
        return [];
    }
}
