<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Symfony;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Throwable;

final class JwtAuthMiddleware
{
    private const APIKEY = "123456";
    public function onKernelRequest(RequestEvent $event): void
    {
        $shouldAuthenticate = $event->getRequest()->attributes->get('auth', false);

        if (!$shouldAuthenticate) {
            return;
        }

        $apikey = $this->getCredentials($event);

        if ($apikey === null) {
            return;
        }
        $this->authenticate($apikey, $event);
    }

    private function getCredentials(RequestEvent $event): ?string
    {
        try {
            return $event->getRequest()->headers->get('api-key');
        } catch (Throwable) {
            $event->setResponse(new JsonResponse(['error' => 'No credentials provided'], Response::HTTP_FORBIDDEN));
        }
        return null;
    }

    private function authenticate(string $apikey, RequestEvent $event): void
    {
        if ($apikey !== self::APIKEY){
            new JsonResponse("Invalid Credentials", HttpResponse::HTTP_FORBIDDEN);
        }
    }

    private function addUserDataToRequest(AuthTokenPayload $tokenPayload, RequestEvent $event): void
    {
        $event->getRequest()->attributes->set('authenticated_uid', $tokenPayload->uid());
        $event->getRequest()->attributes->set('authenticated_email', $tokenPayload->email());
        $event->getRequest()->attributes->set('authenticated_roles', $tokenPayload->roles());
    }
}
