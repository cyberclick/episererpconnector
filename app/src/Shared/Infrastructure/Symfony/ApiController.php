<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Symfony;

use Cyberclick\Shared\Domain\Bus\Command\AsyncCommand;
use Cyberclick\Shared\Domain\Bus\Command\AsyncCommandBus;
use Cyberclick\Shared\Domain\Bus\Command\Command;
use Cyberclick\Shared\Domain\Bus\Command\CommandBus;
use Cyberclick\Shared\Domain\Bus\Query\Query;
use Cyberclick\Shared\Domain\Bus\Query\QueryBus;
use Cyberclick\Shared\Domain\Bus\Query\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use function Lambdish\Phunctional\each;

abstract class ApiController
{
    public function __construct(
        private QueryBus $queryBus,
        private CommandBus $commandBus,
        private AsyncCommandBus $asyncCommandBus,
        ApiExceptionsHttpStatusCodeMapping $exceptionHandler
    )
    {
        each(
            fn(int $httpCode, string $exceptionClass) => $exceptionHandler->register($exceptionClass, $httpCode),
            $this->exceptions()
        );
    }

    abstract protected function exceptions(): array;

    protected function ask(Query $query): ?Response
    {
        return $this->queryBus->ask($query);
    }

    protected function dispatch(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }

    protected function asyncDispatch(AsyncCommand $command): void
    {
        $this->asyncCommandBus->dispatch($command);
    }

    public function errorsResponse(ConstraintViolationListInterface $violationList): JsonResponse
    {
        $errors = [];
        foreach ($violationList->getIterator() as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }
        return new JsonResponse($errors, HttpResponse::HTTP_BAD_REQUEST);
    }
}
