<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Bus\Command\RabbitMq;

use AMQPEnvelope;
use AMQPQueue;
use AMQPQueueException;
use Cyberclick\Shared\Domain\Bus\MessageTracer;
use Cyberclick\Shared\Infrastructure\Bus\Command\CommandHandlerLocator;
use Cyberclick\Shared\Infrastructure\Bus\Command\CommandJsonDeserializer;
use Cyberclick\Shared\Infrastructure\Bus\Event\RabbitMq\RabbitMqConnection;
use Cyberclick\Shared\Infrastructure\Bus\Event\RabbitMq\RabbitMqExchangeNameFormatter;
use Throwable;
use function Lambdish\Phunctional\assoc;
use function Lambdish\Phunctional\get;

final class RabbitMqCommandConsumer
{
    private const OK = 'OK';
    private const KO = 'KO';

    public function __construct(
        private RabbitMqConnection $connection,
        private CommandJsonDeserializer $deserializer,
        private CommandHandlerLocator $locator,
        private MessageTracer $tracer,
        private string $exchangeName,
        private int $maxRetries,
    )
    {
    }

    public function consume(string $queueName): void
    {
        try {
            $this->connection->queue($queueName)->consume($this->consumer());
        } catch (AMQPQueueException) {
            // We don't want to raise an error if there are no messages in the queue
        }
    }

    private function consumer(): callable
    {
        return function (AMQPEnvelope $envelope, AMQPQueue $queue) {
            $command = $this->deserializer->deserialize($envelope->getBody());
            try {
                $this->tracer->start($queue->getName());
                $this->tracer->recordSpan($command::class, $envelope->getBody(), 'command', 'consume');
                $handler = $this->locator->withCommand($command);
                $handler->__invoke($command);
                $this->tracer->stopSpan($command::class);
                $this->tracer->end(self::OK);
            } catch (Throwable $error) {
                $this->tracer->registerError($error);
                $this->tracer->stopSpan($command::class);
                $this->tracer->end(self::KO);

                $this->handleConsumptionError($envelope, $queue);
            }

            $queue->ack($envelope->getDeliveryTag());

            return false;
        };
    }

    private function handleConsumptionError(AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $this->hasBeenRedeliveredTooMuch($envelope)
            ? $this->sendToDeadLetter($envelope, $queue)
            : $this->sendToRetry($envelope, $queue);
    }

    private function hasBeenRedeliveredTooMuch(AMQPEnvelope $envelope): bool
    {
        return get('redelivery_count', $envelope->getHeaders(), 0) >= $this->maxRetries;
    }

    private function sendToDeadLetter(AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $this->sendMessageTo(RabbitMqExchangeNameFormatter::deadLetter($this->exchangeName), $envelope, $queue);
    }

    private function sendMessageTo(string $exchangeName, AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $headers = $envelope->getHeaders();

        $this->connection->exchange($exchangeName)->publish(
            $envelope->getBody(),
            $queue->getName(),
            AMQP_NOPARAM,
            [
                'message_id'       => $envelope->getMessageId(),
                'content_type'     => $envelope->getContentType(),
                'content_encoding' => $envelope->getContentEncoding(),
                'priority'         => $envelope->getPriority(),
                'headers'          => assoc($headers, 'redelivery_count', (string)(get('redelivery_count', $headers, 0) + 1)),
            ]
        );
    }

    private function sendToRetry(AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $this->sendMessageTo(RabbitMqExchangeNameFormatter::retry($this->exchangeName), $envelope, $queue);
    }
}
