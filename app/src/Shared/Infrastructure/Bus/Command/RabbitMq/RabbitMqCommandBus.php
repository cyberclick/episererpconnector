<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Bus\Command\RabbitMq;

use AMQPException;
use Cyberclick\Shared\Domain\Bus\Command\AsyncCommand;
use Cyberclick\Shared\Domain\Bus\Command\AsyncCommandBus;
use Cyberclick\Shared\Infrastructure\Bus\ApmMessageTracer;
use Cyberclick\Shared\Infrastructure\Bus\Command\CommandJsonSerializer;
use Cyberclick\Shared\Infrastructure\Bus\Event\RabbitMq\RabbitMqConnection;

final class RabbitMqCommandBus implements AsyncCommandBus
{
    public function __construct(
        private RabbitMqConnection $connection,
        private string $exchangeName,
        private ApmMessageTracer $tracer,
        //TODO: FIX FAILOVER
        /*MySqlDoctrineEventBus $failoverPublisher*/
    )
    {
    }

    public function dispatch(AsyncCommand $command): void
    {
        try {
            $this->publishCommand($command);
        } catch (AMQPException) {
            //$this->failoverPublisher->publish($command);
        }
    }

    private function publishCommand(AsyncCommand $command): void
    {
        $body       = CommandJsonSerializer::serialize($command);
        $routingKey = 'cyberclick.locator.commands.command';

        $this->tracer->recordSpan($command::class, $body, 'command', 'publish');

        $this->connection->exchange($this->exchangeName)->publish(
            $body,
            $routingKey,
            AMQP_NOPARAM,
            [
                'content_type'     => 'application/json',
                'content_encoding' => 'utf-8',
                'redelivery_count' => 0,
            ]
        );
        $this->tracer->stopSpan($command::class);
    }
}
