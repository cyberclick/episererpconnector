<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Bus\Event\MySql;

use Cyberclick\Shared\Domain\Utils;
use Cyberclick\Shared\Infrastructure\Bus\Event\DomainEventMapping;
use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use RuntimeException;
use function Lambdish\Phunctional\each;
use function Lambdish\Phunctional\map;

final class MySqlDoctrineDomainEventsConsumer
{
    private Connection $connection;
    private DomainEventMapping $eventMapping;

    public function __construct(EntityManager $entityManager, DomainEventMapping $eventMapping)
    {
        $this->eventMapping = $eventMapping;
        $this->connection   = $entityManager->getConnection();
    }

    public function consume(callable $subscribers, int $eventsToConsume): void
    {
        $events = $this->connection
            ->executeQuery("SELECT * FROM sl_domain_events ORDER BY occurred_on LIMIT $eventsToConsume")
            ->fetchAllAssociative();

        each($this->executeSubscribers($subscribers), $events);

        $ids = implode(', ', map($this->idExtractor(), $events));

        if (!empty($ids)) {
            $this->connection->executeStatement("DELETE FROM sl_domain_events WHERE id IN ($ids)");
        }
    }

    private function executeSubscribers(callable $subscribers): callable
    {
        return function (array $rawEvent) use ($subscribers): void {
            try {
                $domainEventClass = $this->eventMapping->for($rawEvent['name']);
                $domainEvent      = $domainEventClass::fromPrimitives(
                    $rawEvent['aggregate_id'],
                    Utils::jsonDecode($rawEvent['body']),
                    $rawEvent['id'],
                    $this->formatDate($rawEvent['occurred_on'])
                );

                $subscribers($domainEvent);
            } catch (RuntimeException) {
            }
        };
    }

    private function formatDate($stringDate): string
    {
        return Utils::dateToString(new DateTimeImmutable($stringDate));
    }

    private function idExtractor(): callable
    {
        return static fn(array $event): string => "'${event['id']}'";
    }
}
