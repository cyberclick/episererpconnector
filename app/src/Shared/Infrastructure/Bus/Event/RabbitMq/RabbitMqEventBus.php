<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Bus\Event\RabbitMq;

use AMQPException;
use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\EventBus;
use Cyberclick\Shared\Domain\Bus\MessageTracer;
use Cyberclick\Shared\Infrastructure\Bus\Event\DomainEventJsonSerializer;
use Cyberclick\Shared\Infrastructure\Bus\Event\MySql\MySqlDoctrineEventBus;
use function Lambdish\Phunctional\each;

final class RabbitMqEventBus implements EventBus
{
    public function __construct(
        private RabbitMqConnection $connection,
        private string $exchangeName,
        private MessageTracer $tracer,
        #private MySqlDoctrineEventBus $failoverPublisher,
    )
    {
    }

    public function publish(DomainEvent ...$events): void
    {
        each($this->publisher(), $events);
    }

    private function publisher(): callable
    {
        return function (DomainEvent $event) {
            try {
                $this->publishEvent($event);
            } catch (AMQPException) {
                #$this->failoverPublisher->publish($event);
            }
        };
    }

    private function publishEvent(DomainEvent $event): void
    {
        $body       = DomainEventJsonSerializer::serialize($event);
        $routingKey = $event::eventName();
        $messageId  = $event->eventId();
        $attributes = [
            'message_id'       => $messageId,
            'content_type'     => 'application/json',
            'content_encoding' => 'utf-8',
            'redelivery_count' => 0,
        ];

        $this->tracer->recordSpan($event::eventName(), $body, 'domain_event', 'publish');

        $this->connection->exchange($this->exchangeName)->publish(
            $body,
            $routingKey,
            AMQP_NOPARAM,
            $attributes
        );

        $this->tracer->stopSpan($event::eventName());
    }
}
