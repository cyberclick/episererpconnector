<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Bus\Event\RabbitMq;

use AMQPEnvelope;
use AMQPQueue;
use AMQPQueueException;
use Cyberclick\Shared\Domain\Bus\MessageTracer;
use Cyberclick\Shared\Domain\DomainError;
use Cyberclick\Shared\Infrastructure\Bus\Event\DomainEventJsonDeserializer;
use Throwable;
use function Lambdish\Phunctional\assoc;
use function Lambdish\Phunctional\get;


final class RabbitMqDomainEventsConsumer
{
    private const OK = 'OK';
    private const KO = 'KO';

    public function __construct(
        private RabbitMqConnection $connection,
        private DomainEventJsonDeserializer $deserializer,
        private MessageTracer $tracer,
        private string $exchangeName,
        private int $maxRetries
    )
    {
    }

    public function consume(callable $subscriber, string $queueName): void
    {
        try {
            $this->connection->queue($queueName)->consume($this->consumer($subscriber));
        } catch (AMQPQueueException) {
            // We don't want to raise an error if there are no messages in the queue
        }
    }

    private function consumer(callable $subscriber): callable
    {
        return function (AMQPEnvelope $envelope, AMQPQueue $queue) use ($subscriber) {
            $event = $this->deserializer->deserialize($envelope->getBody());

            try {
                $this->tracer->start($queue->getName());
                $this->tracer->recordSpan($event::eventName(), $envelope->getBody(), 'domain_event', 'consume');
                $subscriber($event);
                $this->tracer->stopSpan($event::eventName());
                $this->tracer->end(self::OK);
            } catch (Throwable $error) {
                /**
                $this->tracer->registerError($error);
                $this->tracer->stopSpan($event::eventName());
                $this->tracer->end(self::KO);
                 **/
                $this->handleConsumptionError($error, $envelope, $queue);
            }
            $queue->ack($envelope->getDeliveryTag());

            return false;
        };
    }

    private function handleConsumptionError(Throwable $error, AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        if ($error instanceof DomainError) {
            $this->sendToDeadLetter($envelope, $queue);
        } else {
            $this->hasBeenRedeliveredTooMuch($envelope)
                ? $this->sendToDeadLetter($envelope, $queue)
                : $this->sendToRetry($envelope, $queue);
        }
    }

    private function sendToDeadLetter(AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $this->sendMessageTo(RabbitMqExchangeNameFormatter::deadLetter($this->exchangeName), $envelope, $queue);
    }

    private function sendMessageTo(string $exchangeName, AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $headers = $envelope->getHeaders();

        $this->connection->exchange($exchangeName)->publish(
            $envelope->getBody(),
            $queue->getName(),
            AMQP_NOPARAM,
            [
                'message_id'       => $envelope->getMessageId(),
                'content_type'     => $envelope->getContentType(),
                'content_encoding' => $envelope->getContentEncoding(),
                'priority'         => $envelope->getPriority(),
                'headers'          => assoc($headers, 'redelivery_count', (string)(get('redelivery_count', $headers, 0) + 1)),
            ]
        );
    }

    private function hasBeenRedeliveredTooMuch(AMQPEnvelope $envelope): bool
    {
        return get('redelivery_count', $envelope->getHeaders(), 0) >= $this->maxRetries;
    }

    private function sendToRetry(AMQPEnvelope $envelope, AMQPQueue $queue): void
    {
        $this->sendMessageTo(RabbitMqExchangeNameFormatter::retry($this->exchangeName), $envelope, $queue);
    }
}
