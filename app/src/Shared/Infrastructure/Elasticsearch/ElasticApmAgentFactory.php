<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Elasticsearch;

use Symfony\Component\HttpKernel\Kernel;
use ZoiloMora\ElasticAPM\Configuration\CoreConfiguration;
use ZoiloMora\ElasticAPM\ElasticApmTracer;
use ZoiloMora\ElasticAPM\Pool\Memory\MemoryPoolFactory;
use ZoiloMora\ElasticAPM\Reporter\ApmServerCurlReporter;

final class ElasticApmAgentFactory
{
    public function __invoke(
        string $appName,
        string $host,
        string $port,
        string $env,
    ): ElasticApmTracer
    {
        $configuration = CoreConfiguration::create([
            'appName'          => $appName,
            'appVersion'       => '0.1',
            'frameworkName'    => 'Symfony',
            'frameworkVersion' => Kernel::VERSION,
            'environment'      => $env,
            'stacktraceLimit'  => 4,
            'metricSet'        => true,
        ]);

        $reporter = new ApmServerCurlReporter(
            sprintf('http://%s:%s', $host, $port)
        );

        $poolFactory = MemoryPoolFactory::create();

        return new ElasticApmTracer(
            $configuration,
            $reporter,
            $poolFactory
        );
    }
}
