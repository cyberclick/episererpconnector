<?php

namespace Cyberclick\Shared\Infrastructure\Hubspot;

class HubspotEpochTimeConverter
{
    public const TIMEZONE = "GMT";

    public function convertToEpochTime(string $date){
        $date = explode(' ', $date);

        return strtotime($date[0].' '.self::TIMEZONE)."000";
    }
}
