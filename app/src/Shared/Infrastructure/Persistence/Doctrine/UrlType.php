<?php

namespace Cyberclick\Shared\Infrastructure\Persistence\Doctrine;

use Cyberclick\Shared\Domain\Utils;
use Cyberclick\Shared\Domain\ValueObject\Url;
use Cyberclick\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use function Lambdish\Phunctional\last;

final class UrlType extends StringType implements DoctrineCustomType
{
    public function getName(): string
    {
        return self::customTypeName();
    }

    public static function customTypeName(): string
    {
        return Utils::toSnakeCase(str_replace('Type', '', last(explode('\\', __CLASS__))));
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $className = $this->typeClassName();

        return new $className($value);
    }

    protected function typeClassName(): string
    {
        return Url::class;
    }

    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform)
    {
        return $value ? $value->value() : null;
    }
}
