<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Persistence\Doctrine;

use Cyberclick\Shared\Domain\Utils;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType;
use function Lambdish\Phunctional\last;

class UtcDatetimeType extends DateTimeType
{
    private static DateTimeZone $utc;

    public function getName(): string
    {
        return self::customTypeName();
    }

    public static function customTypeName(): string
    {
        return Utils::toSnakeCase(str_replace('Type', '', last(explode('\\', static::class))));
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateTimeImmutable) {
            $auxiliarDate = DateTime::createFromImmutable($value);
            $auxiliarDate->setTimezone(self::getUtc());
            $value = DateTimeImmutable::createFromMutable($auxiliarDate);
        }

        return parent::convertToDatabaseValue($value, $platform);
    }

    private static function getUtc(): DateTimeZone
    {
        if(!isset(self::$utc)){
            self::$utc = new DateTimeZone('UTC');
        }
        return self::$utc;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (null === $value || $value instanceof DateTimeImmutable) {
            return $value;
        }

        $converted = DateTimeImmutable::createFromFormat(
            $platform->getDateTimeFormatString(),
            $value,
            self::getUtc()
        );

        if (!$converted) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                $platform->getDateTimeFormatString()
            );
        }

        return $converted;
    }
}
