<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Infrastructure\Persistence\Doctrine;

use Cyberclick\Shared\Domain\Criteria\AndFilter;
use Cyberclick\Shared\Domain\Criteria\Criteria;
use Cyberclick\Shared\Domain\Criteria\Filter;
use Cyberclick\Shared\Domain\Criteria\FilterField;
use Cyberclick\Shared\Domain\Criteria\FilterInterface;
use Cyberclick\Shared\Domain\Criteria\FilterOperator;
use Cyberclick\Shared\Domain\Criteria\FilterVisitorInterface;
use Cyberclick\Shared\Domain\Criteria\OrderBy;
use Cyberclick\Shared\Domain\Criteria\OrFilter;
use Doctrine\Common\Collections\Criteria as DoctrineCriteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Doctrine\Common\Collections\Expr\Expression;

final class DoctrineCriteriaConverter implements FilterVisitorInterface
{
    private Criteria $criteria;
    private array $criteriaToDoctrineFields;
    private array $hydrators;

    public function __construct(Criteria $criteria, array $criteriaToDoctrineFields, array $hydrators)
    {
        $this->criteria                 = $criteria;
        $this->criteriaToDoctrineFields = $criteriaToDoctrineFields;
        $this->hydrators                = $hydrators;
    }

    public static function convert(
        Criteria $criteria,
        array $criteriaToDoctrineFields = [],
        array $hydrators = []
    ): DoctrineCriteria
    {
        $converter = new self($criteria, $criteriaToDoctrineFields, $hydrators);

        return $converter->convertToDoctrineCriteria();
    }

    private function convertToDoctrineCriteria(): DoctrineCriteria
    {
        return new DoctrineCriteria(
            $this->buildExpression($this->criteria),
            $this->formatOrder($this->criteria),
            $this->criteria->offset(),
            $this->criteria->limit()
        );
    }

    private function buildExpression(Criteria $criteria): ?CompositeExpression
    {
        if ($criteria->hasFilters()) {
            return new CompositeExpression(
                CompositeExpression::TYPE_AND,
                array_map($this->buildComparison(), $criteria->plainFilters())
            );
        }

        return null;
    }

    private function buildComparison(): callable
    {
        return fn(FilterInterface $filter): Expression => $filter->accept($this);
    }

    private function mapFieldValue(FilterField $field)
    {
        return array_key_exists($field->value(), $this->criteriaToDoctrineFields)
            ? $this->criteriaToDoctrineFields[$field->value()]
            : $field->value();
    }

    private function existsHydratorFor($field): bool
    {
        return array_key_exists($field, $this->hydrators);
    }

    private function hydrate($field, string $value)
    {
        return $this->hydrators[$field]($value);
    }

    private function formatOrder(Criteria $criteria): ?array
    {
        if (!$criteria->hasOrder()) {
            return null;
        }

        return [$this->mapOrderBy($criteria->order()->orderBy()) => $criteria->order()->orderType()];
    }

    private function mapOrderBy(OrderBy $field)
    {
        return array_key_exists($field->value(), $this->criteriaToDoctrineFields)
            ? $this->criteriaToDoctrineFields[$field->value()]
            : $field->value();
    }

    public function visitAnd(AndFilter $filter): CompositeExpression
    {
        return new CompositeExpression(
            CompositeExpression::TYPE_AND,
            [$filter->left()->accept($this), $filter->right()->accept($this)]
        );
    }

    public function visitOr(OrFilter $filter): CompositeExpression
    {
        return new CompositeExpression(
            CompositeExpression::TYPE_OR,
            [$filter->left()->accept($this), $filter->right()->accept($this)]
        );
    }

    public function visitFilter(Filter $filter): Comparison
    {
        $field = $this->mapFieldValue($filter->field());
        $value = $this->existsHydratorFor($field)
            ? $this->hydrate($field, $filter->value()->value())
            : $filter->value()->value();

        return new Comparison($field, $this->transformOperator($filter->operator()), $value);
    }

    private function transformOperator(FilterOperator $filterOperator): string
    {
        return match ($filterOperator->value()) {
            FilterOperator::NOT_EQUAL => Comparison::NEQ,
            default => $filterOperator->value()
        };

    }
}
