<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\File;

use RuntimeException;

final class File
{
    private string $path;
    private string $dirname;
    private string $filename;
    private FileExtension $extension;

    public function __construct(string $path)
    {
        $this->path = $path;
        $this->extractExtension($path);
        $this->extractFilename($path);
        $this->extractDirname($path);
    }

    private function extractExtension(string $path): void
    {
        $this->extension = new FileExtension($path);
    }

    private function extractFilename(string $path): void
    {
        $this->filename = pathinfo($path, PATHINFO_FILENAME);
    }

    private function extractDirname(string $path): void
    {
        $this->dirname = pathinfo($path, PATHINFO_DIRNAME);
    }

    public function dirname(): string
    {
        return $this->dirname;
    }

    public function extension(): FileExtension
    {
        return $this->extension;
    }

    public function filename(): string
    {
        return $this->filename;
    }

    public function path(): string
    {
        return $this->path;
    }

    public function count(): int
    {
        $linecount = 0;
        $handle    = fopen($this->path, "rb");
        while (!feof($handle)) {
            $line = fgets($handle);
            if ($line !== false) {
                $linecount++;
            }
        }
        fclose($handle);
        return $linecount;
    }

    public function delete(): void
    {
        unlink($this->path);
    }

    public function move(string $destination): void
    {
        if (!is_dir(dirname($destination)) && !mkdir($concurrentDirectory = dirname($destination), 777, true) && !is_dir($concurrentDirectory)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        rename($this->path, $destination);
        $this->path = $destination;
        $this->extractFilename($this->path);
        $this->extractDirname($this->path);
    }
}
