<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\File;

use Cyberclick\Shared\Domain\ValueObject\StringValueObject;

class FileExtension extends StringValueObject
{
    public function __construct($path)
    {
        parent::__construct($this->extensionFromPath($path));
    }

    private function extensionFromPath(string $path): string
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }
}
