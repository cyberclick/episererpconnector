<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Email;

use Cyberclick\Shared\Domain\ValueObject\Uuid;

class EmailId extends Uuid
{}
