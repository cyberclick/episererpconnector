<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain;

interface RandomNumberGenerator
{
    public function generate(): int;
}
