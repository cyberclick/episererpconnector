<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain;

interface UuidGenerator
{
    public function generate(): string;
}
