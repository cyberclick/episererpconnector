<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Contact;

use Cyberclick\Shared\Domain\ValueObject\Uuid;

class ContactId extends Uuid
{}
