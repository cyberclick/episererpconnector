<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use function Lambdish\Phunctional\map;

abstract class Collection implements Countable, IteratorAggregate
{
    private array $items;

    final public function __construct(array $items)
    {
        Assert::arrayOf($this->type(), $items);
        $this->items = $items;
    }

    abstract protected function type(): string;

    final public static function from(array $items): static
    {
        return new static($items);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items());
    }

    protected function items(): array
    {
        return $this->items;
    }

    public function current()
    {
        return current($this->items);
    }

    public function next(): void
    {
        next($this->items);
    }

    public function valid(): bool
    {
        return array_key_exists($this->key(), $this->items);
    }

    public function key(): int|string|null
    {
        return key($this->items);
    }

    public function rewind(): void
    {
        reset($this->items);
    }

    public function walk(callable $func): void
    {
        array_walk($this->items, $func);
    }

    final public function map(callable $func): Collection
    {
        return new static(array_map($func, $this->items));
    }

    public function reduce(callable $func, $initial)
    {
        return array_reduce($this->items, $func, $initial);
    }

    public function toPrimitives(): array
    {
        return map(fn($item) => $item->toPrimitives(), $this->items);
    }

    final public function sort(callable $func): Collection
    {
        $items = $this->items;
        usort($items, $func);

        return new static($items);
    }

    public function isEmpty(): bool
    {
        return 0 === $this->count();
    }

    public function count(): int
    {
        return count($this->items());
    }

    public function equalTo(Collection $other): bool
    {
        return static::class === get_class($other) && $this->items == $other->items;
    }

    final public function jsonSerialize(): array
    {
        return $this->items;
    }

    final public function addItem($item): static
    {
        Assert::instanceOf($this->type(), $item);
        $items   = $this->items;
        $items[] = $item;

        return new static($items);
    }

    public function removeItem($item): static
    {
        Assert::instanceOf($this->type(), $item);

        return $this->filter(
            static fn($current) => $current !== $item,
        );
    }

    final public function filter(callable $func): static
    {
        return new static(array_values(array_filter($this->items, $func)));
    }
}
