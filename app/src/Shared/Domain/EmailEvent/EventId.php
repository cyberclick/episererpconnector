<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\EmailEvent;

use Cyberclick\Shared\Domain\ValueObject\Uuid;

class EventId extends Uuid
{}
