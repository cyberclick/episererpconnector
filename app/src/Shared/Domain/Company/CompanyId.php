<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Company;

use Cyberclick\Shared\Domain\ValueObject\Uuid;

class CompanyId extends Uuid
{}
