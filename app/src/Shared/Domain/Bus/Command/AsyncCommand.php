<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Bus\Command;

interface AsyncCommand extends Command
{
    public static function fromPrimitives(array $body): static;

    public function toPrimitives(): array;
}
