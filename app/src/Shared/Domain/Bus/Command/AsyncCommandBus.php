<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Bus\Command;

interface AsyncCommandBus
{
    public function dispatch(AsyncCommand $command): void;
}
