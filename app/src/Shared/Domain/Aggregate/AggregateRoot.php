<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Aggregate;

use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;
use DateTimeImmutable;

abstract class AggregateRoot
{
    protected DateTimeImmutable $createdOn;
    protected DateTimeImmutable $updatedOn;
    private array $domainEvents = [];

    public function __construct()
    {
        $this->createdOn = new DateTimeImmutable();
        $this->updatedOn = new DateTimeImmutable();
    }

    public function createdOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }

    public function updatedOn(): DateTimeImmutable
    {
        return $this->updatedOn;
    }

    final public function pullDomainEvents(): array
    {
        $domainEvents       = $this->domainEvents;
        $this->domainEvents = [];

        return $domainEvents;
    }

    final protected function record(DomainEvent $domainEvent): void
    {
        $this->domainEvents[] = $domainEvent;
    }
}
