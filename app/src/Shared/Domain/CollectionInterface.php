<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain;

interface CollectionInterface
{
    public function equalTo(Collection $other): bool;
}
