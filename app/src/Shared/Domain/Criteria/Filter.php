<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;

final class Filter implements FilterInterface
{
    private FilterField $field;
    private FilterOperator $operator;
    private FilterValueInterface $value;

    public function __construct(FilterField $field, FilterOperator $operator, FilterValueInterface $value)
    {
        $this->field    = $field;
        $this->operator = $operator;
        $this->value    = $value;
    }

    public static function fromValues(array $values): self
    {
        $filterValue = is_array($values['value']) ? new FilterArrayValue($values['value']) : new FilterValue($values['value']);
        return new self(
            new FilterField($values['field']),
            new FilterOperator($values['operator']),
            $filterValue
        );
    }

    public function field(): FilterField
    {
        return $this->field;
    }

    public function operator(): FilterOperator
    {
        return $this->operator;
    }

    public function value(): FilterValueInterface
    {
        return $this->value;
    }

    public function serialize(): string
    {
        return sprintf('%s.%s.%s', $this->field->value(), $this->operator->value(), $this->value->value());
    }

    public function accept(FilterVisitorInterface $visitor)
    {
        return $visitor->visitFilter($this);
    }
}
