<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


abstract class LogicFilter implements FilterInterface
{
    public function __construct(
        private FilterInterface $left,
        private FilterInterface $right
    )
    {
    }

    public function left(): FilterInterface
    {
        return $this->left;
    }

    public function right(): FilterInterface
    {
        return $this->right;
    }

    abstract static function type(): string;

}
