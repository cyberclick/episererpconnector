<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


use Cyberclick\Shared\Domain\DomainError;

final class InvalidFilterTypeDomainException extends DomainError
{

    public function __construct(
        private string $type
    )
    {
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('The filter type %s is not valid', $this->type);
    }
}
