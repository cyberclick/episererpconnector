<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


interface FilterInterface
{
    public function accept(FilterVisitorInterface $visitor);

    public function serialize(): string;
}
