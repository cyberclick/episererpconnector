<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


interface FilterValueInterface
{
    public function value();
}
