<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


interface FilterVisitorInterface
{
    public function visitAnd(AndFilter $filter);

    public function visitOr(OrFilter $filter);

    public function visitFilter(Filter $filter);
}
