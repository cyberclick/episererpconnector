<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


final class OrFilter extends LogicFilter
{
    private const TYPE = 'OR';

    public function accept(FilterVisitorInterface $visitor)
    {
        return $visitor->visitOr($this);
    }

    public function serialize(): string
    {
        return sprintf('(%s OR %s)', $this->left()->serialize(), $this->right()->serialize());
    }

    static function type(): string
    {
        return self::TYPE;
    }
}
