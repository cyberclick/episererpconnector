<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


use Cyberclick\Shared\Domain\Collection;

final class FilterArrayValue implements FilterValueInterface
{
    final public function __construct(private array $items)
    {
    }

    public function value(): array
    {
        return $this->items;
    }
}
