<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;


final class AndFilter extends LogicFilter
{
    private const TYPE = 'AND';

    public function accept(FilterVisitorInterface $visitor)
    {
        return $visitor->visitAnd($this);
    }

    public function serialize(): string
    {
        return sprintf('(%s AND %s)', $this->left()->serialize(), $this->right()->serialize());
    }

    public static function type(): string
    {
      return self::TYPE;
    }
}
