<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\ValueObject\StringValueObject;

final class FilterField extends StringValueObject
{
}
