<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Collection;
use function Lambdish\Phunctional\reduce;

final class Filters extends Collection
{
    public static function fromValues(array $values): self
    {
        return new self(array_map(self::buildFilters(), $values));
    }

    private static function buildFilters(): callable
    {
        return static fn(array $values) => self::filterBuilder($values);
    }

    private static function filterBuilder(array $values): FilterInterface {
        if (array_key_exists('left',$values)) {
            return self::filterLogicBuilder($values);
        }
        return Filter::fromValues($values);
    }

    private static function filterLogicBuilder(array $values): FilterInterface
    {
        $left = self::filterBuilder($values['left']);
        $right = self::filterBuilder($values['right']);

        if ($values['type'] === AndFilter::type()) {
            return new AndFilter($left, $right);
        }

        if ($values['type'] === OrFilter::type()) {
            return new OrFilter($left, $right);
        }

        throw new InvalidFilterTypeDomainException($values['type']);

    }

    public function add(Filter $filter): self
    {
        return new self(array_merge($this->items(), [$filter]));
    }

    public function filters(): array
    {
        return $this->items();
    }

    public function serialize(): string
    {
        return reduce(
            static fn(string $accumulate, Filter $filter) => sprintf('%s^%s', $accumulate, $filter->serialize()),
            $this->items(),
            ''
        );
    }

    protected function type(): string
    {
        return FilterInterface::class;
    }
}
