<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Color;

class Color
{
    private const RED   = 'red';
    private const GREEN = 'green';
    private const BLUE  = 'blue';
    private const ALPHA = 'alpha';

    protected ColorRed $red;
    protected ColorGreen $green;
    protected ColorBlue $blue;
    protected Alpha $alpha;

    public function __construct(ColorRed $red, ColorGreen $green, ColorBlue $blue, Alpha $alpha)
    {
        $this->red   = $red;
        $this->green = $green;
        $this->blue  = $blue;
        $this->alpha = $alpha;
    }

    public static function fromHex(string $hex): Color
    {
        [$red, $green, $blue] = sscanf($hex, '#%02x%02x%02x');

        return new self(
            new ColorRed($red),
            new ColorGreen($green),
            new ColorBlue($blue),
            new Alpha(Alpha::MIN_ALPHA_VALUE)
        );
    }

    public static function fromRGB(array $values): self
    {
        return new self(
            new ColorRed($values[self::RED]),
            new ColorGreen($values[self::GREEN]),
            new ColorBlue($values[self::BLUE]),
            isset($values[self::ALPHA]) ? new Alpha($values[self::ALPHA]) : new Alpha(Alpha::MAX_ALPHA_VALUE),
        );
    }

    public function red(): ColorRed
    {
        return $this->red;
    }

    public function green(): ColorGreen
    {
        return $this->green;
    }

    public function blue(): ColorBlue
    {
        return $this->blue;
    }

    public function alpha(): Alpha
    {
        return $this->alpha;
    }

    public function toHex(): string
    {
        $red   = str_pad(dechex($this->red->value()), 2, '0', STR_PAD_LEFT);
        $green = str_pad(dechex($this->green->value()), 2, '0', STR_PAD_LEFT);
        $blue  = str_pad(dechex($this->blue->value()), 2, '0', STR_PAD_LEFT);

        return '#' . $red . $green . $blue;
    }

    public function toPrimitives(): array
    {
        return [
            self::RED   => $this->red->value(),
            self::GREEN => $this->green->value(),
            self::BLUE  => $this->blue->value(),
            self::ALPHA => $this->alpha->value(),
        ];
    }
}
