<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Color;

use Cyberclick\Shared\Domain\ValueObject\IntValueObject;

class ColorValueObject extends IntValueObject
{
    public const MAX_COLOR_VALUE = 255;
    public const MIN_COLOR_VALUE = 0;

    public function __construct(int $value)
    {
        parent::__construct($value);
        $this->ensureColorInLimits($this);
    }

    private function ensureColorInLimits(ColorValueObject $colorValueObject): void
    {
        if ($colorValueObject->value > self::MAX_COLOR_VALUE) {
            throw new ColorMaxValueOvercome($colorValueObject, self::MAX_COLOR_VALUE);
        }

        if ($colorValueObject->value < self::MIN_COLOR_VALUE) {
            throw new ColorMinValueOvercome($colorValueObject, self::MIN_COLOR_VALUE);
        }
    }
}
