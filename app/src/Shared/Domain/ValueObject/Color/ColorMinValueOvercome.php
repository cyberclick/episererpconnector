<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Color;

use Cyberclick\Shared\Domain\DomainError;

final class ColorMinValueOvercome extends DomainError
{
    private ColorValueObject $value;
    private int $min;

    public function __construct(ColorValueObject $value, int $min)
    {
        $this->value = $value;
        $this->min   = $min;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('Color min value is %d, <%d> provided', $this->min, $this->value->value());
    }
}
