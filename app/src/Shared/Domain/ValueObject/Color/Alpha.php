<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Color;

use Cyberclick\Shared\Domain\ValueObject\IntValueObject;

final class Alpha extends IntValueObject
{
    public const MAX_ALPHA_VALUE = 127;
    public const MIN_ALPHA_VALUE = 0;

    public function __construct(int $value)
    {
        parent::__construct($value);
        $this->ensureAlphaInLimits($this);
    }

    private function ensureAlphaInLimits(Alpha $alpha): void
    {
        if ($alpha->value > self::MAX_ALPHA_VALUE) {
            throw new AlphaMaxValueOvercome($alpha, self::MAX_ALPHA_VALUE);
        }

        if ($alpha->value < self::MIN_ALPHA_VALUE) {
            throw new AlphaMinValueOvercome($alpha, self::MIN_ALPHA_VALUE);
        }
    }
}
