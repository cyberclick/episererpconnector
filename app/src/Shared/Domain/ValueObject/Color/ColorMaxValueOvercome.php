<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Color;

use Cyberclick\Shared\Domain\DomainError;

final class ColorMaxValueOvercome extends DomainError
{
    private ColorValueObject $value;
    private int $max;

    public function __construct(ColorValueObject $value, int $max)
    {
        $this->value = $value;
        $this->max   = $max;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('Color max value is %d, <%d> provided', $this->max, $this->value->value());
    }
}
