<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Status;

use Cyberclick\Shared\Domain\ValueObject\IntValueObject;

class Status extends IntValueObject
{
    private const ACTIVE   = 1;
    private const DISABLED = 0;
    private const DELETED  = 2;

    final public function __construct(int $value)
    {
        parent::__construct($value);
    }

    public static function ACTIVE(): static
    {
        return new static(self::ACTIVE);
    }

    public static function DELETED(): static
    {
        return new static(self::DELETED);
    }

    public static function DISABLED(): static
    {
        return new static(self::DISABLED);
    }

    public function isActive(): bool
    {
        return self::ACTIVE === $this->value;
    }

    public function isDeleted(): bool
    {
        return self::DELETED === $this->value;
    }

    public function isDisabled(): bool
    {
        return self::DISABLED === $this->value;
    }
}
