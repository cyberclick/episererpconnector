<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Money;

use Throwable;

class Money
{
    public const  MULTIPLIER = 1000;
    private const CURRENCY   = 'currency';
    private const AMOUNT     = 'amount';

    private int $amount;
    private Currency $currency;

    final public function __construct(int $amount, Currency $currency)
    {
        $this->amount   = $amount;
        $this->currency = $currency;
    }

    public static function fromMoney(Money $aMoney): static
    {
        return new static(
            $aMoney->amount(),
            $aMoney->currency()
        );
    }

    public function amount(): int
    {
        return $this->amount;
    }

    public function currency(): Currency
    {
        return $this->currency;
    }

    public static function fromString(string $strMoney): static
    {
        if ($strMoney === '') {
            throw new BadMoneyStringFormat($strMoney);
        }
        try {
            $result   = preg_match('([A-Z]{3})', $strMoney, $matches);
            if($result === false || $result === 0){
                throw new BadMoneyStringFormat($strMoney);
            }
            $currency = $matches[0];
            $amount   = trim(str_replace(',', '', str_replace($currency, '', $strMoney)));
            if (!is_numeric($amount)) {
                throw new BadMoneyStringFormat($strMoney);
            }
        } catch (Throwable) {
            throw new BadMoneyStringFormat($strMoney);
        }

        return self::create((float)$amount, new Currency($currency));
    }

    final public static function create(float $amount, Currency $currency): static
    {
        $amount *= self::MULTIPLIER;
        return new static((int)$amount, $currency);
    }

    public static function ofCurrency(Currency $aCurrency): static
    {
        return new static(0, $aCurrency);
    }

    public static function fromDatabase(int $amount, string $isoCode): static
    {
        return new static($amount, new Currency($isoCode));
    }

    public static function fromValues(array $values): static
    {
        return new static(
            $values[self::AMOUNT],
            new Currency($values[self::CURRENCY]),
        );
    }

    public function add(Money $money): static
    {
        return new static($this->amount + $money->amount, $this->currency);
    }

    public function toPrimitives(): array
    {
        return [
            self::AMOUNT   => $this->amount,
            self::CURRENCY => $this->currency->value(),
        ];
    }

    public function equals(Money $money): bool
    {
        return
            $money->currency()->equals($this->currency()) &&
            $money->amount() === $this->amount();
    }

    public function greaterThan(Money $money): bool
    {
        return $money->currency->equals($this->currency) &&
            $this->amount > $money->amount();
    }

    public function __toString(): string
    {
        return sprintf('%.2f %s', $this->floatAmount(), $this->currency()->value());
    }

    public function floatAmount(): float
    {
        return round($this->amount / self::MULTIPLIER, 2);
    }

    public function formatStandard(): string
    {
        return $this->floatAmount() . ' ' . $this->currency->value();
    }
}
