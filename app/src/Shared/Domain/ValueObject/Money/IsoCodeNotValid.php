<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Money;

use Cyberclick\Shared\Domain\DomainError;

final class IsoCodeNotValid extends DomainError
{
    public function __construct(
        private string $isoCode,
    )
    {
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('<%s> is not a valid ISO CODE', $this->isoCode);
    }
}
