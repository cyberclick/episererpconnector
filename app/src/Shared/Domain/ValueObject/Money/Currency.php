<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Money;

use Cyberclick\Shared\Domain\ValueObject\StringValueObject;

class Currency extends StringValueObject
{
    public function __construct(string $value)
    {
        $this->assertIsoCode($value);
        parent::__construct($value);
    }

    private function assertIsoCode(string $isoCode): void
    {
        if (!preg_match('/^[A-Z]{3}$/', $isoCode)) {
            throw new IsoCodeNotValid($isoCode);
        }
    }

    public function equals(Currency $currency): bool
    {
        return $currency->value() === $this->value();
    }
}
