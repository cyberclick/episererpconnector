<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Money;

use Cyberclick\Shared\Domain\DomainError;

final class BadMoneyStringFormat extends DomainError
{
    private string $data;

    public function __construct(string $data)
    {
        $this->data = $data;

        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('String Money is not well formated %s, Format: XX.XX CUR/CUR XX.XX/CURXX.XX/XX.XXCUR', $this->data);
    }
}
