<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject;

abstract class ValueObject
{
    protected mixed $value;

    public function __construct(mixed $value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        return (string)$this->value();
    }

    public function value(): mixed
    {
        return $this->value;
    }
}
