<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject;

use Cyberclick\Shared\Domain\DomainError;

final class NotValidUrl extends DomainError
{
    public function __construct(
        private string $url,
    )
    {
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('The url <%s> is not well formatted', $this->url);
    }
}
