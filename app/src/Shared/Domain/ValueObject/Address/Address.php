<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Address;

class Address
{
    private const STREET      = 'street';
    private const CITY        = 'city';
    private const STATE       = 'state';
    private const POSTAL_CODE = 'postal_code';
    private const COUNTRY     = 'country';

    public function __construct(
        private AddressStreet $street,
        private AddressCity $city,
        private AddressState $state,
        private AddressPostalCode $postalCode,
        private AddressCountry $country,
    )
    {
    }

    public static function fromValues(array $values): static
    {
        return new static(
            new AddressStreet($values[self::STREET]),
            new AddressCity($values[self::CITY]),
            new AddressState($values[self::STATE]),
            new AddressPostalCode($values[self::POSTAL_CODE]),
            new AddressCountry($values[self::COUNTRY]),
        );
    }

    public function format(): string
    {
        return sprintf('%s, %s %s %s %s',
            $this->street, $this->city, $this->state, $this->postalCode, $this->country);
    }

    public function equalTo(Address $address): bool
    {
        if ($address->street->value() !== $this->street->value()) {
            return false;
        }
        if ($address->city->value() !== $this->city->value()) {
            return false;
        }
        if ($address->state->value() !== $this->state->value()) {
            return false;
        }
        if ($address->postalCode->value() !== $this->postalCode->value()) {
            return false;
        }
        if ($address->country->value() !== $this->country->value()) {
            return false;
        }

        return true;
    }

    public function toPrimitives(): array
    {
        return [
            self::STREET      => $this->street->value(),
            self::CITY        => $this->city->value(),
            self::STATE       => $this->state->value(),
            self::POSTAL_CODE => $this->postalCode->value(),
            self::COUNTRY     => $this->country->value()
        ];
    }

    public function street(): AddressStreet
    {
        return $this->street;
    }

    public function city(): AddressCity
    {
        return $this->city;
    }

    public function state(): AddressState
    {
        return $this->state;
    }

    public function postalCode(): AddressPostalCode
    {
        return $this->postalCode;
    }

    public function country(): AddressCountry
    {
        return $this->country;
    }
}
