<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Address;

use Cyberclick\Shared\Domain\ValueObject\StringValueObject;

final class AddressPostalCode extends StringValueObject
{

}
