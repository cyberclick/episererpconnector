<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject;

use Cyberclick\Shared\Domain\File\FileExtension;

class Url extends StringValueObject
{
    protected FileExtension $extension;

    public function __construct(string $value)
    {
        parent::__construct($value);

        $this->ensureIsValidUrl($value);
    }

    private function ensureIsValidUrl(string $url): void
    {
        $path      = parse_url($url, PHP_URL_PATH);
        $parsedUrl = $url;
        if ($path !== null) {
            $encodedPath = array_map('urlencode', explode('/', $path));
            $parsedUrl   = str_replace($path, implode('/', $encodedPath), $url);
        }

        if (false === filter_var($parsedUrl, FILTER_VALIDATE_URL)) {
            throw new NotValidUrl($url);
        }
    }

    public function extension(): FileExtension
    {
        if (!isset($this->extension)) {
            $this->initialize();
        }
        return $this->extension;
    }

    private function initialize(): void
    {
        $this->extension = new FileExtension($this->value);
    }
}
