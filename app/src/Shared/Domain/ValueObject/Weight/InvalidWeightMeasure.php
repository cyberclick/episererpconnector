<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\DomainError;

final class InvalidWeightMeasure extends DomainError
{

    public function __construct(private int $value)
    {
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('Weight measure should be a positive integer <%d>', $this->value);
    }
}
