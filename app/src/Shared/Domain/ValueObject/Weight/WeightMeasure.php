<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\ValueObject\IntValueObject;

final class WeightMeasure extends IntValueObject
{
    private const MIN_VALUE = 0;

    public function __construct(int $value)
    {
        parent::__construct($value);

        $this->ensureValidWeightMeasure($value);
    }

    private function ensureValidWeightMeasure(int $value): void
    {
        if ($value < self::MIN_VALUE) {
            throw new InvalidWeightMeasure($value);
        }
    }
}
