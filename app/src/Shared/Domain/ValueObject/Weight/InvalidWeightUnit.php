<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\DomainError;

final class InvalidWeightUnit extends DomainError
{

    public function __construct(private string $value)
    {
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('Invalid Weight unit <%s>', $this->value);
    }
}
