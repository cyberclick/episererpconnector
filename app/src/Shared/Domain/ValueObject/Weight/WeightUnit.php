<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\ValueObject\StringValueObject;

final class WeightUnit extends StringValueObject
{
    public const SUPPORTED_UNITS = [
        self::LB,
        self::OZ,
        self::G,
        self::KG,
    ];

    private const LB = 'lb';
    private const OZ = 'oz';
    private const G  = 'g';
    public const  KG = 'kg';

    public function __construct(string $value)
    {
        parent::__construct($value);

        $this->ensureSupportedUnit($value);
    }

    private function ensureSupportedUnit(string $value): void
    {
        if (!in_array($value, self::SUPPORTED_UNITS)) {
            throw new InvalidWeightUnit($value);
        }
    }
}
