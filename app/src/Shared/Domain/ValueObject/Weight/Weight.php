<?php

declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Weight;

class Weight
{
    public const  MULTIPLIER = 1000;
    private const MEASURE    = 'measure';
    private const UNIT       = 'unit';

    final public function __construct(
        private WeightMeasure $measure,
        private WeightUnit $unit
    )
    {
    }

    public static function create(float $measure, WeightUnit $unit): static
    {
        $measure *= self::MULTIPLIER;
        return new static(new WeightMeasure((int)$measure), $unit);
    }

    public static function fromDatabase(int $quantity, string $unit): static
    {
        return new static(new WeightMeasure($quantity), new WeightUnit($unit));
    }

    public static function fromValues(array $values): static
    {
        return new static(
            new WeightMeasure($values[self::MEASURE]),
            new WeightUnit($values[self::UNIT]),
        );
    }

    public function equals(?Weight $weight): bool
    {
        if ($weight === null) {
            return false;
        }
        if ($weight?->measure->value() !== $this->measure->value() ||
            $weight?->unit->value() !== $this->unit->value()) {
            return false;
        }

        return true;
    }

    public function greaterThan(?Weight $weight): bool
    {
        if ($weight === null) {
            return true;
        }
        if ($this->measure->value() > $weight->measure()->value()) {
            return true;
        }

        return false;
    }

    public function measure(): WeightMeasure
    {
        return $this->measure;
    }

    public function lowerThan(?Weight $weight): bool
    {
        if ($weight === null) {
            return true;
        }
        if ($this->measure->value() < $weight->measure()->value()) {
            return true;
        }

        return false;
    }

    public function unit(): WeightUnit
    {
        return $this->unit;
    }

    public function toPrimitives(): array
    {
        return [
            self::MEASURE => $this->measure->value(),
            self::UNIT    => $this->unit->value(),
        ];
    }

    public function formatStandard(): string
    {
        return $this->floatAmount() . ' ' . $this->unit->value();
    }

    public function floatAmount(): float
    {
        return $this->measure->value() / self::MULTIPLIER;
    }
}
