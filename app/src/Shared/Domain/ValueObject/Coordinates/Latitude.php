<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Coordinates;

final class Latitude extends Coordinate
{
    private const MAX = 90;
    private const MIN = -90;

    public static function fromString(string $value): Latitude
    {
        $val = (float)$value;
        if ($val > self::MAX || $val < self::MIN) {
            throw new InvalidCoordinateValue($val);
        }
        return self::create($val);
    }
}
