<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Coordinates;

class Coordinates
{
    private const LATITUDE  = 'latitude';
    private const LONGITUDE = 'longitude';

    public function __construct(
        protected ?Latitude $latitude,
        protected ?Longitude $longitude,
    )
    {
    }

    public static function fromString(string $lat, string $lon): static
    {
        return new static(
            Latitude::fromString($lat),
            Longitude::fromString($lon)
        );
    }

    public static function none():static
    {
        return new static(
            null,
            null,
        );
    }

    public static function fromValues(array $values): static
    {
        return new static(
            Latitude::fromString($values[self::LATITUDE]),
            Longitude::fromString($values[self::LONGITUDE])
        );
    }

    public function toPrimitives(): array
    {
        return [
            self::LATITUDE  => (string)$this->latitude,
            self::LONGITUDE => (string)$this->longitude,
        ];
    }

    public function latitude(): ?Latitude
    {
        return $this->latitude;
    }

    public function longitude(): ?Longitude
    {
        return $this->longitude;
    }

    public function equalTo(?Coordinates $coordinates): bool
    {
        if (null === $coordinates) {
            return false;
        }
        if ($coordinates->latitude?->value() !== $this->latitude?->value()) {
            return false;
        }
        if ($coordinates->longitude?->value() !== $this->longitude?->value()) {
            return false;
        }

        return true;
    }
}
