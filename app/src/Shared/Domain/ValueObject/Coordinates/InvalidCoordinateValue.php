<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Coordinates;

use Cyberclick\Shared\Domain\DomainError;

final class InvalidCoordinateValue extends DomainError
{
    public function __construct(
        private float $value,
    )
    {
        parent::__construct();
    }

    protected function errorMessage(): string
    {
        return sprintf('<%s> is not a valid coordinate value', $this->value);
    }
}
