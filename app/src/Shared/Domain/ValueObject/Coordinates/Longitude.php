<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Coordinates;

final class Longitude extends Coordinate
{
    private const MAX = 180;
    private const MIN = -180;

    public static function fromString(string $value): Longitude
    {
        $val = (float)$value;
        if ($val > self::MAX || $val < self::MIN) {
            throw new InvalidCoordinateValue($val);
        }
        return self::create($val);
    }
}
