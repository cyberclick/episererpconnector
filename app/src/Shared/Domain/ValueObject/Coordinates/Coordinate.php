<?php
declare(strict_types=1);

namespace Cyberclick\Shared\Domain\ValueObject\Coordinates;

use Cyberclick\Shared\Domain\ValueObject\FloatValueObject;
use Cyberclick\Shared\Domain\ValueObject\IntValueObject;

class Coordinate extends FloatValueObject
{
    public static function create(float $value): static
    {
        return new static($value);
    }

    public function format(): string
    {
        return (string)$this->value;
    }
}
