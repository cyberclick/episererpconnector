<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Domain\Token;

final class HubspotAuthToken
{
    public const ACCESS_TOKEN_FIELD  = 'access_token';
    public const REFRESH_TOKEN_FIELD = 'refresh_token';
    public const EXPIRES_FIELD       = 'expires_in';
    public const TTL_TOKEN           = '7200';

    public function __construct(
    )
    {
    }
}
