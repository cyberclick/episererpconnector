<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Domain\HubspotCompany;

final class HubspotCompanyFields
{
    /** todo: mapear constantes con nombres de propiedades en Hubspot. */
    public const COMPANY_NAME = "name";
    public const COMPANY_ERP_CODE = "codigo_erp";
    public const COMPANY_DUNS_CODE = "codigo_duns";
    public const COMPANY_CREDIT_ASSEGURAT = "credit_assegurat";
    public const COMPANY_CREDIT_INTERN = "credit_intern";
    public const COMPANY_NOTA_CREDIT = "nota_credit";

}
