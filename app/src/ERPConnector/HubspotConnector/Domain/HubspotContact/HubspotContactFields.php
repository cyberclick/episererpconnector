<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Domain\HubspotContact;

final class HubspotContactFields
{
    /** todo: mapear constantes con nombres de propiedades en Hubspot. */
    public const CONTACT_FIRSTNAME     = "firstname";
    public const CONTACT_LASTNAME      = "lastname";
    public const CONTACT_IDCONTACTO    = "id_contacto";
    public const CONTACT_EMAIL         = "email";
    public const CONTACT_EMPRESA       = "company";
    public const CONTACT_FECHANAC      = "date_of_birth";
    public const CONTACT_FECHAULTCOM   = "fecha_ult_com";
    public const CONTACT_GENERO        = "gender";
    public const CONTACT_SUBSCRIPTIONS = "subscriptions";
    public const CONTACT_IDPICUP       = "id_picup";
    public const CONTACT_NOMBRECLIENTE = "client_name";
    public const CONTACT_SITCLIENTE    = "sit_cliente";
    public const CONTACT_MERCADO       = "mercado";
    public const CONTACT_TRATAMIENTO   = "salutation";
    public const CONTACT_ORIGENDIGITAL = "origen_digital";
    public const CONTACT_DELEGACION    = "delegacion";
    public const CONTACT_NOMBREJRV     = "nombre_jrv";
    public const CONTACT_VENDEXT       = "vend_ext";
    public const CONTACT_VENDINT       = "vend_int";
    public const CONTACT_SECTORTXT     = "sector_txt";
    public const CONTACT_ZONAGEO       = "zona_geo";
    public const CONTACT_LOCALIDAD     = "localidad";
    public const CONTACT_FECHAALTACLI  = "fecha_alta_cli";
    public const CONTACT_FACTURAPAPEL  = "factura_papel";
    public const CONTACT_CATLIBRE1     = "cat_libre_1";
    public const CONTACT_CATLIBRE2     = "cat_libre_2";
    public const CONTACT_LANGUAGE      = "hs_language";
    public const CONTACT_IDPERSCTO     = "id_pers_cto";
    public const CONTACT_SITPERSCTO    = "sit_pers_cto";
    public const CONTACT_DEPARTAMENTO  = "departamento";
    public const CONTACT_FECHAALTAPC   = "fecha_alta_pc";
    public const CONTACT_FECHAULTVIS   = "fecha_ult_vis";
    public const CONTACT_NROCONTPC     = "nro_cont_pc";
    public const CONTACT_EMAILALB      = "email_alb";
    public const CONTACT_EMAILAGE      = "email_age";
    public const CONTACT_CLIREFPRE     = "cli_ref_pre";
    public const CONTACT_CONSENT       = "consent";
    public const CONTACT_FECHACONSENT  = "fecha_consent";
    public const CONTACT_HORACONSENT   = "hora_consent";
    public const CONTACT_CELLPHONE     = "mobilephone";

}
