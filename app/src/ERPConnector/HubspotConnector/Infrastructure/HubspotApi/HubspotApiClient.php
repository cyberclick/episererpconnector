<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Infrastructure\HubspotApi;

use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class HubspotApiClient implements HubspotClient
{
    private const   BASE_URL                        = 'https://api.hubapi.com';
    protected const THROW_EXCEPTION_ON_BAD_RESPONSE = false;
    private const   RATE_LIMIT_ERROR_CODE           = 429;

    public function __construct(private HttpClientInterface $httpClient,
                                private string $apiKey)
    {
    }

    public function updateCompany(string $companyERP, array $properties): void
    {
        $url = self::BASE_URL . "/crm/v3/objects/companies/$companyERP?idProperty=codigo_erp&hapikey=".$this->apiKey;

        $request = $this->httpClient->request(
            'PATCH', $url, [
                'json'        => ['properties' => $properties]
            ]
        );

        $this->checkValidResponse($request);
    }

    public function searchContact(string $email): ?array
    {
        $url     = self::BASE_URL . "/contacts/v1/contact/email/$email/profile?hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'GET', $url,[]
        );

        if ($this->isNotFound($response)) {
            return null;
        }

        return $response->toArray();

    }

    public function createContact(array $properties): array
    {
        $url = self::BASE_URL . "/crm/v3/objects/contacts?hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'POST', $url, [
                'json'        => ['properties' => $properties]
            ]
        );

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    public function updateContact(string $email, array $properties): array
    {
        $url = self::BASE_URL . "/crm/v3/objects/contacts/$email?idProperty=email&hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'PATCH', $url, [
                'json'        => ['properties' => $properties]
            ]
        );

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    public function getEmailEvents(string $fechaInicial, string $fechaFinal, string $campaignId, string $offset, ?string $email): array
    {
        $emailSlug = "";
        if ($email){
            $emailSlug = "&recipient=".$email ;
        }
        $url = self::BASE_URL . "/email/public/v1/events?campaignId=".$campaignId."&limit=1000&offset=".$offset.$emailSlug."&startTimestamp=".$fechaInicial."&endTimestamp=".$fechaFinal."&hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'GET', $url, []);

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    public function getEmailInformation(int $campaignId): array
    {
        $url = self::BASE_URL . "/email/public/v1/campaigns/".$campaignId."?limit=200&hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'GET', $url, []);

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    public function getSubscriptionById(int $subscriptionId): array
    {
        $url = self::BASE_URL . "/email/public/v1/subscriptions?hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'GET', $url, []);

        $this->checkValidResponse($response);
        $response = $response->toArray();
        foreach ($response['subscriptionDefinitions'] as $subscription) {
            if ($subscription["id"] === $subscriptionId){
                return $subscription;
            }
        }
        return ["error" => "subscriptionId ".$subscriptionId."not found"];
    }

    public function getSubscriptionsTimeline(string $fechaInicial, string $fechaFinal, string $offset): array
    {
        $url = self::BASE_URL . "/email/public/v1/subscriptions/timeline?limit=1000&offset=".$offset."&startTimestamp=".$fechaInicial."&endTimestamp=".$fechaFinal."&hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'GET', $url, []);

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    public function sendEmail(array $properties): array
    {
        $url = self::BASE_URL . "/marketing/v3/transactional/single-email/send?hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'POST', $url, [
                'json'        => ['message' => $properties["message"], 'emailId' => $properties["emailId"]]
            ]
        );

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    public function getTransactionalEmailInformation(string $statusId): array
    {
        $url = self::BASE_URL . "/marketing/v3/email/send-statuses/".$statusId."?hapikey=".$this->apiKey;

        $response = $this->httpClient->request(
            'GET', $url, []
        );

        $this->checkValidResponse($response);

        return $response->toArray();
    }

    private function checkValidResponse(ResponseInterface $response): void
    {
        if ($response->getStatusCode() >= 300) {
            $this->waitOnRateLimitAchieved($response);

            $content = $response->toArray(self::THROW_EXCEPTION_ON_BAD_RESPONSE);

            if ($content['status'] === 'error') {

                throw new \Exception($content['message']);
            }
        }
    }

    private function waitOnRateLimitAchieved(ResponseInterface $response): void
    {
        if ($response->getStatusCode() === self::RATE_LIMIT_ERROR_CODE) {
            sleep(1);
        }
        $headers = $response->getHeaders(self::THROW_EXCEPTION_ON_BAD_RESPONSE);
        if (isset($headers['X-HubSpot-RateLimit-Remaining']) && $headers['X-HubSpot-RateLimit-Remaining'] <= 10){
            sleep(3);
        }
    }

    private function isNotFound(ResponseInterface $response): bool
    {
        $this->waitOnRateLimitAchieved($response);
        return $response->getStatusCode() === 404;

    }
}
