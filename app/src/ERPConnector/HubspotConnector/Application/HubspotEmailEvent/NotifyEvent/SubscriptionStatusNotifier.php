<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotEmailEvent\NotifyEvent;

use Cyberclick\ERPConnector\EmailEvent\Application\ReturnEmailEvent\ReturnEmailEventCommand;
use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Cyberclick\Shared\Domain\Bus\Command\CommandBus;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Hubspot\HubspotEpochTimeConverter;

final class SubscriptionStatusNotifier
{

    public function __construct(
        private CommandBus $commandBus,
        private HubspotClient $hubspotClient,
        private HubspotEpochTimeConverter $timeConverter
    )
    {
    }


    public function __invoke(EmailEventRequest $request): void
    {
        $id = Uuid::random()->value();
        $guid = $request->guid();
        $datetime = $request->datetime();
        $emailType = $request->emailType();
        $idContacto = $request->idContacto();

        $fechaInicial = $this->timeConverter->convertToEpochTime($request->fechaInicial()) ;
        $fechaFinal = $this->timeConverter->convertToEpochTime($request->fechaFinal()) ;
        $email = $request->email();
        $eventInformation = [];
        $offset = "";
        do {
            $emailEvents = $this->hubspotClient->getSubscriptionsTimeline($fechaInicial, $fechaFinal, $offset);
            foreach ($emailEvents['timeline'] as $emailEvent) {
                if ($email === $emailEvent['recipient'] || $email === ""){
                    $eventInformation["recipient"] = $emailEvent["recipient"];
                    $eventInformation["change"] = $emailEvent["changes"][0]["change"];
                    $eventInformation["subscriptionInformation"] = $this->hubspotClient->getSubscriptionById($emailEvent["changes"][0]["subscriptionId"]);
                    $seconds = $emailEvent["timestamp"] / 1000;
                    $eventInformation["timestamp"] =  date("d/m/Y H:i:s", (int) $seconds);
                    $this->commandBus->dispatch(
                        new ReturnEmailEventCommand($id, $guid, $datetime, $request->fechaInicial(), $request->fechaFinal(), $emailType, $email, $idContacto, $eventInformation)
                    );
                }
            }
            $offset = $emailEvents["offset"];
        } while ($emailEvents["hasMore"]);


    }

}
