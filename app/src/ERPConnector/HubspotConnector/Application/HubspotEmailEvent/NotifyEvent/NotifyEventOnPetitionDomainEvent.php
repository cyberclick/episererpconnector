<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotEmailEvent\NotifyEvent;

use Cyberclick\ERPConnector\EmailEvent\Domain\EmailEvent\GetEmailEventsDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class NotifyEventOnPetitionDomainEvent implements DomainEventSubscriber
{
    public function __construct(private EmailEventNotifier $emailEventNotifier, private SubscriptionStatusNotifier $subscriptionStatusNotifier)
    {
    }

    public static function subscribedTo(): array
    {
        return [GetEmailEventsDomainEvent::class];
    }

    public function __invoke(GetEmailEventsDomainEvent $event)
    {
        $request = new EmailEventRequest(
            $event->guid(),
            $event->datetime(),
            $event->fechaInicial(),
            $event->fechaFinal(),
            $event->emailType(),
            $event->email(),
            $event->idContacto(),
        );

        $this->emailEventNotifier->__invoke(
            $request
        );

        $this->subscriptionStatusNotifier->__invoke(
            $request
        );
    }
}
