<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotEmailEvent\NotifyEvent;

final class EmailEventRequest
{
    public function __construct(
        private string $guid,
        private string $datetime,
        private string $fechaInicial,
        private string $fechaFinal,
        private string $emailType,
        private string $email,
        private string $idContacto,

    )
{
}
    public function fechaInicial(): string
    {
        return $this->fechaInicial;
    }

    public function fechaFinal(): string
    {
        return $this->fechaFinal;
    }

    public function emailType(): string
    {
        return $this->emailType;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function idContacto(): string
    {
        return $this->idContacto;
    }

    public function guid(): string
    {
        return $this->guid;
    }

    public function datetime(): string
    {
        return $this->datetime;
    }

}
