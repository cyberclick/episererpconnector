<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotEmailEvent\NotifyEvent;

use Cyberclick\ERPConnector\EmailEvent\Application\ReturnEmailEvent\ReturnEmailEventCommand;
use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Cyberclick\Shared\Domain\Bus\Command\CommandBus;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Hubspot\HubspotEpochTimeConverter;

final class EmailEventNotifier
{

    public function __construct(
        private CommandBus $commandBus,
        private HubspotClient $hubspotClient,
        private HubspotEpochTimeConverter $timeConverter
    )
    {
    }


    public function __invoke(EmailEventRequest $request): void
    {
        $id = Uuid::random()->value();
        $guid = $request->guid();
        $datetime = $request->datetime();
        $emailType = $request->emailType();
        $idContacto = $request->idContacto();

        $fechaInicial = $this->timeConverter->convertToEpochTime($request->fechaInicial()) ;
        $fechaFinal = $this->timeConverter->convertToEpochTime($request->fechaFinal()) ;
        $campaignId = $request->emailType();
        $email = $request->email();
        $eventInformation = [];
        $offset = "";
        do {
            try{
                $emailEvents = $this->hubspotClient->getEmailEvents($fechaInicial, $fechaFinal, $campaignId, $offset, $email);
            }
            catch (\Exception $e){
                $eventInformation =  ["message"=> $e->getMessage()];
                $this->commandBus->dispatch(
                    new ReturnEmailEventCommand($id, $guid, $datetime, $request->fechaInicial(), $request->fechaFinal(), $emailType, $email, $idContacto, $eventInformation)
                );
            }
            foreach ($emailEvents['events'] as $emailEvent) {
                $eventInformation["recipient"] = $emailEvent["recipient"];
                $eventInformation["type"] = $emailEvent["type"];
                $seconds = $emailEvent["created"] / 1000;
                $eventInformation["created"] =  date("d/m/Y H:i:s", (int) $seconds);
                try{
                    $eventInformation["emailInformation"] = $this->hubspotClient->getEmailInformation($emailEvent["emailCampaignId"]);
                }
                catch (\Exception $e){
                    $eventInformation =  ["message"=> $e->getMessage()];
                }
                $this->commandBus->dispatch(
                    new ReturnEmailEventCommand($id, $guid, $datetime, $request->fechaInicial(), $request->fechaFinal(), $emailType, $email, $idContacto, $eventInformation)
                );
            }
            $offset = $emailEvents["offset"];
        } while ($emailEvents["hasMore"]);
    }

}
