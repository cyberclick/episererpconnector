<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotContact\UpdateContact;

use Cyberclick\ERPConnector\Contact\Application\ReturnContact\ReturnContactCommand;
use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Cyberclick\ERPConnector\HubspotConnector\Domain\HubspotContact\HubspotContactFields;
use Cyberclick\Shared\Domain\Bus\Command\CommandBus;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Hubspot\HubspotEpochTimeConverter;
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;


final class HubspotContactUpdater
{

    public function __construct(
        private CommandBus $commandBus,
        private HubspotClient $hubspotClient,
        private HubspotEpochTimeConverter $timeConverter,
        private string $environment,
    )
    {
    }

    public function __invoke(HubspotContactRequest $request): void
    {
        $id = Uuid::random()->value();
        $guid = $request->guid();
        $datetime = $request->datetime();
        $hubspotContact = $this->hubspotClient->searchContact($request->email());

        /**todo:descomentar todas las propiedades */
        $properties = [
            HubspotContactFields::CONTACT_FIRSTNAME => $request->firstName(),
            HubspotContactFields::CONTACT_LASTNAME  => $request->lastName(),
            //HubspotContactFields::CONTACT_IDCONTACTO => $request->idContacto(),
            HubspotContactFields::CONTACT_EMAIL     => $request->email(),
            //HubspotContactFields::CONTACT_EMPRESA => $request ->empresa(),
        ];

        if ($this->environment = "prod"){
            $properties = [
                HubspotContactFields::CONTACT_FIRSTNAME     => $request->firstName(),
                HubspotContactFields::CONTACT_LASTNAME      => $request->lastName(),
                HubspotContactFields::CONTACT_EMAIL         => $request->email(),
                HubspotContactFields::CONTACT_GENERO        => $request->genero(),
                HubspotContactFields::CONTACT_SUBSCRIPTIONS => $request->subscriptions(),
                HubspotContactFields::CONTACT_IDPICUP       => $request->idPicUp(),
                HubspotContactFields::CONTACT_NOMBRECLIENTE => $request->nombreCliente(),
                HubspotContactFields::CONTACT_SITCLIENTE    => $request->sitCliente(),
                HubspotContactFields::CONTACT_MERCADO       => $request->mercado(),
                HubspotContactFields::CONTACT_TRATAMIENTO   => $request->tratamiento(),
                HubspotContactFields::CONTACT_ORIGENDIGITAL => $request->origenDigital(),
                HubspotContactFields::CONTACT_DELEGACION    => $request->delegacion(),
                HubspotContactFields::CONTACT_NOMBREJRV     => $request->nombreJRV(),
                HubspotContactFields::CONTACT_VENDEXT       => $request->vendExt(),
                HubspotContactFields::CONTACT_VENDINT       => $request->vendInt(),
                HubspotContactFields::CONTACT_SECTORTXT     => $request->sectorTxt(),
                HubspotContactFields::CONTACT_ZONAGEO       => $request->zonaGeo(),
                HubspotContactFields::CONTACT_LOCALIDAD     => $request->localidad(),
                HubspotContactFields::CONTACT_FACTURAPAPEL  => $request->facturaPapel(),
                HubspotContactFields::CONTACT_CATLIBRE1     => $request->catLibre1(),
                HubspotContactFields::CONTACT_CATLIBRE2     => $request->catLibre2(),
                HubspotContactFields::CONTACT_LANGUAGE      => $request->language(),
                HubspotContactFields::CONTACT_IDPERSCTO     => $request->idPersCto(),
                HubspotContactFields::CONTACT_SITPERSCTO    => $request->sitPersCto(),
                HubspotContactFields::CONTACT_DEPARTAMENTO  => $request->departamento(),
                HubspotContactFields::CONTACT_NROCONTPC     => $request->nroContPC(),
                HubspotContactFields::CONTACT_EMAILALB      => $request->emailAlb(),
                HubspotContactFields::CONTACT_EMAILAGE      => $request->emailAge(),
                HubspotContactFields::CONTACT_CLIREFPRE     => $request->cliRefPre(),
                HubspotContactFields::CONTACT_CONSENT       => $request->consent(),
                HubspotContactFields::CONTACT_HORACONSENT   => $request->horaConsent(),
                HubspotContactFields::CONTACT_CELLPHONE     => $request->cellPhone(),
            ];
        }

        $properties = $this->addDates($request, $properties);
        if ($hubspotContact === null) {
            $properties[HubspotContactFields::CONTACT_IDCONTACTO] = $request->idContacto();
            $properties[HubspotContactFields::CONTACT_IDCONTACTO."_".strtolower($request->empresa())] =  $request->idContacto();
            try {
                $contactInformation = $this->hubspotClient->createContact($properties);
            } catch (\Exception $e){
                $contactInformation =  ["message"=> $e->getMessage()];
            }
            $this->commandBus->dispatch(
                new ReturnContactCommand($id, $guid, $datetime, $request->firstName(), $request->lastName(),
                    $request->idContacto(), $request->email(), $request->empresa(),$request->fechaNac(), $request->fechaUltCom(), $request->genero(),
                    $request->subscriptions(), $request->idPicUp(), $request->nombreCliente(), $request->sitCliente(), $request->mercado(), $request->tratamiento(),
                    $request->origenDigital(), $request->delegacion(), $request->nombreJRV(), $request->vendExt(), $request->vendInt(),
                    $request->sectorTxt(), $request->zonaGeo(), $request->localidad(), $request->fechaAltaCli(), $request->facturaPapel(), $request->catLibre1(),
                    $request->catLibre2(), $request->language(), $request->idPersCto(), $request->sitPersCto(), $request->departamento(),
                    $request->fechaAltaPC(), $request->fechaUltVis(), $request->nroContPC(), $request->emailAlb(), $request->emailAge(), $request->cliRefPre(),
                    $request->consent(), $request->fechaConsent(), $request->horaConsent(), $request->cellPhone(), $contactInformation)
            );
            return;
        }
        $contactInformation = ["Exception: Contact already exists with contact id:".$hubspotContact["properties"]["id_contacto"]["value"] ];
        if ($hubspotContact["properties"]["id_contacto"]["value"] === $request->idContacto()){
            try {
                $contactInformation = $this->hubspotClient->updateContact($request->email(), $properties);
            }catch (\Exception $e){
                $contactInformation =  ["message"=> $e->getMessage()];
            }
        }
        else if (!isset($hubspotContact["properties"]["id_contacto_".strtolower($request->empresa())])){
            $properties = [
                HubspotContactFields::CONTACT_IDCONTACTO."_".strtolower($request->empresa())=> $request->idContacto(),
            ];
            try {
                $contactInformation = $this->hubspotClient->updateContact($request->email(), $properties);
            }catch (\Exception $e){
                $contactInformation =  ["message"=> $e->getMessage()];
            }
        }

        $this->commandBus->dispatch(
            new ReturnContactCommand($id, $guid, $datetime, $request->firstName(), $request->lastName(),
                $request->idContacto(), $request->email(), $request->empresa(),$request->fechaNac(), $request->fechaUltCom(), $request->genero(),
                $request->subscriptions(), $request->idPicUp(), $request->nombreCliente(), $request->sitCliente(), $request->mercado(), $request->tratamiento(),
                $request->origenDigital(), $request->delegacion(), $request->nombreJRV(), $request->vendExt(), $request->vendInt(),
                $request->sectorTxt(), $request->zonaGeo(), $request->localidad(), $request->fechaAltaCli(), $request->facturaPapel(), $request->catLibre1(),
                $request->catLibre2(), $request->language(), $request->idPersCto(), $request->sitPersCto(), $request->departamento(),
                $request->fechaAltaPC(), $request->fechaUltVis(), $request->nroContPC(), $request->emailAlb(), $request->emailAge(), $request->cliRefPre(),
                $request->consent(), $request->fechaConsent(), $request->horaConsent(), $request->cellPhone(), $contactInformation)
        );

    }

    private function addDates(HubspotContactRequest $request, array $properties): array
    {
        if ($request->fechaNac() != ""){
            $properties[HubspotContactFields::CONTACT_FECHANAC]= $this->timeConverter->convertToEpochTime($request->fechaNac());
        }
        if ($request->fechaUltCom() != ""){
            $properties[HubspotContactFields::CONTACT_FECHAULTCOM]= $this->timeConverter->convertToEpochTime($request->fechaUltCom());
        }
        if ($request->fechaAltaCli() != ""){
            $properties[HubspotContactFields::CONTACT_FECHAALTACLI]= $this->timeConverter->convertToEpochTime($request->fechaAltaCli());
        }
        if ($request->fechaAltaPC() != ""){
            $properties[HubspotContactFields::CONTACT_FECHAALTAPC]= $this->timeConverter->convertToEpochTime($request->fechaAltaPC());
        }
        if ($request->fechaUltVis() != ""){
            $properties[HubspotContactFields::CONTACT_FECHAULTVIS]= $this->timeConverter->convertToEpochTime($request->fechaUltVis());
        }
        if ($request->fechaConsent() != ""){
            $properties[HubspotContactFields::CONTACT_FECHACONSENT]= $this->timeConverter->convertToEpochTime($request->fechaConsent());
        }

        return $properties;
    }
}

