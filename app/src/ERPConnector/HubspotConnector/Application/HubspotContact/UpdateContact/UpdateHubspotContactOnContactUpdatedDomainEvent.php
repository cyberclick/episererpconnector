<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotContact\UpdateContact;

use Cyberclick\ERPConnector\Contact\Domain\ContactUpdate\ContactUpdatedDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class UpdateHubspotContactOnContactUpdatedDomainEvent implements DomainEventSubscriber
{
    public function __construct(private HubspotContactUpdater $contactUpdater)
    {
    }

    public static function subscribedTo(): array
    {
        return [ContactUpdatedDomainEvent::class];
    }

    public function __invoke(ContactUpdatedDomainEvent $event)
    {

        $this->contactUpdater->__invoke(
            new HubspotContactRequest(
                $event->guid(),
                $event->datetime(),
                $event->firstName(),
                $event->lastName(),
                $event->idContacto(),
                $event->email(),
                $event->empresa(),
                $event->fechaNac(),
                $event->fechaUltCom(),
                $event->genero(),
                $event->subscriptions(),
                $event->idPicUp(),
                $event->nombreCliente(),
                $event->sitCliente(),
                $event->mercado(),
                $event->tratamiento(),
                $event->origenDigital(),
                $event->delegacion(),
                $event->nombreJRV(),
                $event->vendExt(),
                $event->vendInt(),
                $event->sectorTxt(),
                $event->zonaGeo(),
                $event->localidad(),
                $event->fechaAltaCli(),
                $event->facturaPapel(),
                $event->catLibre1(),
                $event->catLibre2(),
                $event->language(),
                $event->idPersCto(),
                $event->sitPersCto(),
                $event->departamento(),
                $event->fechaAltaPC(),
                $event->fechaUltVis(),
                $event->nroContPC(),
                $event->emailAlb(),
                $event->emailAge(),
                $event->cliRefPre(),
                $event->consent(),
                $event->fechaConsent(),
                $event->horaConsent(),
                $event->cellPhone()),

        );
    }
}
