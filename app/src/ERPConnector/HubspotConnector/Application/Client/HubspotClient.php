<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\Client;



use Cyberclick\ERPConnector\HubspotConnector\Domain\Token\HubspotAuthToken;

interface HubspotClient
{

    public function updateCompany(string $companyERP, array $properties): void;
    public function updateContact(string $email, array $properties): array;
    public function searchContact(string $email): ?array;
    public function createContact(array $properties): array;


}
