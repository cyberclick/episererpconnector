<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotCompany\UpdateCompanyCredit;

final class HubspotCompanyCreditRequest
{
    public function __construct(
        private string $name,
        private string $ERPCode,
        private string $DUNSCode,
        private int $creditAssegurat,
        private int $creditIntern,
        private string $notaCredit
    )
    {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function ERPCode(): string
    {
        return $this->ERPCode;
    }

    public function DUNSCode(): string
    {
        return $this->DUNSCode;
    }

    public function creditAssegurat(): int
    {
        return $this->creditAssegurat;
    }

    public function creditIntern(): int
    {
        return $this->creditIntern;
    }

    public function notaCredit(): string
    {
        return $this->notaCredit;
    }


}
