<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotCompany\UpdateCompanyCredit;

use Cyberclick\ERPConnector\Company\Domain\CompanyCreditUpdate\CompanyCreditUpdatedDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class UpdateHubspotCompanyOnCompanyCreditUpdatedDomainEvent implements DomainEventSubscriber
{
    public function __construct(private HubspotCompanyCreditUpdater $companyCreditUpdater)
    {
    }

    public static function subscribedTo(): array
    {
        return [CompanyCreditUpdatedDomainEvent::class];
    }

    public function __invoke(CompanyCreditUpdatedDomainEvent $event)
    {

        $this->companyCreditUpdater->__invoke(
            new HubspotCompanyCreditRequest(
                $event->name(),
                $event->erpCode(),
                $event->DUNSCode(),
                $event->creditAssegurat(),
                $event->creditIntern(),
                $event->notaCredit()
            ),

        );
    }
}
