<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotCompany\UpdateCompanyCredit;

use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Cyberclick\ERPConnector\HubspotConnector\Domain\HubspotCompany\HubspotCompanyFields;

final class HubspotCompanyCreditUpdater
{

    public function __construct(
        private HubspotClient $hubspotClient,
        private string $environment,
    )
    {
    }


    public function __invoke(HubspotCompanyCreditRequest $request): void
    {
        if($this->environment === "prod"){
            $properties = [
                HubspotCompanyFields::COMPANY_NAME => $request->name(),
                HubspotCompanyFields::COMPANY_ERP_CODE => $request->ERPCode(),
                HubspotCompanyFields::COMPANY_DUNS_CODE => $request->DUNSCode(),
                HubspotCompanyFields::COMPANY_CREDIT_ASSEGURAT => $request->creditAssegurat(),
                HubspotCompanyFields::COMPANY_CREDIT_INTERN => $request ->creditIntern(),
                HubspotCompanyFields::COMPANY_NOTA_CREDIT => $request ->notaCredit()
            ];

            $this->hubspotClient->updateCompany($request->ERPCode(), $properties);

            return;
        }

        /**todo:descomentar todas las propiedades */
        $properties = [
        HubspotCompanyFields::COMPANY_NAME => $request->name(),
        HubspotCompanyFields::COMPANY_ERP_CODE => $request->ERPCode(),
        //HubspotCompanyFields::COMPANY_DUNS_CODE => $request->DUNSCode(),
        //HubspotCompanyFields::COMPANY_CREDIT_ASSEGURAT => $request->creditAssegurat(),
        //HubspotCompanyFields::COMPANY_CREDIT_INTERN => $request ->creditIntern(),
        //HubspotCompanyFields::COMPANY_NOTA_CREDIT => $request ->notaCredit()
        ];

        $this->hubspotClient->updateCompany($request->ERPCode(), $properties);
    }

}
