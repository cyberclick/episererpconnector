<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotEmail\EmailSender;

use Cyberclick\ERPConnector\EmailSender\Application\ReturnEmail\ReturnEmailSentCommand;
use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Cyberclick\Shared\Domain\Bus\Command\CommandBus;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use Cyberclick\Shared\Infrastructure\Hubspot\HubspotEpochTimeConverter;

final class EmailSender
{

    public function __construct(
        private CommandBus $commandBus,
        private HubspotClient $hubspotClient,
    )
    {
    }


    public function __invoke(EmailRequest $request): void
    {
        $id = Uuid::random()->value();
        $guid = $request->guid();
        $datetime = $request->datetime();
        $message["to"] = $request->contact();
        $message["from"] = $request->sender();
        $message["sendId"] = $request->sendId();
        $message["replyTo"] = $request->replyTo();
        $message["cc"] = $request->CC();
        $message["bcc"] = $request->BCC();
        $contactProperties = $request->contactProperties();
        $customProperties = $request->customProperties();
        $templateId = $request->templateId();

        $properties["message"]= $message;
        $properties["contactProperties"] = $contactProperties;
        $properties["customProperties"] = $customProperties;
        $properties["emailId"] = (int) $templateId;
        $email = $this->hubspotClient->sendEmail($properties);

        $email = $this->hubspotClient->getTransactionalEmailInformation($email["statusId"]);

        $this->commandBus->dispatch(
            new ReturnEmailSentCommand($id, $guid, $datetime, $message["to"],
                $message["from"], $message["sendId"], $message["replyTo"], $message["cc"], $message["bcc"],
                $contactProperties, $customProperties, $templateId, $email )
        );
    }

}
