<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\HubspotConnector\Application\HubspotEmail\EmailSender;

use Cyberclick\ERPConnector\EmailSender\Domain\SendEmail\SendEmailDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class SendEmailOnPetitionDomainEvent implements DomainEventSubscriber
{
    public function __construct(private EmailSender $emailSender)
    {
    }

    public static function subscribedTo(): array
    {
        return [SendEmailDomainEvent::class];
    }

    public function __invoke(SendEmailDomainEvent $event)
    {
        $this->emailSender->__invoke(
            new EmailRequest(
                $event->guid(),
                $event->datetime(),
                $event->contact(),
                $event->sender(),
                $event->sendId(),
                $event->replyTo(),
                $event->CC(),
                $event->BCC(),
                $event->contactProperties(),
                $event->customProperties(),
                $event->templateId(),
            )
        );
    }
}
