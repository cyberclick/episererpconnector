<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\PicupConnector\Infrastructure\PicupApi;

use Cyberclick\ERPConnector\HubspotConnector\Application\Client\HubspotClient;
use Cyberclick\ERPConnector\PicupConnector\Application\Client\PicupClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class PicupApiClient implements PicupClient
{
    private const   BASE_URL                        = 'http://webservices.epidor.com/PICUP/Customer/EventHubSpot';
    protected const THROW_EXCEPTION_ON_BAD_RESPONSE = false;
    private const   RATE_LIMIT_ERROR_CODE           = 429;

    public function __construct(private HttpClientInterface $httpClient,
                                private string $apiKey)
    {
    }

    public function notifyContact(array $properties): void
    {
        $url = self::BASE_URL;

        $request = $this->httpClient->request(
            'POST', $url, [
                'headers' => ["PICUP-ApiKey" => $this->apiKey],
                'json'        => ['properties' => $properties]
            ]
        );

        $this->checkValidResponse($request);
    }

    public function notifyEvent(array $properties): void
    {
        $url = self::BASE_URL;

        $request = $this->httpClient->request(
            'POST', $url, [
                'headers' => ["PICUP-ApiKey" => $this->apiKey],
                'json'        => ['properties' => $properties]
            ]
        );

        $this->checkValidResponse($request);
    }


    private function checkValidResponse(ResponseInterface $response): void
    {
        if ($response->getStatusCode() >= 300) {
            $this->waitOnRateLimitAchieved($response);

            $content = $response->toArray(self::THROW_EXCEPTION_ON_BAD_RESPONSE);

            if ($content['status'] === 'error') {

                throw new \Exception($content['message']);
            }
        }
    }

    private function waitOnRateLimitAchieved(ResponseInterface $response): void
    {
        if ($response->getStatusCode() === self::RATE_LIMIT_ERROR_CODE) {
            sleep(1);
        }
        $headers = $response->getHeaders(self::THROW_EXCEPTION_ON_BAD_RESPONSE);
        if (isset($headers['X-HubSpot-RateLimit-Remaining']) && $headers['X-HubSpot-RateLimit-Remaining'] <= 10){
            sleep(3);
        }
    }

    private function isNotFound(ResponseInterface $response): bool
    {
        $this->waitOnRateLimitAchieved($response);
        return $response->getStatusCode() === 404;

    }
}
