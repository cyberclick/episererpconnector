<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupContact\ContactResponse;

use Cyberclick\ERPConnector\PicupConnector\Application\Client\PicupClient;

final class ContactResponseNotifier
{

    public function __construct(
        private PicupClient $picupClient,
    )
    {
    }


    public function __invoke(ContactResponseRequest $request): void
    {

        // TODO: reverse EPOCH time converter


        $returnInformation = [
            "callParameters" =>[
                "guid" => $request->guid(),
                "datetime" => $request->datetime(),
                "idContacto" => $request->idContacto(),
                "email" => $request->email()
                ],
            "eventInformation" => $request->contactInformation()
        ];
        $this->picupClient->notifyContact($returnInformation);
    }

}
