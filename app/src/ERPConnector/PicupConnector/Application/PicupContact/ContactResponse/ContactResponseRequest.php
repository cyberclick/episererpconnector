<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupContact\ContactResponse;

final class ContactResponseRequest
{
    public function __construct(
        private string $guid,
        private string $datetime,
        private string $firstName,
        private string $lastName,
        private string $idContacto,
        private string $email,
        private string $empresa,
        private ?string $fechaNac,
        private ?string $fechaUltCom,
        private string $genero,
        private ?string $subscriptions,
        private string $idPicUp,
        private string $nombreCliente,
        private string $sitCliente,
        private string $mercado,
        private string $tratamiento,
        private string $origenDigital,
        private string $delegacion,
        private string $nombreJRV,
        private ?string $vendExt,
        private ?string $vendInt,
        private string $sectorTxt,
        private string $zonaGeo,
        private string $localidad,
        private string $fechaAltaCli,
        private string $facturaPapel,
        private ?string $catLibre1,
        private ?string $catLibre2,
        private string $language,
        private string $idPersCto,
        private string $sitPersCto,
        private string $departamento,
        private string $fechaAltaPC,
        private ?string $fechaUltVis,
        private string $nroContPC,
        private string $emailAlb,
        private string $emailAge,
        private ?string $cliRefPre,
        private ?string $consent,
        private ?string $fechaConsent,
        private ?string $horaConsent,
        private ?string $cellPhone,
        private array $contactInformation

    )
{
}
    public function guid(): string
    {
        return $this->guid;
    }
    public function datetime(): string
    {
        return $this->datetime;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function idContacto(): string
    {
        return $this->idContacto;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function empresa(): string
    {
        return $this->empresa;
    }

    public function fechaNac(): ?string
    {
        return $this->fechaNac;
    }

    public function fechaUltCom(): ?string
    {
        return $this->fechaUltCom;
    }

    public function genero(): string
    {
        return $this->genero;
    }

    public function subscriptions(): ?string
    {
        return $this->subscriptions;
    }

    public function idPicUp(): string
    {
        return $this->idPicUp;
    }

    public function nombreCliente(): string
    {
        return $this->nombreCliente;
    }

    public function sitCliente(): string
    {
        return $this->sitCliente;
    }

    public function mercado(): string
    {
        return $this->mercado;
    }

    public function tratamiento(): string
    {
        return $this->tratamiento;
    }

    public function origenDigital(): string
    {
        return $this->origenDigital;
    }

    public function delegacion(): string
    {
        return $this->delegacion;
    }

    public function nombreJRV(): string
    {
        return $this->nombreJRV;
    }

    public function vendExt(): ?string
    {
        return $this->vendExt;
    }

    public function vendInt(): ?string
    {
        return $this->vendInt;
    }

    public function sectorTxt(): string
    {
        return $this->sectorTxt;
    }

    public function zonaGeo(): string
    {
        return $this->zonaGeo;
    }

    public function localidad(): string
    {
        return $this->localidad;
    }

    public function fechaAltaCli(): string
    {
        return $this->fechaAltaCli;
    }

    public function facturaPapel(): string
    {
        return $this->facturaPapel;
    }

    public function catLibre1(): ?string
    {
        return $this->catLibre1;
    }

    public function catLibre2(): ?string
    {
        return $this->catLibre2;
    }

    public function language(): string
    {
        return $this->language;
    }

    public function idPersCto(): string
    {
        return $this->idPersCto;
    }

    public function sitPersCto(): string
    {
        return $this->sitPersCto;
    }

    public function departamento(): string
    {
        return $this->departamento;
    }

    public function fechaAltaPC(): string
    {
        return $this->fechaAltaPC;
    }

    public function fechaUltVis(): ?string
    {
        return $this->fechaUltVis;
    }

    public function nroContPC(): string
    {
        return $this->nroContPC;
    }

    public function emailAlb(): string
    {
        return $this->emailAlb;
    }

    public function emailAge(): string
    {
        return $this->emailAge;
    }

    public function cliRefPre(): ?string
    {
        return $this->cliRefPre;
    }

    public function consent(): ?string
    {
        return $this->consent;
    }

    public function fechaConsent(): ?string
    {
        return $this->fechaConsent;
    }

    public function horaConsent(): ?string
    {
        return $this->horaConsent;
    }

    public function cellPhone(): ?string
    {
        return $this->cellPhone;
    }
    public function contactInformation(): array
    {
        return $this->contactInformation;
    }

}
