<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupEmailEvent\ReturnEmailEvent;

use Cyberclick\ERPConnector\PicupConnector\Application\Client\PicupClient;

final class EmailEventNotifier
{

    public function __construct(
        private PicupClient $picupClient,
    )
    {
    }


    public function __invoke(EmailEventRequest $request): void
    {

        // TODO: reverse EPOCH time converter


        $returnInformation = [
            "callParameters" =>[
                "guid" => $request->guid(),
                "datetime" => $request->datetime(),
                "emailType" => $request->emailType(),
                "idContacto" => $request->idContacto(),
                "fechaInicial" => $request->fechaInicial(),
                "fechaFinal" => $request->fechaFinal(),
                "email" => $request->email()
                ],
            "eventInformation" => $request->eventInformation()
        ];

        $this->picupClient->notifyEvent($returnInformation);
    }

}
