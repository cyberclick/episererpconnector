<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupEmailEvent\ReturnEmailEvent;

use Cyberclick\ERPConnector\EmailEvent\Domain\EmailEvent\ReturnEmailEventDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class NotifyEventOnHubspotResponseDomainEvent implements DomainEventSubscriber
{
    public function __construct(private EmailEventNotifier $emailEventNotifier)
    {
    }

    public static function subscribedTo(): array
    {
        return [ReturnEmailEventDomainEvent::class];
    }

    public function __invoke(ReturnEmailEventDomainEvent $event)
    {
        $request = new EmailEventRequest(
            $event->guid(),
            $event->datetime(),
            $event->fechaInicial(),
            $event->fechaFinal(),
            $event->emailType(),
            $event->email(),
            $event->idContacto(),
            $event->eventInformation()
        );

        $this->emailEventNotifier->__invoke(
            $request
        );
    }
}
