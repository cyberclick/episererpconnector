<?php

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupEmailSent;

use Cyberclick\ERPConnector\PicupConnector\Application\Client\PicupClient;

class EmailSentNotifier
{
    public function __construct(
        private PicupClient $picupClient,
    )
    {
    }

    public function __invoke(EmailSentRequest $request): void
    {

        // TODO: reverse EPOCH time converter


        $returnInformation = [
            "callParameters" =>[
                "guid" => $request->guid(),
                "datetime" => $request->datetime(),
                "contact" => $request->contact(),
                "sender" => $request->sender(),
                "sendId" => $request->sendId(),
                "replyTo" => $request->replyTo(),
                "cc" => $request->CC(),
                "bcc" => $request->BCC(),
                "contactProperties" => $request->contactProperties(),
                "customProperties" => $request->customProperties(),
                "templateId" => $request->templateId(),
            ],
            "emailInformation" => $request->emailInformation()
        ];

        $this->picupClient->notifyEvent($returnInformation);
    }

}
