<?php

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupEmailSent;

use Cyberclick\ERPConnector\EmailSender\Domain\SendEmail\ReturnEmailDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

class NotifyEmailSentOnHubspotResponseDomainEvent implements DomainEventSubscriber
{
    public function __construct(private EmailSentNotifier $emailSentNotifier)
    {
    }

    public static function subscribedTo(): array
    {
        return [ReturnEmailDomainEvent::class];
    }

    public function __invoke(ReturnEmailDomainEvent $event)
    {
        $request = new EmailSentRequest(
            $event->guid(),
            $event->datetime(),
            $event->contact(),
            $event->sender(),
            $event->sendId(),
            $event->replyTo(),
            $event->CC(),
            $event->BCC(),
            $event->contactProperties(),
            $event->customProperties(),
            $event->templateId(),
            $event->emailInformation()
        );

        $this->emailSentNotifier->__invoke(
            $request
        );
    }
}
