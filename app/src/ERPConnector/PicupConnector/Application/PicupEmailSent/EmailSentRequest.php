<?php

namespace Cyberclick\ERPConnector\PicupConnector\Application\PicupEmailSent;

class EmailSentRequest
{
    public function __construct(
        private string $guid,
        private string $datetime,
        private string $contact,
        private ?string $sender,
        private ?string $sendId,
        private ?array $replyTo,
        private ?array $CC,
        private ?array $BCC,
        private ?array $contactProperties,
        private ?array $customProperties,
        private string $templateId,
        private array $emailInformation,
    )
    {
    }

    public function guid(): string
    {
        return $this->guid;
    }

    public function datetime(): string
    {
        return $this->datetime;
    }

    public function contact(): string
    {
        return $this->contact;
    }

    public function sender(): ?string
    {
        return $this->sender;
    }

    public function sendId(): ?string
    {
        return $this->sendId;
    }

    public function replyTo(): ?array
    {
        return $this->replyTo;
    }

    public function CC(): ?array
    {
        return $this->CC;
    }

    public function BCC(): ?array
    {
        return $this->BCC;
    }

    public function contactProperties(): ?array
    {
        return $this->contactProperties;
    }

    public function customProperties(): ?array
    {
        return $this->customProperties;
    }

    public function templateId(): string
    {
        return $this->templateId;
    }

    public function emailInformation(): array
    {
        return $this->emailInformation;
    }
}
