<?php

declare(strict_types=1);

namespace Cyberclick\ERPConnector\PicupConnector\Application\Client;


interface PicupClient
{
    public function notifyEvent(array $properties): void;
}
