<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\Contact\Domain;


use Cyberclick\ERPConnector\Contact\Domain\ContactUpdate\ContactUpdatedDomainEvent;
use Cyberclick\ERPConnector\Contact\Domain\ContactUpdate\ReturnContactDomainEvent;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Contact\ContactId;
use Cyberclick\Shared\Domain\Aggregate\AggregateRoot;


final class Contact extends AggregateRoot


{
    private ContactFirstName $firstName;
    private ContactLastName $lastName;
    private ContactIdContacto $idContacto;
    private ContactEmail $email;
    private ContactEmpresa $empresa;
    private ?ContactFechaNac $fechaNac;
    private ?ContactFechaUltCom $fechaUltCom;
    private ContactGenero $genero;
    private ?ContactSubscriptions $subscriptions;
    private ContactIdPicUp $idPicUp;
    private ContactNombreCliente $nombreCliente;
    private ContactSitCliente $sitCliente;
    private ContactMercado $mercado;
    private ContactTratamiento $tratamiento;
    private ContactOrigenDigital $origenDigital;
    private ContactDelegacion $delegacion;
    private ContactNombreJRV $nombreJRV;
    private ?ContactVendExt $vendExt;
    private ?ContactVendInt $vendInt;
    private ContactSectorTxt $sectorTxt;
    private ContactZonaGeo $zonaGeo;
    private ContactLocalidad $localidad;
    private ContactFechaAltaCli $fechaAltaCli;
    private ContactFacturaPapel $facturaPapel;
    private ?ContactCatLibre1 $catLibre1;
    private ?ContactCatLibre2 $catLibre2;
    private ContactLanguage $language;
    private ContactIdPersCto $idPersCto;
    private ContactSitPersCto $sitPersCto;
    private ContactDepartamento $departamento;
    private ContactFechaAltaPC $fechaAltaPC;
    private ?ContactFechaUltVis $fechaUltVis;
    private ContactNroContPC $nroContPC;
    private ContactEmailAlb $emailAlb;
    private ContactEmailAge $emailAge;
    private ?ContactCliRefPre $cliRefPre;
    private ?ContactConsent $consent;
    private ?ContactFechaConsent $fechaConsent;
    private ?ContactHoraConsent $horaConsent;
    private ?ContactCellPhone $cellPhone;

    public function __construct(
        ContactFirstName     $firstName ,
        ContactLastName      $lastName ,
        ContactIdContacto     $idContacto,
        ContactEmail         $email ,
        ContactEmpresa       $empresa ,
        ?ContactFechaNac      $fechaNac ,
        ?ContactFechaUltCom   $fechaUltCom ,
        ContactGenero        $genero ,
        ?ContactSubscriptions $subscriptions ,
        ContactIdPicUp        $idPicUp,
        ContactNombreCliente $nombreCliente ,
        ContactSitCliente    $sitCliente ,
        ContactMercado       $mercado ,
        ContactTratamiento   $tratamiento ,
        ContactOrigenDigital $origenDigital ,
        ContactDelegacion    $delegacion ,
        ContactNombreJRV     $nombreJRV ,
        ?ContactVendExt       $vendExt ,
        ?ContactVendInt       $vendInt ,
        ContactSectorTxt     $sectorTxt ,
        ContactZonaGeo       $zonaGeo = null,
        ContactLocalidad     $localidad = null,
        ContactFechaAltaCli  $fechaAltaCli = null,
        ContactFacturaPapel  $facturaPapel = null,
        ?ContactCatLibre1     $catLibre1 = null,
        ?ContactCatLibre2     $catLibre2 = null,
        ContactLanguage      $language = null,
        ContactIdPersCto     $idPersCto = null,
        ContactSitPersCto    $sitPersCto = null,
        ContactDepartamento  $departamento = null,
        ContactFechaAltaPC   $fechaAltaPC = null,
        ?ContactFechaUltVis   $fechaUltVis = null,
        ContactNroContPC     $nroContPC = null,
        ContactEmailAlb      $emailAlb = null,
        ContactEmailAge      $emailAge = null,
        ?ContactCliRefPre     $cliRefPre = null,
        ?ContactConsent       $consent = null,
        ?ContactFechaConsent  $fechaConsent = null,
        ?ContactHoraConsent   $horaConsent = null,
        ?ContactCellPhone     $cellPhone = null
    )
    {
        parent::__construct();
        $this->firstName     = $firstName;
        $this->lastName      = $lastName;
        $this->idContacto    = $idContacto;
        $this->email         = $email;
        $this->empresa       = $empresa;
        $this->fechaNac      = $fechaNac;
        $this->fechaUltCom   = $fechaUltCom;
        $this->genero        = $genero;
        $this->subscriptions = $subscriptions;
        $this->idPicUp       = $idPicUp;
        $this->nombreCliente = $nombreCliente;
        $this->sitCliente    = $sitCliente;
        $this->mercado       = $mercado;
        $this->tratamiento   = $tratamiento;
        $this->origenDigital = $origenDigital;
        $this->delegacion    = $delegacion;
        $this->nombreJRV     = $nombreJRV;
        $this->vendExt       = $vendExt;
        $this->vendInt       = $vendInt;
        $this->sectorTxt     = $sectorTxt;
        $this->zonaGeo       = $zonaGeo;
        $this->localidad     = $localidad;
        $this->fechaAltaCli  = $fechaAltaCli;
        $this->facturaPapel  = $facturaPapel;
        $this->catLibre1     = $catLibre1;
        $this->catLibre2     = $catLibre2;
        $this->language      = $language;
        $this->idPersCto     = $idPersCto;
        $this->sitPersCto    = $sitPersCto;
        $this->departamento  = $departamento;
        $this->fechaAltaPC   = $fechaAltaPC;
        $this->fechaUltVis   = $fechaUltVis;
        $this->nroContPC     = $nroContPC;
        $this->emailAlb      = $emailAlb;
        $this->emailAge      = $emailAge;
        $this->cliRefPre     = $cliRefPre;
        $this->consent       = $consent;
        $this->fechaConsent  = $fechaConsent;
        $this->horaConsent   = $horaConsent;
        $this->cellPhone     = $cellPhone;
    }

    public static function create(
        ContactId            $id,
        CallGUID           $guid,
        CallDatetime       $datetime,
        ContactFirstName   $firstName,
        ContactLastName    $lastName,
        ContactIdContacto  $idContacto,
        ContactEmail       $email,
        ContactEmpresa     $empresa,
        ContactFechaNac    $fechaNac,
        ContactFechaUltCom $fechaUltCom,
        ContactGenero      $genero,
        ContactSubscriptions $subscriptions,
        ContactIdPicUp       $idPicUp,
        ContactNombreCliente $nombreCliente,
        ContactSitCliente    $sitCliente,
        ContactMercado       $mercado,
        ContactTratamiento   $tratamiento,
        ContactOrigenDigital $origenDigital,
        ContactDelegacion    $delegacion,
        ContactNombreJRV     $nombreJRV,
        ContactVendExt       $vendExt,
        ContactVendInt       $vendInt,
        ContactSectorTxt     $sectorTxt,
        ContactZonaGeo       $zonaGeo,
        ContactLocalidad     $localidad,
        ContactFechaAltaCli  $fechaAltaCli,
        ContactFacturaPapel  $facturaPapel,
        ContactCatLibre1     $catLibre1,
        ContactCatLibre2     $catLibre2,
        ContactLanguage      $language,
        ContactIdPersCto     $idPersCto,
        ContactSitPersCto    $sitPersCto,
        ContactDepartamento  $departamento,
        ContactFechaAltaPC   $fechaAltaPC,
        ContactFechaUltVis   $fechaUltVis,
        ContactNroContPC     $nroContPC,
        ContactEmailAlb      $emailAlb,
        ContactEmailAge      $emailAge,
        ContactCliRefPre     $cliRefPre,
        ContactConsent       $consent,
        ContactFechaConsent  $fechaConsent,
        ContactHoraConsent   $horaConsent,
        ContactCellPhone     $cellPhone): self
    {
        $Contact = new self($firstName, $lastName, $idContacto, $email, $empresa, $fechaNac, $fechaUltCom, $genero,
            $subscriptions, $idPicUp, $nombreCliente, $sitCliente, $mercado, $tratamiento, $origenDigital,
            $delegacion, $nombreJRV, $vendExt, $vendInt, $sectorTxt, $zonaGeo, $localidad, $fechaAltaCli,
            $facturaPapel, $catLibre1, $catLibre2, $language, $idPersCto, $sitPersCto, $departamento,
            $fechaAltaPC, $fechaUltVis, $nroContPC, $emailAlb, $emailAge, $cliRefPre, $consent, $fechaConsent, $horaConsent, $cellPhone);
        $Contact->record(
            new ContactUpdatedDomainEvent(
                $id->value(),
                $guid->value(),
                $datetime->value(),
                $firstName->value(),
                $lastName->value(),
                $idContacto->value(),
                $email->value(),
                $empresa->value(),
                $fechaNac->value(),
                $fechaUltCom->value(),
                $genero->value(),
                $subscriptions->value(),
                $idPicUp->value(),
                $nombreCliente->value(),
                $sitCliente->value(),
                $mercado->value(),
                $tratamiento->value(),
                $origenDigital->value(),
                $origenDigital->value(),
                $nombreJRV->value(),
                $vendExt->value(),
                $vendInt->value(),
                $sectorTxt->value(),
                $zonaGeo->value(),
                $localidad->value(),
                $fechaAltaCli->value(),
                $facturaPapel->value(),
                $catLibre1->value(),
                $catLibre2->value(),
                $language->value(),
                $idPersCto->value(),
                $sitPersCto->value(),
                $departamento->value(),
                $fechaAltaPC->value(),
                $fechaUltVis->value(),
                $nroContPC->value(),
                $emailAlb->value(),
                $emailAge->value(),
                $cliRefPre->value(),
                $consent->value(),
                $fechaConsent->value(),
                $horaConsent->value(),
                $cellPhone->value()
            )
        );
        return $Contact;
    }
    public static function return(
        ContactId            $id,
        CallGUID           $guid,
        CallDatetime       $datetime,
        ContactFirstName   $firstName,
        ContactLastName    $lastName,
        ContactIdContacto  $idContacto,
        ContactEmail       $email,
        ContactEmpresa     $empresa,
        ContactFechaNac    $fechaNac,
        ContactFechaUltCom $fechaUltCom,
        ContactGenero      $genero,
        ContactSubscriptions $subscriptions,
        ContactIdPicUp       $idPicUp,
        ContactNombreCliente $nombreCliente,
        ContactSitCliente    $sitCliente,
        ContactMercado       $mercado,
        ContactTratamiento   $tratamiento,
        ContactOrigenDigital $origenDigital,
        ContactDelegacion    $delegacion,
        ContactNombreJRV     $nombreJRV,
        ContactVendExt       $vendExt,
        ContactVendInt       $vendInt,
        ContactSectorTxt     $sectorTxt,
        ContactZonaGeo       $zonaGeo,
        ContactLocalidad     $localidad,
        ContactFechaAltaCli  $fechaAltaCli,
        ContactFacturaPapel  $facturaPapel,
        ContactCatLibre1     $catLibre1,
        ContactCatLibre2     $catLibre2,
        ContactLanguage      $language,
        ContactIdPersCto     $idPersCto,
        ContactSitPersCto    $sitPersCto,
        ContactDepartamento  $departamento,
        ContactFechaAltaPC   $fechaAltaPC,
        ContactFechaUltVis   $fechaUltVis,
        ContactNroContPC     $nroContPC,
        ContactEmailAlb      $emailAlb,
        ContactEmailAge      $emailAge,
        ContactCliRefPre     $cliRefPre,
        ContactConsent       $consent,
        ContactFechaConsent  $fechaConsent,
        ContactHoraConsent   $horaConsent,
        ContactCellPhone     $cellPhone,
        array $contactInformation): self
    {
        $Contact = new self($firstName, $lastName, $idContacto, $email, $empresa, $fechaNac, $fechaUltCom, $genero,
            $subscriptions, $idPicUp, $nombreCliente, $sitCliente, $mercado, $tratamiento, $origenDigital,
            $delegacion, $nombreJRV, $vendExt, $vendInt, $sectorTxt, $zonaGeo, $localidad, $fechaAltaCli,
            $facturaPapel, $catLibre1, $catLibre2, $language, $idPersCto, $sitPersCto, $departamento,
            $fechaAltaPC, $fechaUltVis, $nroContPC, $emailAlb, $emailAge, $cliRefPre, $consent, $fechaConsent, $horaConsent, $cellPhone);
        $Contact->record(
            new ReturnContactDomainEvent(
                $id->value(),
                $guid->value(),
                $datetime->value(),
                $firstName->value(),
                $lastName->value(),
                $idContacto->value(),
                $email->value(),
                $empresa->value(),
                $fechaNac->value(),
                $fechaUltCom->value(),
                $genero->value(),
                $subscriptions->value(),
                $idPicUp->value(),
                $nombreCliente->value(),
                $sitCliente->value(),
                $mercado->value(),
                $tratamiento->value(),
                $origenDigital->value(),
                $origenDigital->value(),
                $nombreJRV->value(),
                $vendExt->value(),
                $vendInt->value(),
                $sectorTxt->value(),
                $zonaGeo->value(),
                $localidad->value(),
                $fechaAltaCli->value(),
                $facturaPapel->value(),
                $catLibre1->value(),
                $catLibre2->value(),
                $language->value(),
                $idPersCto->value(),
                $sitPersCto->value(),
                $departamento->value(),
                $fechaAltaPC->value(),
                $fechaUltVis->value(),
                $nroContPC->value(),
                $emailAlb->value(),
                $emailAge->value(),
                $cliRefPre->value(),
                $consent->value(),
                $fechaConsent->value(),
                $horaConsent->value(),
                $cellPhone->value(),
                $contactInformation
            )
        );
        return $Contact;
    }

    public function firstName(): ContactFirstName
    {
        return $this->firstName;
    }

    public function lastName(): ContactLastName
    {
        return $this->lastName;
    }

    public function idContacto(): ContactIdContacto
    {
        return $this->idContacto;
    }

    public function email(): ContactEmail
    {
        return $this->email;
    }

    public function empresa(): ContactEmpresa
    {
        return $this->empresa;
    }

    public function fechaNac(): ?ContactFechaNac
    {
        return $this->fechaNac;
    }

    public function fechaUltCom(): ?ContactFechaUltCom
    {
        return $this->fechaUltCom;
    }

    public function genero(): ContactGenero
    {
        return $this->genero;
    }

    public function subscriptions(): ?ContactSubscriptions
    {
        return $this->subscriptions;
    }

    public function idPicUp(): ContactIdPicUp
    {
        return $this->idPicUp;
    }

    public function nombreCliente(): ContactNombreCliente
    {
        return $this->nombreCliente;
    }

    public function sitCliente(): ContactSitCliente
    {
        return $this->sitCliente;
    }

    public function mercado(): ContactMercado
    {
        return $this->mercado;
    }

    public function tratamiento(): ContactTratamiento
    {
        return $this->tratamiento;
    }

    public function origenDigital(): ContactOrigenDigital
    {
        return $this->origenDigital;
    }

    public function delegacion(): ContactDelegacion
    {
        return $this->delegacion;
    }

    public function nombreJRV(): ContactNombreJRV
    {
        return $this->nombreJRV;
    }

    public function vendExt(): ?ContactVendExt
    {
        return $this->vendExt;
    }

    public function vendInt(): ?ContactVendInt
    {
        return $this->vendInt;
    }

    public function sectorTxt(): ContactSectorTxt
    {
        return $this->sectorTxt;
    }

    public function zonaGeo(): ContactZonaGeo
    {
        return $this->zonaGeo;
    }

    public function localidad(): ContactLocalidad
    {
        return $this->localidad;
    }

    public function fechaAltaCli(): ContactFechaAltaCli
    {
        return $this->fechaAltaCli;
    }

    public function facturaPapel(): ContactFacturaPapel
    {
        return $this->facturaPapel;
    }

    public function catLibre1(): ?ContactCatLibre1
    {
        return $this->catLibre1;
    }

    public function catLibre2(): ?ContactCatLibre2
    {
        return $this->catLibre2;
    }

    public function language(): ContactLanguage
    {
        return $this->language;
    }

    public function idPersCto(): ContactIdPersCto
    {
        return $this->idPersCto;
    }

    public function sitPersCto(): ContactSitPersCto
    {
        return $this->sitPersCto;
    }

    public function departamento(): ContactDepartamento
    {
        return $this->departamento;
    }

    public function fechaAltaPC(): ContactFechaAltaPC
    {
        return $this->fechaAltaPC;
    }

    public function fechaUltVis(): ?ContactFechaUltVis
    {
        return $this->fechaUltVis;
    }

    public function nroContPC(): ContactNroContPC
    {
        return $this->nroContPC;
    }

    public function emailAlb(): ContactEmailAlb
    {
        return $this->emailAlb;
    }

    public function emailAge(): ContactEmailAge
    {
        return $this->emailAge;
    }

    public function cliRefPre(): ?ContactCliRefPre
    {
        return $this->cliRefPre;
    }

    public function consent(): ?ContactConsent
    {
        return $this->consent;
    }

    public function fechaConsent(): ?ContactFechaConsent
    {
        return $this->fechaConsent;
    }

    public function horaConsent(): ?ContactHoraConsent
    {
        return $this->horaConsent;
    }

    public function cellPhone(): ?ContactCellPhone
    {
        return $this->cellPhone;
    }
}
