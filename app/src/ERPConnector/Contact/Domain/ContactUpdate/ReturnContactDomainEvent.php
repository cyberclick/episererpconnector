<?php

namespace Cyberclick\ERPConnector\Contact\Domain\ContactUpdate;

use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;

final class ReturnContactDomainEvent extends DomainEvent
{
    private const VERSION = 1;
    private string $guid;
    private string $datetime;
    private string $firstName;
    private string $lastName;
    private string $idContacto;
    private string $email;
    private string $empresa;
    private ?string $fechaNac;
    private ?string $fechaUltCom;
    private string $genero;
    private ?string $subscriptions;
    private string $idPicUp;
    private string $nombreCliente;
    private string $sitCliente;
    private string $mercado;
    private string $tratamiento;
    private string $origenDigital;
    private string $delegacion;
    private string $nombreJRV;
    private ?string $vendExt;
    private ?string $vendInt;
    private string $sectorTxt;
    private string $zonaGeo;
    private string $localidad;
    private string $fechaAltaCli;
    private string $facturaPapel;
    private ?string $catLibre1;
    private ?string $catLibre2;
    private string $language;
    private string $idPersCto;
    private string $sitPersCto;
    private string $departamento;
    private string $fechaAltaPC;
    private ?string $fechaUltVis;
    private string $nroContPC;
    private string $emailAlb;
    private string $emailAge;
    private ?string $cliRefPre;
    private ?string $consent;
    private ?string $fechaConsent;
    private ?string $horaConsent;
    private ?string $cellPhone;
    private array $contactInformation;

    public function __construct(string $id,
                                string $guid,
                                string $datetime,
                                string  $firstName,
                                string  $lastName,
                                string  $idContacto,
                                string  $email,
                                string  $empresa,
                                ?string $fechaNac,
                                ?string $fechaUltCom,
                                string  $genero,
                                ?string $subscriptions,
                                string     $idPicUp,
                                string  $nombreCliente,
                                string  $sitCliente,
                                string  $mercado,
                                string  $tratamiento,
                                string  $origenDigital,
                                string  $delegacion,
                                string     $nombreJRV,
                                ?string $vendExt,
                                ?string $vendInt,
                                string  $sectorTxt,
                                string  $zonaGeo,
                                string  $localidad,
                                string  $fechaAltaCli,
                                string  $facturaPapel,
                                ?string $catLibre1,
                                ?string $catLibre2,
                                string  $language,
                                string     $idPersCto,
                                string  $sitPersCto,
                                string  $departamento,
                                string  $fechaAltaPC,
                                ?string $fechaUltVis,
                                string     $nroContPC,
                                string  $emailAlb,
                                string  $emailAge,
                                ?string $cliRefPre,
                                ?string $consent,
                                ?string $fechaConsent,
                                ?string $horaConsent,
                                ?string $cellPhone,
                                array $contactInformation,
                                string  $eventId = null,
                                string  $occurredOn = null)
    {
        parent::__construct($id,$eventId, $occurredOn);
        $this->guid          = $guid;
        $this->datetime      = $datetime;
        $this->firstName     = $firstName;
        $this->lastName      = $lastName;
        $this->idContacto    = $idContacto;
        $this->email         = $email;
        $this->empresa       = $empresa;
        $this->fechaNac      = $fechaNac;
        $this->fechaUltCom   = $fechaUltCom;
        $this->genero        = $genero;
        $this->subscriptions = $subscriptions;
        $this->idPicUp       = $idPicUp;
        $this->nombreCliente = $nombreCliente;
        $this->sitCliente    = $sitCliente;
        $this->mercado       = $mercado;
        $this->tratamiento   = $tratamiento;
        $this->origenDigital = $origenDigital;
        $this->delegacion    = $delegacion;
        $this->nombreJRV     = $nombreJRV;
        $this->vendExt       = $vendExt;
        $this->vendInt       = $vendInt;
        $this->sectorTxt     = $sectorTxt;
        $this->zonaGeo       = $zonaGeo;
        $this->localidad     = $localidad;
        $this->fechaAltaCli  = $fechaAltaCli;
        $this->facturaPapel  = $facturaPapel;
        $this->catLibre1     = $catLibre1;
        $this->catLibre2     = $catLibre2;
        $this->language      = $language;
        $this->idPersCto     = $idPersCto;
        $this->sitPersCto    = $sitPersCto;
        $this->departamento  = $departamento;
        $this->fechaAltaPC   = $fechaAltaPC;
        $this->fechaUltVis   = $fechaUltVis;
        $this->nroContPC     = $nroContPC;
        $this->emailAlb      = $emailAlb;
        $this->emailAge      = $emailAge;
        $this->cliRefPre     = $cliRefPre;
        $this->consent       = $consent;
        $this->fechaConsent  = $fechaConsent;
        $this->horaConsent   = $horaConsent;
        $this->cellPhone     = $cellPhone;
        $this->contactInformation = $contactInformation;
    }

    public static function fromPrimitives(string $aggregateId, array $body, string $eventId, string $occurredOn): DomainEvent
    {
        return new self(
            $aggregateId,
            $body["guid"],
            $body["datetime"],
            $body["firstName"],
            $body["lastName"],
            $body["idContacto"],
            $body["email"],
            $body["empresa"],
            $body["fechaNac"],
            $body["fechaUltCom"],
            $body["genero"],
            $body["subscriptions"],
            $body["idPicUp"],
            $body["nombreCliente"],
            $body["sitcliente"],
            $body["mercado"],
            $body["tratamiento"],
            $body["origenDigital"],
            $body["delegacion"],
            $body["nombreJRV"],
            $body["vendExt"],
            $body["vendInt"],
            $body["sectorTxt"],
            $body["zonaGeo"],
            $body["localidad"],
            $body["fechaAltaCli"],
            $body["facturaPapel"],
            $body["catLibre1"],
            $body["catLibre2"],
            $body["language"],
            $body["idPersCto"],
            $body["sitPersCto"],
            $body["departamento"],
            $body["fechaAltaPC"],
            $body["fechaUltVis"],
            $body["nroContPC"],
            $body["emailAlb"],
            $body["emailAge"],
            $body["cliRefPre"],
            $body["consent"],
            $body["fechaConsent"],
            $body["horaConsent"],
            $body["cellPhone"],
            $body["contactInformation"],
            $eventId,
            $occurredOn);
    }

    public static function eventName(): string
    {
        return "return.contact";
    }

    public function toPrimitives(): array
    {
        return [
            'guid' => $this->guid,
            'datetime' => $this->datetime,
            'firstName'     => $this->firstName,
            'lastName'      => $this->lastName,
            'idContacto'    => $this->idContacto,
            'email'         => $this->email,
            'empresa'       => $this->empresa,
            "fechaNac"      => $this->fechaNac,
            "fechaUltCom"   => $this->fechaUltCom,
            "genero"        => $this->genero,
            "subscriptions" => $this->subscriptions,
            "idPicUp"       => $this->idPicUp,
            "nombreCliente" => $this->nombreCliente,
            "sitcliente"    => $this->sitCliente,
            "mercado"       => $this->mercado,
            "tratamiento"   => $this->tratamiento,
            "origenDigital" => $this->origenDigital,
            "delegacion"    => $this->delegacion,
            "nombreJRV"     => $this->nombreJRV,
            "vendExt"       => $this->vendExt,
            "vendInt"       => $this->vendInt,
            "sectorTxt"     => $this->sectorTxt,
            "zonaGeo"       => $this->zonaGeo,
            "localidad"     => $this->localidad,
            "fechaAltaCli"  => $this->fechaAltaCli,
            "facturaPapel"  => $this->facturaPapel,
            "catLibre1"     => $this->catLibre1,
            "catLibre2"     => $this->catLibre2,
            "language"      => $this->language,
            "idPersCto"     => $this->idPersCto,
            "sitPersCto"    => $this->sitPersCto,
            "departamento"  => $this->departamento,
            "fechaAltaPC"   => $this->fechaAltaPC,
            "fechaUltVis"   => $this->fechaUltVis,
            "nroContPC"     => $this->nroContPC,
            "emailAlb"      => $this->emailAlb,
            "emailAge"      => $this->emailAge,
            "cliRefPre"     => $this->cliRefPre,
            "consent"       => $this->consent,
            "fechaConsent"  => $this->fechaConsent,
            "horaConsent"   => $this->horaConsent,
            "cellPhone"     => $this->cellPhone,
            "contactInformation" => $this->contactInformation,
        ];
    }
    public function guid(): string
    {
        return $this->guid;
    }

    public function datetime(): string
    {
        return $this->datetime;
    }

    public function firstName(): string
    {
        return $this->firstName;
    }

    public function lastName(): string
    {
        return $this->lastName;
    }

    public function idContacto(): string
    {
        return $this->idContacto;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function empresa(): string
    {
        return $this->empresa;
    }

    public function fechaNac(): ?string
    {
        return $this->fechaNac;
    }

    public function fechaUltCom(): ?string
    {
        return $this->fechaUltCom;
    }

    public function genero(): string
    {
        return $this->genero;
    }

    public function subscriptions(): ?string
    {
        return $this->subscriptions;
    }

    public function idPicUp(): string
    {
        return $this->idPicUp;
    }

    public function nombreCliente(): string
    {
        return $this->nombreCliente;
    }

    public function sitCliente(): string
    {
        return $this->sitCliente;
    }

    public function mercado(): string
    {
        return $this->mercado;
    }

    public function tratamiento(): string
    {
        return $this->tratamiento;
    }

    public function origenDigital(): string
    {
        return $this->origenDigital;
    }

    public function delegacion(): string
    {
        return $this->delegacion;
    }

    public function nombreJRV(): string
    {
        return $this->nombreJRV;
    }

    public function vendExt(): ?string
    {
        return $this->vendExt;
    }

    public function vendInt(): ?string
    {
        return $this->vendInt;
    }

    public function sectorTxt(): string
    {
        return $this->sectorTxt;
    }

    public function zonaGeo(): string
    {
        return $this->zonaGeo;
    }

    public function localidad(): string
    {
        return $this->localidad;
    }

    public function fechaAltaCli(): string
    {
        return $this->fechaAltaCli;
    }

    public function facturaPapel(): string
    {
        return $this->facturaPapel;
    }

    public function catLibre1(): ?string
    {
        return $this->catLibre1;
    }

    public function catLibre2(): ?string
    {
        return $this->catLibre2;
    }

    public function language(): string
    {
        return $this->language;
    }

    public function idPersCto(): string
    {
        return $this->idPersCto;
    }

    public function sitPersCto(): string
    {
        return $this->sitPersCto;
    }

    public function departamento(): string
    {
        return $this->departamento;
    }

    public function fechaAltaPC(): string
    {
        return $this->fechaAltaPC;
    }

    public function fechaUltVis(): ?string
    {
        return $this->fechaUltVis;
    }

    public function nroContPC(): string
    {
        return $this->nroContPC;
    }

    public function emailAlb(): string
    {
        return $this->emailAlb;
    }

    public function emailAge(): string
    {
        return $this->emailAge;
    }

    public function cliRefPre(): ?string
    {
        return $this->cliRefPre;
    }

    public function consent(): ?string
    {
        return $this->consent;
    }

    public function fechaConsent(): ?string
    {
        return $this->fechaConsent;
    }

    public function horaConsent(): ?string
    {
        return $this->horaConsent;
    }

    public function cellPhone(): ?string
    {
        return $this->cellPhone;
    }
    public function contactInformation(): array
    {
        return $this->contactInformation;
    }
}
