<?php

namespace Cyberclick\ERPConnector\Contact\Application\ReturnContact;

use Cyberclick\ERPConnector\Contact\Domain\Contact;
use Cyberclick\ERPConnector\Contact\Domain\ContactCatLibre1;
use Cyberclick\ERPConnector\Contact\Domain\ContactCatLibre2;
use Cyberclick\ERPConnector\Contact\Domain\ContactCellPhone;
use Cyberclick\ERPConnector\Contact\Domain\ContactCliRefPre;
use Cyberclick\ERPConnector\Contact\Domain\ContactConsent;
use Cyberclick\ERPConnector\Contact\Domain\ContactDelegacion;
use Cyberclick\ERPConnector\Contact\Domain\ContactDepartamento;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmail;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmailAge;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmailAlb;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmpresa;
use Cyberclick\ERPConnector\Contact\Domain\ContactFacturaPapel;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaAltaCli;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaAltaPC;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaConsent;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaNac;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaUltCom;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaUltVis;
use Cyberclick\ERPConnector\Contact\Domain\ContactFirstName;
use Cyberclick\ERPConnector\Contact\Domain\ContactGenero;
use Cyberclick\ERPConnector\Contact\Domain\ContactHoraConsent;
use Cyberclick\ERPConnector\Contact\Domain\ContactIdContacto;
use Cyberclick\ERPConnector\Contact\Domain\ContactIdPersCto;
use Cyberclick\ERPConnector\Contact\Domain\ContactIdPicUp;
use Cyberclick\ERPConnector\Contact\Domain\ContactLanguage;
use Cyberclick\ERPConnector\Contact\Domain\ContactLastName;
use Cyberclick\ERPConnector\Contact\Domain\ContactLocalidad;
use Cyberclick\ERPConnector\Contact\Domain\ContactMercado;
use Cyberclick\ERPConnector\Contact\Domain\ContactNombreCliente;
use Cyberclick\ERPConnector\Contact\Domain\ContactNombreJRV;
use Cyberclick\ERPConnector\Contact\Domain\ContactNroContPC;
use Cyberclick\ERPConnector\Contact\Domain\ContactOrigenDigital;
use Cyberclick\ERPConnector\Contact\Domain\ContactSectorTxt;
use Cyberclick\ERPConnector\Contact\Domain\ContactSitCliente;
use Cyberclick\ERPConnector\Contact\Domain\ContactSitPersCto;
use Cyberclick\ERPConnector\Contact\Domain\ContactSubscriptions;
use Cyberclick\ERPConnector\Contact\Domain\ContactTratamiento;
use Cyberclick\ERPConnector\Contact\Domain\ContactVendExt;
use Cyberclick\ERPConnector\Contact\Domain\ContactVendInt;
use Cyberclick\ERPConnector\Contact\Domain\ContactZonaGeo;
use Cyberclick\Shared\Domain\Bus\Event\EventBus;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Contact\ContactId;


class ReturnContact
{
    public function __construct(
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(ContactId            $id,
                             CallGUID           $guid,
                             CallDatetime       $datetime,
                             ContactFirstName   $firstName,
                             ContactLastName    $lastName,
                             ContactIdContacto  $idContacto,
                             ContactEmail       $email,
                             ContactEmpresa     $empresa,
                             ContactFechaNac    $fechaNac,
                             ContactFechaUltCom $fechaUltCom,
                             ContactGenero      $genero,
                             ContactSubscriptions $subscriptions,
                             ContactIdPicUp       $idPicUp,
                             ContactNombreCliente $nombreCliente,
                             ContactSitCliente    $sitCliente,
                             ContactMercado       $mercado,
                             ContactTratamiento   $tratamiento,
                             ContactOrigenDigital $origenDigital,
                             ContactDelegacion    $delegacion,
                             ContactNombreJRV     $nombreJRV,
                             ContactVendExt       $vendExt,
                             ContactVendInt       $vendInt,
                             ContactSectorTxt     $sectorTxt,
                             ContactZonaGeo       $zonaGeo,
                             ContactLocalidad     $localidad,
                             ContactFechaAltaCli  $fechaAltaCli,
                             ContactFacturaPapel  $facturaPapel,
                             ContactCatLibre1     $catLibre1,
                             ContactCatLibre2     $catLibre2,
                             ContactLanguage      $language,
                             ContactIdPersCto     $idPersCto,
                             ContactSitPersCto    $sitPersCto,
                             ContactDepartamento  $departamento,
                             ContactFechaAltaPC   $fechaAltaPC,
                             ContactFechaUltVis   $fechaUltVis,
                             ContactNroContPC     $nroContPC,
                             ContactEmailAlb      $emailAlb,
                             ContactEmailAge      $emailAge,
                             ContactCliRefPre     $cliRefPre,
                             ContactConsent       $consent,
                             ContactFechaConsent  $fechaConsent,
                             ContactHoraConsent   $horaConsent,
                             ContactCellPhone     $cellPhone,
                             array $contactInformation): void



    {
        $contact = Contact::return($id, $guid, $datetime, $firstName, $lastName, $idContacto, $email, $empresa,$fechaNac, $fechaUltCom, $genero,
            $subscriptions, $idPicUp, $nombreCliente, $sitCliente, $mercado, $tratamiento, $origenDigital, $delegacion,
            $nombreJRV, $vendExt, $vendInt, $sectorTxt, $zonaGeo, $localidad, $fechaAltaCli, $facturaPapel, $catLibre1,
            $catLibre2, $language, $idPersCto, $sitPersCto, $departamento, $fechaAltaPC, $fechaUltVis, $nroContPC,
            $emailAlb, $emailAge, $cliRefPre, $consent, $fechaConsent, $horaConsent, $cellPhone, $contactInformation);
        $this->eventBus->publish(... $contact->pullDomainEvents());
    }
}
