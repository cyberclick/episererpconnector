<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\Contact\Application\UpdateContact;

use Cyberclick\ERPConnector\Contact\Domain\ContactFirstName;
use Cyberclick\ERPConnector\Contact\Domain\ContactLastName;
use Cyberclick\ERPConnector\Contact\Domain\ContactIdContacto;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmail;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmpresa;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaNac;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaUltCom;
use Cyberclick\ERPConnector\Contact\Domain\ContactGenero;
use Cyberclick\ERPConnector\Contact\Domain\ContactSubscriptions;
use Cyberclick\ERPConnector\Contact\Domain\ContactIdPicUp;
use Cyberclick\ERPConnector\Contact\Domain\ContactNombreCliente;
use Cyberclick\ERPConnector\Contact\Domain\ContactSitCliente;
use Cyberclick\ERPConnector\Contact\Domain\ContactMercado;
use Cyberclick\ERPConnector\Contact\Domain\ContactTratamiento;
use Cyberclick\ERPConnector\Contact\Domain\ContactOrigenDigital;
use Cyberclick\ERPConnector\Contact\Domain\ContactDelegacion;
use Cyberclick\ERPConnector\Contact\Domain\ContactNombreJRV;
use Cyberclick\ERPConnector\Contact\Domain\ContactVendExt;
use Cyberclick\ERPConnector\Contact\Domain\ContactVendInt;
use Cyberclick\ERPConnector\Contact\Domain\ContactSectorTxt;
use Cyberclick\ERPConnector\Contact\Domain\ContactZonaGeo;
use Cyberclick\ERPConnector\Contact\Domain\ContactLocalidad;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaAltaCli;
use Cyberclick\ERPConnector\Contact\Domain\ContactFacturaPapel;
use Cyberclick\ERPConnector\Contact\Domain\ContactCatLibre1;
use Cyberclick\ERPConnector\Contact\Domain\ContactCatLibre2;
use Cyberclick\ERPConnector\Contact\Domain\ContactLanguage;
use Cyberclick\ERPConnector\Contact\Domain\ContactIdPersCto;
use Cyberclick\ERPConnector\Contact\Domain\ContactSitPersCto;
use Cyberclick\ERPConnector\Contact\Domain\ContactDepartamento;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaAltaPC;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaUltVis;
use Cyberclick\ERPConnector\Contact\Domain\ContactNroContPC;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmailAlb;
use Cyberclick\ERPConnector\Contact\Domain\ContactEmailAge;
use Cyberclick\ERPConnector\Contact\Domain\ContactCliRefPre;
use Cyberclick\ERPConnector\Contact\Domain\ContactConsent;
use Cyberclick\ERPConnector\Contact\Domain\ContactFechaConsent;
use Cyberclick\ERPConnector\Contact\Domain\ContactHoraConsent;
use Cyberclick\ERPConnector\Contact\Domain\ContactCellPhone;
use Cyberclick\Shared\Domain\Bus\Command\CommandHandler;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Contact\ContactId;
use function Lambdish\Phunctional\apply;

final class UpdateContactCommandHandler implements CommandHandler
{
    public function __construct(
        private ContactUpdater $updater,
    )
    {
    }

    public function __invoke(UpdateContactCommand $command)
    {
        $id = new ContactId($command->id());
        $guid      = new CallGUID($command->guid());
        $datetime      = new CallDatetime($command->datetime());
        $firstName     = new ContactFirstName($command->firstName());
        $lastName      = new ContactLastName($command->lastName());
        $idContacto    = new ContactIdContacto($command->idContacto());
        $email         = new ContactEmail($command->email());
        $empresa       = new ContactEmpresa($command->empresa());
        $fechaNac      = new ContactFechaNac($command->fechaNac());
        $fechaUltCom   = new ContactFechaUltCom($command->fechaUltCom());
        $genero        = new ContactGenero($command->genero());
        $subscriptions = new ContactSubscriptions($command->subscriptions());
        $idPicUp       = new ContactIdPicUp($command->idPicUp());
        $nombreCliente = new ContactNombreCliente($command->nombreCliente());
        $sitCliente    = new ContactSitCliente($command->sitCliente());
        $mercado       = new ContactMercado($command->mercado());
        $tratamiento   = new ContactTratamiento($command->tratamiento());
        $origenDigital = new ContactOrigenDigital($command->origenDigital());
        $delegacion    = new ContactDelegacion($command->delegacion());
        $nombreJRV     = new ContactNombreJRV($command->nombreJRV());
        $vendExt       = new ContactVendExt($command->vendExt());
        $vendInt       = new ContactVendInt($command->vendInt());
        $sectorTxt     = new ContactSectorTxt($command->sectorTxt());
        $zonaGeo       = new ContactZonaGeo($command->zonaGeo());
        $localidad     = new ContactLocalidad($command->localidad());
        $fechaAltaCli  = new ContactFechaAltaCli($command->fechaAltaCli());
        $facturaPapel  = new ContactFacturaPapel($command->facturaPapel());
        $catLibre1     = new ContactCatLibre1($command->catLibre1());
        $catLibre2     = new ContactCatLibre2($command->catLibre2());
        $language      = new ContactLanguage($command->language());
        $idPersCto     = new ContactIdPersCto($command->idPersCto());
        $sitPersCto    = new ContactSitPersCto($command->sitPersCto());
        $departamento  = new ContactDepartamento($command->departamento());
        $fechaAltaPC   = new ContactFechaAltaPC($command->fechaAltaPC());
        $fechaUltVis   = new ContactFechaUltVis($command->fechaUltVis());
        $nroContPC     = new ContactNroContPC($command->nroContPC());
        $emailAlb      = new ContactEmailAlb($command->emailAlb());
        $emailAge      = new ContactEmailAge($command->emailAge());
        $cliRefPre     = new ContactCliRefPre($command->cliRefPre());
        $consent       = new ContactConsent($command->consent());
        $fechaConsent  = new ContactFechaConsent($command->fechaConsent());
        $horaConsent   = new ContactHoraConsent($command->horaConsent());
        $cellPhone     = new ContactCellPhone($command->cellPhone());


        apply($this->updater, [$id, $guid, $datetime, $firstName, $lastName, $idContacto, $email, $empresa, $fechaNac, $fechaUltCom, $genero,
                               $subscriptions, $idPicUp, $nombreCliente, $sitCliente, $mercado, $tratamiento, $origenDigital,
                               $delegacion, $nombreJRV, $vendExt, $vendInt, $sectorTxt, $zonaGeo, $localidad, $fechaAltaCli,
                               $facturaPapel, $catLibre1, $catLibre2, $language, $idPersCto, $sitPersCto, $departamento,
                               $fechaAltaPC, $fechaUltVis, $nroContPC, $emailAlb, $emailAge, $cliRefPre, $consent, $fechaConsent, $horaConsent, $cellPhone]);
    }
}
