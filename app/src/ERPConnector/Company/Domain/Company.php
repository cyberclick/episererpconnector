<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\Company\Domain;


use Cyberclick\ERPConnector\Company\Domain\CompanyCreditUpdate\CompanyCreditUpdatedDomainEvent;
use Cyberclick\Shared\Domain\Company\CompanyId;
use Cyberclick\Shared\Domain\Aggregate\AggregateRoot;


final class Company extends AggregateRoot
{
    public function __construct(
        private CompanyName $name,
        private CompanyERPCode $erpCode,
        private CompanyDUNSCode $DUNSCode,
        private CompanyCreditAssegurat $creditAssegurat,
        private CompanyCreditIntern $creditIntern,
        private CompanyNotaCredit   $notaCredit,

    )
    {
        parent::__construct();
    }

    public static function create(
                                  CompanyId $id,
                                  CompanyName $name,
                                  CompanyERPCode $erpCode,
                                  CompanyDUNSCode $DUNSCode,
                                  CompanyCreditAssegurat $creditAssegurat,
                                  CompanyCreditIntern $creditIntern,
                                  CompanyNotaCredit $notaCredit):self
    {
        $company = new self($name, $erpCode, $DUNSCode, $creditAssegurat, $creditIntern, $notaCredit);
        $company->record(
            new CompanyCreditUpdatedDomainEvent(
                $id->value(),
                $name->value(),
                $erpCode->value(),
                $DUNSCode->value(),
                $creditAssegurat->value(),
                $creditIntern->value(),
                $notaCredit->value()

            )
        );
        return $company;
    }


    public function name(): CompanyName
    {
        return $this->name;
    }

    public function erpCode(): CompanyERPCode
    {
        return $this->erpCode;
    }


    public function DUNSCode(): CompanyDUNSCode
    {
        return $this->DUNSCode;
    }

    public function creditAssegurat(): CompanyCreditAssegurat
    {
        return $this->creditAssegurat;
    }

    public function creditIntern(): CompanyCreditIntern
    {
        return $this->creditIntern;
    }

    public function notaCredit(): CompanyNotaCredit
    {
        return $this->notaCredit;
    }

}
