<?php

namespace Cyberclick\ERPConnector\Company\Domain\CompanyCreditUpdate;

use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;

final class CompanyCreditUpdatedDomainEvent extends DomainEvent
{
    private const VERSION = 1;

    private string $name;
    private string $erpCode;
    private string $DUNSCode;
    private int $creditAssegurat;
    private int $creditIntern;
    private string $notaCredit;

    public function __construct(string $id , string $name,  string $erpCode,string $DUNSCode, int $creditAssegurat ,
                                int $creditIntern, string $notaCredit, string $eventId = null, string $occurredOn = null)
    {
        parent::__construct( $id ,$eventId, $occurredOn);
        $this->name = $name;
        $this->erpCode = $erpCode;
        $this->DUNSCode = $DUNSCode;
        $this->creditAssegurat = $creditAssegurat;
        $this->creditIntern = $creditIntern;
        $this->notaCredit = $notaCredit;
    }

    public static function fromPrimitives(string $aggregateId, array $body, string $eventId, string $occurredOn): DomainEvent
    {
        return new self(
            $aggregateId,
            $body["name"],
            $body["erpCode"],
            $body["DUNSCode"],
            $body["creditAssegurat"],
            $body["creditIntern"],
            $body["notaCredit"],
            $eventId, $occurredOn
        );
    }

    public static function eventName(): string
    {
        return "company.updated";
    }

    public function toPrimitives(): array
    {
        return [
            'name'            => $this->name,
            'erpCode'          => $this->erpCode,
            'DUNSCode' => $this->DUNSCode,
            'creditAssegurat' => $this->creditAssegurat,
            'creditIntern' => $this->creditIntern,
            'notaCredit' => $this->notaCredit

        ];
    }

    public function name(): string
    {
        return $this->name;
    }

    public function erpCode(): string
    {
        return $this->erpCode;
    }

    public function DUNSCode(): string
    {
        return $this->DUNSCode;
    }

    public function creditAssegurat(): int
    {
        return $this->creditAssegurat;
    }

    public function creditIntern(): int
    {
        return $this->creditIntern;
    }

    public function notaCredit(): string
    {
        return $this->notaCredit;
    }

}
