<?php
declare(strict_types=1);


namespace Cyberclick\ERPConnector\Company\Application\UpdateCredit;

use Cyberclick\Shared\Domain\Bus\Command\Command;

final class UpdateCompanyCreditCommand implements Command
{
    public function __construct(
        private string $id,
        private string $name,
        private string $erpCode,
        private string $DUNSCode,
        private int $creditAssegurat,
        private int $creditIntern,
        private string $notaCredit
    )
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function erpCode(): string
    {
        return $this->erpCode;
    }

    public function DUNSCode(): string
    {
        return $this->DUNSCode;
    }

    public function creditAssegurat(): int
    {
        return $this->creditAssegurat;
    }

    public function creditIntern(): int
    {
        return $this->creditIntern;
    }

    public function notaCredit(): string
    {
        return $this->notaCredit;
    }


}
