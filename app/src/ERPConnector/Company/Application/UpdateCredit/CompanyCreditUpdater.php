<?php

namespace Cyberclick\ERPConnector\Company\Application\UpdateCredit;

use Cyberclick\ERPConnector\Company\Domain\Company;
use Cyberclick\ERPConnector\Company\Domain\CompanyCreditAssegurat;
use Cyberclick\ERPConnector\Company\Domain\CompanyCreditIntern;
use Cyberclick\ERPConnector\Company\Domain\CompanyDUNSCode;
use Cyberclick\ERPConnector\Company\Domain\CompanyERPCode;
use Cyberclick\ERPConnector\Company\Domain\CompanyName;
use Cyberclick\ERPConnector\Company\Domain\CompanyNotaCredit;
use Cyberclick\Shared\Domain\Company\CompanyId;
use Cyberclick\Shared\Domain\Bus\Event\EventBus;
use phpDocumentor\Reflection\Types\Void_;
use function Lambdish\Phunctional\identity;

final class CompanyCreditUpdater
{
    public function __construct(
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(CompanyId $id, CompanyName $name, CompanyERPCode $erpCode,
                             CompanyDUNSCode $DUNSCode, CompanyCreditAssegurat $creditAssegurat,
                            CompanyCreditIntern $creditIntern, CompanyNotaCredit $notaCredit) : void

    {
        $company = Company::create($id , $name, $erpCode, $DUNSCode, $creditAssegurat, $creditIntern, $notaCredit);
        $this->eventBus->publish(... $company->pullDomainEvents());
    }
}
