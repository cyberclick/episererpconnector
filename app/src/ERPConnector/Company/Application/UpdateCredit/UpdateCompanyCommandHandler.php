<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\Company\Application\UpdateCredit;

use Cyberclick\ERPConnector\Company\Domain\CompanyCreditAssegurat;
use Cyberclick\ERPConnector\Company\Domain\CompanyCreditIntern;
use Cyberclick\ERPConnector\Company\Domain\CompanyDUNSCode;
use Cyberclick\ERPConnector\Company\Domain\CompanyERPCode;
use Cyberclick\ERPConnector\Company\Domain\CompanyName;
use Cyberclick\ERPConnector\Company\Domain\CompanyNotaCredit;
use Cyberclick\Shared\Domain\Bus\Command\CommandHandler;
use Cyberclick\Shared\Domain\Company\CompanyId;
use Cyberclick\Shared\Domain\ValueObject\Uuid;
use function Lambdish\Phunctional\apply;

final class UpdateCompanyCommandHandler implements CommandHandler
{
    public function __construct(
        private CompanyCreditUpdater $updater,
    )
    {
    }

    public function __invoke(UpdateCompanyCreditCommand $command)
    {
        $id = new CompanyId($command->id());
        $name = new CompanyName($command->name());
        $erpCode = new CompanyERPCode($command->erpCode());
        $DUNSCode = new CompanyDUNSCode($command->DUNSCode());
        $creditAssegurat = new CompanyCreditAssegurat($command->creditAssegurat());
        $creditIntern = new CompanyCreditIntern($command->creditIntern());
        $notaCredit = new CompanyNotaCredit($command->notaCredit());


        apply($this->updater, [$id, $name, $erpCode, $DUNSCode, $creditAssegurat, $creditIntern, $notaCredit]);
    }
}
