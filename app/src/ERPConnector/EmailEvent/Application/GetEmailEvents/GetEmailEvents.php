<?php

namespace Cyberclick\ERPConnector\EmailEvent\Application\GetEmailEvents;

use Cyberclick\ERPConnector\EmailEvent\Domain\EmailEvent;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventContactId;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventEmail;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventEmailType;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventFechaFinal;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventFechaInicial;
use Cyberclick\Shared\Domain\Bus\Event\EventBus;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\EmailEvent\EventId;

class GetEmailEvents
{
    public function __construct(
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(EventId $id , CallGUID $guid , CallDatetime $datetime,
                             EventFechaInicial $fechaInicial, EventFechaFinal $fechaFinal, EventEmailType $emailType,
                             EventEmail $email, EventContactId $idContacto) : void

    {
        $event = EmailEvent::create($id, $guid, $datetime, $fechaInicial, $fechaFinal, $emailType, $email, $idContacto);
        $this->eventBus->publish(... $event->pullDomainEvents());
    }
}
