<?php
declare(strict_types=1);


namespace Cyberclick\ERPConnector\EmailEvent\Application\GetEmailEvents;

use Cyberclick\ERPConnector\EmailEvent\Domain\EventContactId;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventEmail;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventEmailType;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventFechaFinal;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventFechaInicial;
use Cyberclick\Shared\Domain\Bus\Command\CommandHandler;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\EmailEvent\EventId;
use function Lambdish\Phunctional\apply;


class GetEmailEventsCommandHandler implements CommandHandler
{
    public function __construct(
        private GetEmailEvents $notifier,
    )
    {
    }

    public function __invoke(GetEmailEventsCommand $command)
    {
        $id = new EventId($command->id());
        $fechaInicial = new EventFechaInicial($command->fechaInicial());
        $fechaFinal = new EventFechaFinal($command->fechaFinal());
        $emailType = new EventEmailType($command->emailType());
        $email = new EventEmail($command->email());
        $idContacto = new EventContactId($command->idContacto());
        $guid = new CallGUID($command->guid());
        $datetime = new CallDatetime($command->datetime());



        apply($this->notifier, [$id, $guid, $datetime, $fechaInicial, $fechaFinal, $emailType, $email, $idContacto]);
    }
}
