<?php

namespace Cyberclick\ERPConnector\EmailEvent\Domain\EmailEvent;

use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;

final class ReturnEmailEventDomainEvent extends DomainEvent
{
    private const VERSION = 1;

        private string $guid;
        private string $datetime;
        private string $fechaInicial;
        private string $fechaFinal;
        private string $emailType;
        private string $email;
        private string $idContacto;
        private array $eventInformation;

    public function __construct(string $id ,string $guid, string $datetime, string $fechaInicial, string $fechaFinal, string $emailType, string $email,
                                 string $idContacto, array $eventInformation,  string $eventId = null, string $occurredOn = null)
    {
        parent::__construct( $id ,$eventId, $occurredOn);
        $this->guid = $guid;
        $this->datetime = $datetime;
        $this->fechaInicial = $fechaInicial;
        $this->fechaFinal = $fechaFinal;
        $this->emailType = $emailType;
        $this->email = $email;
        $this->idContacto = $idContacto;
        $this->eventInformation = $eventInformation;

    }

    public static function fromPrimitives(string $aggregateId, array $body, string $eventId, string $occurredOn): DomainEvent
    {
        return new self(
            $aggregateId,
            $body["guid"],
            $body["datetime"],
            $body["fechaInicial"],
            $body["fechaFinal"],
            $body["emailType"],
            $body["email"],
            $body["idContacto"],
            $body["eventInformation"],
            $eventId, $occurredOn
        );
    }

    public static function eventName(): string
    {
        return "return.email.event";
    }

    public function toPrimitives(): array
    {
        return [
            'guid' => $this->guid,
            'datetime' => $this->datetime,
            'fechaInicial' => $this->fechaInicial,
            'fechaFinal' => $this->fechaFinal,
            'emailType' => $this->emailType,
            'email' => $this->email,
            'idContacto' => $this->idContacto,
            'eventInformation' => $this->eventInformation,
        ];
    }

    public function fechaInicial(): string
    {
        return $this->fechaInicial;
    }

    public function fechaFinal(): string
    {
        return $this->fechaFinal;
    }

    public function emailType(): string
    {
        return $this->emailType;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function idContacto(): string
    {
        return $this->idContacto;
    }

    public function guid(): string
    {
        return $this->guid;
    }

    public function datetime(): string
    {
        return $this->datetime;
    }

    public function eventInformation(): array
    {
        return $this->eventInformation;
    }
}
