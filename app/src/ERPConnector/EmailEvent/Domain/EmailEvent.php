<?php

namespace Cyberclick\ERPConnector\EmailEvent\Domain;

use Cyberclick\ERPConnector\EmailEvent\Domain\EmailEvent\GetEmailEventsDomainEvent;
use Cyberclick\ERPConnector\EmailEvent\Domain\EmailEvent\ReturnEmailEventDomainEvent;
use Cyberclick\Shared\Domain\Aggregate\AggregateRoot;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\EmailEvent\EventId;

class EmailEvent extends AggregateRoot
{
    public function __construct(
        private CallGUID $guid,
        private CallDatetime $datetime,
        private EventFechaInicial $fechaInicial,
        private EventFechaFinal $fechaFinal,
        private EventEmailType $emailType,
        private EventEmail $email,
        private EventContactId $idContacto,
    )
    {
        parent::__construct();
    }

    public static function create(
        EventId $id,
        CallGUID $guid,
        CallDatetime $datetime,
        EventFechaInicial $fechaInicial,
        EventFechaFinal $fechaFinal,
        EventEmailType $emailType,
        EventEmail $email,
        EventContactId $idContacto):self
    {
        $Contact = new self($guid, $datetime,$fechaInicial, $fechaFinal, $emailType, $email, $idContacto);
        $Contact->record(
            new GetEmailEventsDomainEvent(
                $id->value(),
                $guid->value(),
                $datetime->value(),
                $fechaInicial->value(),
                $fechaFinal->value(),
                $emailType->value(),
                $email->value(),
                $idContacto->value()
            )
        );
        return $Contact;
    }

    public static function return(
        EventId $id,
        CallGUID $guid,
        CallDatetime $datetime,
        EventFechaInicial $fechaInicial,
        EventFechaFinal $fechaFinal,
        EventEmailType $emailType,
        EventEmail $email,
        EventContactId $idContacto,
        array $eventInformation):self
    {
        $Contact = new self($guid, $datetime,$fechaInicial, $fechaFinal, $emailType, $email, $idContacto);
        $Contact->record(
            new ReturnEmailEventDomainEvent(
                $id->value(),
                $guid->value(),
                $datetime->value(),
                $fechaInicial->value(),
                $fechaFinal->value(),
                $emailType->value(),
                $email->value(),
                $idContacto->value(),
                $eventInformation,
            )
        );
        return $Contact;
    }

    public function fechaInicial(): EventFechaInicial
    {
        return $this->fechaInicial;
    }

    public function fechaFinal(): EventFechaFinal
    {
        return $this->fechaFinal;
    }

    public function emailType(): EventEmailType
    {
        return $this->emailType;
    }

    public function email(): EventEmail
    {
        return $this->email;
    }

    public function idContacto(): EventContactId
    {
        return $this->idContacto;
    }

    public function guid(): CallGUID
    {
        return $this->guid;
    }
    public function datetime(): CallDatetime
    {
        return $this->datetime;
    }
}
