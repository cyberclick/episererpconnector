<?php
declare(strict_types=1);

namespace Cyberclick\ERPConnector\EmailEvent\Domain;

use Cyberclick\Shared\Domain\ValueObject\StringValueObject;

class EventGUID  extends StringValueObject
{

}
