<?php
declare(strict_types=1);


namespace Cyberclick\ERPConnector\EmailSender\Application\ReturnEmail;

use Cyberclick\ERPConnector\EmailEvent\Domain\EventContactId;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventEmail;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventEmailType;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventFechaFinal;
use Cyberclick\ERPConnector\EmailEvent\Domain\EventFechaInicial;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailFrom;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailSendId;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailTemplateId;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailTo;
use Cyberclick\Shared\Domain\Bus\Command\CommandHandler;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Email\EmailId;
use Cyberclick\Shared\Domain\EmailEvent\EventId;
use function Lambdish\Phunctional\apply;


class ReturnEmailSentCommandHandler implements CommandHandler
{
    public function __construct(
        private ReturnEmailSent $notifier,
    )
    {
    }

    public function __invoke(ReturnEmailSentCommand $command)
    {
        $id = new EmailId($command->id());
        $contact = new EmailTo($command->contact());
        $sender = new EmailFrom($command->sender());
        $sendId = new EmailSendId($command->sendId());
        $replyTo=$command->replyTo();
        $CC = $command->CC();
        $BCC = $command->BCC();
        $contactProperties = $command->contactProperties();
        $customProperties = $command->customProperties();
        $templateId = new EmailTemplateId($command->templateId());
        $guid = new CallGUID($command->guid());
        $datetime = new CallDatetime($command->datetime());
        $emailInformation = $command->emailInformation();



        apply($this->notifier, [$id, $guid, $datetime, $contact, $sender, $sendId, $replyTo, $CC, $BCC, $contactProperties, $customProperties, $templateId, $emailInformation]);
    }
}
