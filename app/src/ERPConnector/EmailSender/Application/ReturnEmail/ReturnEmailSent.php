<?php

namespace Cyberclick\ERPConnector\EmailSender\Application\ReturnEmail;

use Cyberclick\ERPConnector\EmailSender\Domain\Email;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailFrom;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailSendId;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailTemplateId;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailTo;
use Cyberclick\Shared\Domain\Bus\Event\EventBus;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Email\EmailId;

class ReturnEmailSent
{
    public function __construct(
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(EmailId $id , CallGUID $guid , CallDatetime $datetime,
                             EmailTo $contact, EmailFrom $sender, EmailSendId $sendId,
                             array $replyTo, array $CC, array $BCC,
                             array $contactProperties, array $customProperties, EmailTemplateId $templateId, array $eventInformation) : void

    {
        $event = Email::return($id, $guid, $datetime, $contact, $sender, $sendId, $replyTo, $CC, $BCC, $contactProperties, $customProperties, $templateId, $eventInformation);
        $this->eventBus->publish(... $event->pullDomainEvents());
    }
}
