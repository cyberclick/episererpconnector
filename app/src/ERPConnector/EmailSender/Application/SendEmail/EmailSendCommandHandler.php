<?php

namespace Cyberclick\ERPConnector\EmailSender\Application\SendEmail;


use Cyberclick\ERPConnector\EmailSender\Domain\EmailFrom;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailSendId;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailTemplateId;
use Cyberclick\ERPConnector\EmailSender\Domain\EmailTo;
use Cyberclick\Shared\Domain\Bus\Command\CommandHandler;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Email\EmailId;
use function Lambdish\Phunctional\apply;

class EmailSendCommandHandler implements CommandHandler
{
    public function __construct(
        private EmailSender $sender,
    )
    {
    }
    public function __invoke(EmailSendCommand $command)
    {
        $id = new EmailId($command->id());
        $contact = new EmailTo($command->contact());
        $sender = new EmailFrom($command->sender());
        $sendId = new EmailSendId($command->sendId());
        $replyTo=$command->replyTo();
        $CC = $command->CC();
        $BCC = $command->BCC();
        $contactProperties = $command->contactProperties();
        $customProperties = $command->customProperties();
        $templateId = new EmailTemplateId($command->templateId());
        $guid = new CallGUID($command->guid());
        $datetime = new CallDatetime($command->datetime());



        apply($this->sender, [$id, $guid, $datetime, $contact, $sender, $sendId, $replyTo, $CC, $BCC, $contactProperties, $customProperties, $templateId]);
    }
}
