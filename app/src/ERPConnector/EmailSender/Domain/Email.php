<?php

namespace Cyberclick\ERPConnector\EmailSender\Domain;

use Cyberclick\ERPConnector\EmailSender\Domain\SendEmail\ReturnEmailDomainEvent;
use Cyberclick\ERPConnector\EmailSender\Domain\SendEmail\SendEmailDomainEvent;
use Cyberclick\Shared\Domain\Aggregate\AggregateRoot;
use Cyberclick\Shared\Domain\CallParameters\CallDatetime;
use Cyberclick\Shared\Domain\CallParameters\CallGUID;
use Cyberclick\Shared\Domain\Email\EmailId;

class Email extends AggregateRoot
{
    public function __construct(
        private CallGUID $guid,
        private CallDatetime $datetime,
        private EmailTo $contact,
        private ?EmailFrom $sender,
        private ?EmailSendId $sendId,
        private ?array $replyTo,
        private ?array $CC,
        private ?array $BCC,
        private ?array $contactProperties,
        private ?array $customProperties,
        private EmailTemplateId $templateId,

    )
    {
        parent::__construct();
    }
    public static function create(
        EmailId $id,
        CallGUID $guid,
        CallDatetime $datetime,
        EmailTo $contact,
        ?EmailFrom $sender,
        ?EmailSendId $sendId,
        ?array $replyTo,
        ?array $CC,
        ?array $BCC,
        ?array $contactProperties,
        ?array $customProperties,
        EmailTemplateId $templateId):self
    {
        $Email = new self($guid, $datetime, $contact, $sender, $sendId, $replyTo, $CC, $BCC, $contactProperties, $customProperties, $templateId);
        $Email->record(
            new SendEmailDomainEvent(
                $id->value(),
                $guid->value(),
                $datetime->value(),
                $contact->value(),
                $sender->value(),
                $sendId->value(),
                $replyTo,
                $CC,
                $BCC,
                $contactProperties,
                $customProperties,
                $templateId->value(),
            )
        );
        return $Email;
    }

    public static function return(
        EmailId $id,
        CallGUID $guid,
        CallDatetime $datetime,
        EmailTo $contact,
        ?EmailFrom $sender,
        ?EmailSendId $sendId,
        ?array $replyTo,
        ?array $CC,
        ?array $BCC,
        ?array $contactProperties,
        ?array $customProperties,
        EmailTemplateId $templateId,
        array $emailInformation):self
    {
        $Email = new self($guid, $datetime, $contact, $sender, $sendId, $replyTo, $CC, $BCC, $contactProperties, $customProperties, $templateId);
        $Email->record(
            new ReturnEmailDomainEvent(
                $id->value(),
                $guid->value(),
                $datetime->value(),
                $contact->value(),
                $sender->value(),
                $sendId->value(),
                $replyTo,
                $CC,
                $BCC,
                $contactProperties,
                $customProperties,
                $templateId->value(),
                $emailInformation
            )
        );
        return $Email;
    }

    public function guid(): CallGUID
    {
        return $this->guid;
    }

    public function datetime(): CallDatetime
    {
        return $this->datetime;
    }

    public function contact(): EmailTo
    {
        return $this->contact;
    }

    public function sender(): ?EmailFrom
    {
        return $this->sender;
    }

    public function sendId(): ?EmailSendId
    {
        return $this->sendId;
    }

    public function replyTo(): ?array
    {
        return $this->replyTo;
    }

    public function CC(): ?array
    {
        return $this->CC;
    }

    public function BCC(): ?array
    {
        return $this->BCC;
    }

    public function contactProperties(): ?array
    {
        return $this->contactProperties;
    }

    public function customProperties(): ?array
    {
        return $this->customProperties;
    }

    public function templateId(): EmailTemplateId
    {
        return $this->templateId;
    }


}
