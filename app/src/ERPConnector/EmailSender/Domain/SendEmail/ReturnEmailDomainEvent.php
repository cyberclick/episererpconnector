<?php

namespace Cyberclick\ERPConnector\EmailSender\Domain\SendEmail;

use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;

class ReturnEmailDomainEvent  extends DomainEvent
{
    private const VERSION = 1;

    private string $guid;
    private string $datetime;
    private string $contact;
    private ?string $sender;
    private ?string $sendId;
    private ?array $replyTo;
    private ?array $CC;
    private ?array $BCC;
    private ?array $contactProperties;
    private ?array $customProperties;
    private string $templateId;
    private array $emailInformation;

    public function __construct(string $id ,string $guid, string $datetime, string $contact, string $sender, string $sendId, array $replyTo,
                                array $CC,  array $BCC, array $contactProperties, array $customProperties, string $templateId, array $emailInformation,
                                string $eventId = null, string $occurredOn = null)
    {
    parent::__construct( $id ,$eventId, $occurredOn);
    $this->guid = $guid;
    $this->datetime = $datetime;
    $this->contact = $contact;
    $this->sender = $sender;
    $this->sendId = $sendId;
    $this->replyTo = $replyTo;
    $this->CC = $CC;
    $this->BCC = $BCC;
    $this->contactProperties = $contactProperties;
    $this->customProperties = $customProperties;
    $this->templateId = $templateId;
    $this->emailInformation = $emailInformation;

}

    public static function fromPrimitives(string $aggregateId, array $body, string $eventId, string $occurredOn): DomainEvent
{
    return new self(
        $aggregateId,
        $body["guid"],
        $body["datetime"],
        $body["contact"],
        $body["sender"],
        $body["sendId"],
        $body["replyTo"],
        $body["CC"],
        $body["BCC"],
        $body["contactProperties"],
        $body["customProperties"],
        $body["templateId"],
        $body["emailInformation"],
        $eventId, $occurredOn
    );
}

    public static function eventName(): string
{
    return "return.send.email";
}

    public function toPrimitives(): array
{
    return [
        'guid' => $this->guid,
        'datetime' => $this->datetime,
        'contact' => $this->contact,
        'sender' => $this->sender,
        'sendId' => $this->sendId,
        'replyTo' => $this->replyTo,
        'CC' => $this->CC,
        'BCC' => $this->BCC,
        'contactProperties' => $this->contactProperties,
        'customProperties' => $this->customProperties,
        'templateId' => $this->templateId,
        'emailInformation' => $this->emailInformation,

    ];
}

    public function guid(): string
{
    return $this->guid;
}

    public function datetime(): string
{
    return $this->datetime;
}

    public function contact(): string
{
    return $this->contact;
}

    public function sender(): ?string
{
    return $this->sender;
}

    public function sendId(): ?string
{
    return $this->sendId;
}

    public function replyTo(): ?array
{
    return $this->replyTo;
}

    public function CC(): ?array
{
    return $this->CC;
}

    public function BCC(): ?array
{
    return $this->BCC;
}

    public function contactProperties(): ?array
{
    return $this->contactProperties;
}

    public function customProperties(): ?array
{
    return $this->customProperties;
}

    public function templateId(): string
{
    return $this->templateId;
}

    public function emailInformation(): array
    {
        return $this->emailInformation;
    }
}
