
-- +migrate Up
CREATE TABLE `sl_domain_events`
(
    `id`           char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `aggregate_id` char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `name`         varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `body`         JSON                                    NOT NULL,
    `occurred_on`  timestamp                               NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE `sl_clients`
(
    `id`              char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `name`            varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `status`          TINYINT                                 NOT NULL,
    `allowed_domains` JSON                                    NOT NULL,
    `api_key`         char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `schedule`        JSON                                    NOT NULL,
    `created_on`      datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `updated_on`      datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE `sl_store_providers`
(
    `id`         char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `type`       varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `status`     TINYINT                                 NOT NULL,
    `url`        TEXT                                    NOT NULL,
    `mapping`    JSON                                    NOT NULL,
    `client_id`  char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `created_on` datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `updated_on` datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;


CREATE TABLE `sl_store_providers_salesforce`
(
    `id`                       char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `salesforce_client_id`     varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `salesforce_client_secret` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `login_data`               JSON                                    NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;


CREATE TABLE `sl_stores`
(
    `id`                    char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `name`                  varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `phone`                 varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `address`               JSON                                    NOT NULL,
    `coordinates_latitude`  DOUBLE PRECISION DEFAULT NULL,
    `coordinates_longitude` DOUBLE PRECISION DEFAULT NULL,
    `external_id`           varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `client_id`             char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `status`                TINYINT                                 NOT NULL,
    `created_on`            datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `updated_on`            datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;


CREATE TABLE `sl_geocoders`
(
    `id`         char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `type`       varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `apikey`     varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `client_id`  char(36) COLLATE utf8mb4_0900_ai_ci     NOT NULL,
    `created_on` datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `updated_on` datetime                                NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

-- +migrate Down
