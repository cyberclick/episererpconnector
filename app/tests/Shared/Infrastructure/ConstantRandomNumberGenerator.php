<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure;

use Cyberclick\Shared\Domain\RandomNumberGenerator;

final class ConstantRandomNumberGenerator implements RandomNumberGenerator
{
    public function generate(): int
    {
        return 1;
    }
}
