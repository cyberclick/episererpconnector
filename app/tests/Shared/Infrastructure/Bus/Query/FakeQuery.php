<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Bus\Query;

use Cyberclick\Shared\Domain\Bus\Query\Query;

final class FakeQuery implements Query
{
}
