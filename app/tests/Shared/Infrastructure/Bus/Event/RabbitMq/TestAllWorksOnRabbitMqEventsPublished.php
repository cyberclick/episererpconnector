<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Bus\Event\RabbitMq;

use Cyberclick\Locator\Store\Domain\StoreCreatedDomainEvent;
use Cyberclick\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class TestAllWorksOnRabbitMqEventsPublished implements DomainEventSubscriber
{
    public static function subscribedTo(): array
    {
        return [
            StoreCreatedDomainEvent::class
        ];
    }

    public function __invoke(StoreCreatedDomainEvent $event)
    {
    }
}
