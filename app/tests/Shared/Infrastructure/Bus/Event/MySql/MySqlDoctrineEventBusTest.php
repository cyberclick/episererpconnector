<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Bus\Event\MySql;

use Cyberclick\Apps\Locator\Backend\LocatorBackendKernel;
use Cyberclick\Shared\Domain\Bus\Event\DomainEvent;
use Cyberclick\Shared\Infrastructure\Bus\Event\DomainEventMapping;
use Cyberclick\Shared\Infrastructure\Bus\Event\MySql\MySqlDoctrineDomainEventsConsumer;
use Cyberclick\Shared\Infrastructure\Bus\Event\MySql\MySqlDoctrineEventBus;
use Cyberclick\Tests\Locator\Client\Domain\ClientCreatedDomainEventMother;
use Cyberclick\Tests\Shared\Infrastructure\PhpUnit\InfrastructureTestCase;
use Doctrine\ORM\EntityManager;

final class MySqlDoctrineEventBusTest extends InfrastructureTestCase
{
    private ?MySqlDoctrineEventBus $bus;
    private ?MySqlDoctrineDomainEventsConsumer $consumer;

    /** @test */
    public function it_should_publish_and_consume_domain_events_from_mysql(): void
    {
        $domainEvent        = ClientCreatedDomainEventMother::create();
        $anotherDomainEvent = ClientCreatedDomainEventMother::create();

        $this->bus->publish($domainEvent, $anotherDomainEvent);

        $this->consumer->consume(
            fn(DomainEvent ...$expectedEvents) => $this->assertContainsEquals($domainEvent, $expectedEvents),
            $eventsToConsume = 2
        );
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->bus      = new MySqlDoctrineEventBus($this->service(EntityManager::class));
        $this->consumer = new MySqlDoctrineDomainEventsConsumer(
            $this->service(EntityManager::class),
            $this->service(DomainEventMapping::class)
        );
    }

    protected function kernelClass(): string
    {
        return LocatorBackendKernel::class;
    }
}
