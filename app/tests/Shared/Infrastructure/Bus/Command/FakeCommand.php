<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Bus\Command;

use Cyberclick\Shared\Domain\Bus\Command\Command;

final class FakeCommand implements Command
{
}
