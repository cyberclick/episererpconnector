<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Bus\Command;

use Cyberclick\Shared\Domain\Bus\Command\Command;
use Cyberclick\Shared\Infrastructure\Bus\Command\CommandNotRegisteredError;
use Cyberclick\Shared\Infrastructure\Bus\Command\InMemory\InMemorySymfonyCommandBus;
use Cyberclick\Tests\Shared\Infrastructure\Bus\FakeMiddlewareHandler;
use Cyberclick\Tests\Shared\Infrastructure\PhpUnit\UnitTestCase;
use Mockery\MockInterface;
use RuntimeException;

final class InMemorySymfonyCommandBusTest extends UnitTestCase
{
    private ?InMemorySymfonyCommandBus $commandBus;

    /** @test */
    public function it_should_be_able_to_handle_a_command(): void
    {
        $this->expectException(RuntimeException::class);

        $this->commandBus->dispatch(new FakeCommand());
    }

    /** @test */
    public function it_should_raise_an_exception_dispatching_a_non_registered_command(): void
    {
        $this->expectException(CommandNotRegisteredError::class);

        $this->commandBus->dispatch($this->command());
    }

    /** @return Command| MockInterface */
    private function command()
    {
        return $this->mock(Command::class);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->commandBus = new InMemorySymfonyCommandBus([$this->commandHandler()], new FakeMiddlewareHandler());
    }

    private function commandHandler(): object
    {
        return new class {
            public function __invoke(FakeCommand $command)
            {
                throw new RuntimeException('This works fine!');
            }
        };
    }
}
