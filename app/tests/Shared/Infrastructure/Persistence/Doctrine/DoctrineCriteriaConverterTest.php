<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Persistence\Doctrine;


use Cyberclick\Apps\Locator\Backend\LocatorBackendKernel;
use Cyberclick\Locator\Client\Domain\Client;
use Cyberclick\Shared\Domain\Criteria\AndFilter;
use Cyberclick\Shared\Domain\Criteria\Filters;
use Cyberclick\Shared\Domain\Criteria\OrFilter;
use Cyberclick\Shared\Infrastructure\Persistence\Doctrine\DoctrineCriteriaConverter;
use Cyberclick\Tests\Shared\Domain\Criteria\CriteriaMother;
use Cyberclick\Tests\Shared\Domain\Criteria\FilterMother;
use Cyberclick\Tests\Shared\Infrastructure\PhpUnit\InfrastructureTestCase;
use Doctrine\ORM\EntityManager;

final class DoctrineCriteriaConverterTest extends InfrastructureTestCase
{
    private EntityManager|null $entityManager;

    public function setUp(): void
    {
        parent::setUp();
        $this->entityManager = $this->service(EntityManager::class);
    }

    /** @test */
    public function it_should_convert_criteria_to_query(): void
    {
        $dql     = 'SELECT c FROM Cyberclick\Locator\Client\Domain\Client c WHERE c.maiores <> :maiores AND (c.ab < :ab OR (c.quasi = :quasi AND c.aut LIKE :aut))';
        $filter1 = FilterMother::fromValues([
            'field'    => 'maiores',
            'operator' => '!=',
            'value'    => 'maiores'
        ]);
        $filter2 = FilterMother::fromValues([
            'field'    => 'ab',
            'operator' => '<',
            'value'    => '5'
        ]);
        $filter3 = FilterMother::fromValues([
            'field'    => 'quasi',
            'operator' => '=',
            'value'    => '4'
        ]);
        $filter4 = FilterMother::fromValues([
            'field'    => 'aut',
            'operator' => 'CONTAINS',
            'value'    => 'aut'
        ]);

        $filters = new Filters(
            [
                $filter1,
                new OrFilter(
                    $filter2,
                    new AndFilter(
                        $filter3,
                        $filter4
                    )
                )
            ]
        );

        $criteria   = CriteriaMother::create($filters);
        $conversion = DoctrineCriteriaConverter::convert($criteria);
        $qb         = $this->entityManager->getRepository(Client::class)->createQueryBuilder('c')->addCriteria($conversion)->getQuery();

        self::assertEquals($dql, $qb->getDQL());
    }

    protected function kernelClass(): string
    {
        return LocatorBackendKernel::class;
    }
}
