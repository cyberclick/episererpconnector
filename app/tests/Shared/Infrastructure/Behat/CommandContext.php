<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Infrastructure\Behat;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Cyberclick\Apps\Locator\Backend\LocatorBackendKernel;
use Cyberclick\Shared\Domain\Utils;
use Cyberclick\Tests\Shared\Domain\FilePathMother;
use Exception;
use InvalidArgumentException;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Webmozart\Assert\Assert;
use function Lambdish\Phunctional\apply;

class CommandContext implements Context
{
    private Application $application;
    private array $commandParameters;
    private CommandTester $tester;
    private string $runCommand;
    private ?Exception $commandException;
    private int $exitCode;

    public function __construct(
        LocatorBackendKernel $kernel,
    )
    {
        $this->application           = new Application($kernel);
        $this->commandParameters     = [];
        $this->exitCode              = 0;
        $this->runCommand            = '';
    }


    /**
     * @Given I am the system
     */
    public function iAmTheSystem(): void
    {
        Assert::same('cli', PHP_SAPI);
    }

    /**
     * @Given /^I run a command "([^"]*)" with parameters:$/
     */
    public function iRunACommandWithParameters($command, PyStringNode $parameterJson): void
    {
        $this->commandParameters = Utils::jsonDecode($parameterJson->getRaw());

        if (count($this->commandParameters) === 0) {
            throw new InvalidArgumentException(
                "Parameters could not be converted to json."
            );
        }

        $this->iRunACommand($command);
    }

    /**
     * @Given /^I run a command "([^"]*)"$/
     */
    public function iRunACommand($command): void
    {
        $commandInstance = $this->getCommand($command);
        $this->tester    = new CommandTester($commandInstance);

        try {
            $this->exitCode         = $this
                ->tester
                ->execute(
                    $this->getCommandParams($command)
                );
            $this->commandException = null;
        } catch (Exception $exception) {
            $this->exitCode         = $exception->getCode();
            $this->commandException = $exception;
        }
        $this->runCommand        = $command;
        $this->commandParameters = [];
    }

    private function getCommand(string $command): Command
    {
        return $this->application->find($command);
    }

    private function getCommandParams(string $command): array
    {
        $default = [
            'command' => $command
        ];

        return array_merge(
            $this->commandParameters,
            $default
        );
    }

    /**
     * @Then /^The command exception "([^"]*)" should be thrown$/
     */
    public function theCommandExceptionShouldBeThrown($exceptionClass): void
    {
        $this->checkThatCommandHasRun();

        if ($this->commandException !== null) {
            Assert::eq(get_class($this->commandException), $exceptionClass);
        }
    }

    private function checkThatCommandHasRun(): bool
    {
        if ('' === $this->runCommand) {
            throw new LogicException(
                "You first need to run a command to check to use this step"
            );
        }

        return true;
    }

    /**
     * @Then /^The command exception should be empty$/
     */
    public function theCommandExceptionShouldBeEmpty(): void
    {
        $this->checkThatCommandHasRun();

        Assert::eq($this->commandException, null);
    }

    /**
     * @Then /^The command exception "([^"]*)" with message "([^"]*)" should be thrown$/
     */
    public function theCommandExceptionWithMessageShouldBeThrown($exceptionClass, $exceptionMessage): void
    {
        $this->checkThatCommandHasRun();

        if ($this->commandException !== null) {
            Assert::eq(get_class($this->commandException), $exceptionClass);
        }

        $mesage = $this->commandException->getMessage();

        Assert::regex($mesage, $exceptionMessage);
    }

    /**
     * @Then /^The command exit code should be (\d+)$/
     */
    public function theCommandExitCodeShouldBe($exitCode): void
    {
        $this->checkThatCommandHasRun();

        Assert::eq($exitCode, $this->exitCode);
    }

    /**
     * @Then /^I should see "([^"]*)" in the command output$/
     */
    public function iShouldSeeInTheCommandOutput($regexp): void
    {
        $this->checkThatCommandHasRun();

        Assert::regex($regexp, $this->tester->getDisplay());
    }
}

