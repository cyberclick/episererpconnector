<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class TimezoneMother
{
    public static function create(): string
    {
        return MotherCreator::random()->timezone;
    }
}
