<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class FilePathMother
{
    public static function create(): string
    {
        return implode('/', Repeater::random(fn() => WordMother::create())) . '.' . FileExtensionMother::random();
    }
}
