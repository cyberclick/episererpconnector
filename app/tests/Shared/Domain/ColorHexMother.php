<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class ColorHexMother
{
    public static function random(): string
    {
        return MotherCreator::random()->hexColor;
    }
}
