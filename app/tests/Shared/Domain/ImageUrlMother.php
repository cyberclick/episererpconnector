<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class ImageUrlMother
{
    public static function random(int $width = 800, int $height = 600): string
    {
        return MotherCreator::random()->imageUrl($width, $height);
    }
}
