<?php


namespace Cyberclick\Tests\Shared\Domain\ValueObject\Address;


use Cyberclick\Locator\Client\Domain\ClientSchedule\ClientScheduleInterval;
use Cyberclick\Shared\Domain\ValueObject\Address\AddressState;
use Cyberclick\Tests\Shared\Domain\RandomElementPicker;
use Cyberclick\Tests\Shared\Domain\WordMother;

class AddressStateMother
{
    public static function create(?string $value = null): AddressState
    {
        return new AddressState(
            $value ?? WordMother::create()
        );
    }
}
