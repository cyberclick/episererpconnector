<?php


namespace Cyberclick\Tests\Shared\Domain\ValueObject\Address;


use Cyberclick\Locator\Client\Domain\ClientSchedule\ClientScheduleInterval;
use Cyberclick\Shared\Domain\ValueObject\Address\AddressCity;
use Cyberclick\Tests\Shared\Domain\RandomElementPicker;
use Cyberclick\Tests\Shared\Domain\WordMother;

class AddressCityMother
{
    public static function create(?string $value = null): AddressCity
    {
        return new AddressCity(
            $value ?? WordMother::create()
        );
    }
}
