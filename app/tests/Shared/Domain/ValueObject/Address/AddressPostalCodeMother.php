<?php


namespace Cyberclick\Tests\Shared\Domain\ValueObject\Address;


use Cyberclick\Locator\Client\Domain\ClientSchedule\ClientScheduleInterval;
use Cyberclick\Shared\Domain\ValueObject\Address\AddressPostalCode;
use Cyberclick\Tests\Shared\Domain\MotherCreator;
use Cyberclick\Tests\Shared\Domain\RandomElementPicker;

class AddressPostalCodeMother
{
    public static function create(?string $value = null): AddressPostalCode
    {
        return new AddressPostalCode(
            $value ?? MotherCreator::random()->postcode
    );
    }
}
