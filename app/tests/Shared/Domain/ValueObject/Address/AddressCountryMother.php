<?php


namespace Cyberclick\Tests\Shared\Domain\ValueObject\Address;


use Cyberclick\Locator\Client\Domain\ClientSchedule\ClientScheduleInterval;
use Cyberclick\Shared\Domain\ValueObject\Address\AddressCountry;
use Cyberclick\Tests\Shared\Domain\RandomElementPicker;
use Cyberclick\Tests\Shared\Domain\WordMother;

class AddressCountryMother
{
    public static function create(?string $value = null): AddressCountry
    {
        return new AddressCountry(
            $value ?? WordMother::create()
        );
    }
}
