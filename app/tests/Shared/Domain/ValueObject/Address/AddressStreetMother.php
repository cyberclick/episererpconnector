<?php


namespace Cyberclick\Tests\Shared\Domain\ValueObject\Address;


use Cyberclick\Locator\Client\Domain\ClientSchedule\ClientScheduleInterval;
use Cyberclick\Shared\Domain\ValueObject\Address\AddressStreet;
use Cyberclick\Tests\Shared\Domain\RandomElementPicker;
use Cyberclick\Tests\Shared\Domain\WordMother;

class AddressStreetMother
{
    public static function create(?string $value = null): AddressStreet
    {
        return new AddressStreet(
            $value ?? WordMother::create());
    }
}
