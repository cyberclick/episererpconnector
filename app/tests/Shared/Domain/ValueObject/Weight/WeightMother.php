<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\ValueObject\Weight\Weight;
use Cyberclick\Shared\Domain\ValueObject\Weight\WeightMeasure;
use Cyberclick\Shared\Domain\ValueObject\Weight\WeightUnit;

final class WeightMother
{
    public static function create(
        ?WeightMeasure $measure = null,
        ?WeightUnit $unit = null
    ): Weight
    {
        return Weight::create(
            $measure->value() ?? WeightMeasureMother::create()->value(),
            $unit ?? WeightUnitMother::create(),
        );
    }
}
