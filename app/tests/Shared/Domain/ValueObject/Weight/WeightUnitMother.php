<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\ValueObject\Weight\WeightUnit;
use Cyberclick\Tests\Shared\Domain\RandomElementPicker;

final class WeightUnitMother
{
    public static function create(?string $value = null): WeightUnit
    {
        return new WeightUnit($value ?? RandomElementPicker::from(... WeightUnit::SUPPORTED_UNITS));
    }
}
