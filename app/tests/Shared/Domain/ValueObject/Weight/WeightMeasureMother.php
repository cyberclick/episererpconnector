<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Weight;

use Cyberclick\Shared\Domain\ValueObject\Weight\WeightMeasure;
use Cyberclick\Tests\Shared\Domain\IntegerMother;

final class WeightMeasureMother
{
    public static function create(?int $value = null): WeightMeasure
    {
        return new WeightMeasure($value ?? IntegerMother::between(0, 100));
    }
}
