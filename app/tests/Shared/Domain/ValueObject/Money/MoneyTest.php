<?php
declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Money;

use Cyberclick\Shared\Domain\ValueObject\Money\Money;
use Cyberclick\Tests\Shared\Domain\Money\MoneyMother;
use PHPUnit\Framework\TestCase;

class MoneyTest extends TestCase
{
    /** @test */
    public function copied_money_should_represent_same_value(): void
    {
        $money       = MoneyMother::create();
        $copiedMoney = Money::fromMoney($money);
        self::assertTrue($money->equals($copiedMoney));
    }

    /** @test */
    public function good_string_should_return_money(): void
    {
        $baseMoney   = MoneyMother::create();
        $moneyString = implode(' ', [(string)$baseMoney->floatAmount(), $baseMoney->currency()->value()]);

        $money = Money::fromString($moneyString);

        self::assertTrue($money->equals($baseMoney));
    }

    /** @test */
    public function good_joined_string_should_return_money(): void
    {
        $baseMoney   = MoneyMother::create();
        $moneyString = implode('', [(string)$baseMoney->floatAmount(), $baseMoney->currency()->value()]);

        $money = Money::fromString($moneyString);

        self::assertTrue($money->equals($baseMoney));
    }

    /** @test */
    public function good_alternative_string_should_return_money(): void
    {
        $baseMoney   = MoneyMother::create();
        $moneyString = implode(' ', [$baseMoney->currency()->value(), (string)$baseMoney->floatAmount()]);

        $money = Money::fromString($moneyString);

        self::assertTrue($money->equals($baseMoney));
    }

    /** @test */
    public function good_joined_alternative_string_should_return_money(): void
    {
        $baseMoney   = MoneyMother::create();
        $moneyString = implode('', [$baseMoney->currency()->value(), (string)$baseMoney->floatAmount()]);

        $money = Money::fromString($moneyString);

        self::assertTrue($money->equals($baseMoney));
    }

    /** @test */
    public function should_create_a_valid_money_with_commas(): void
    {
        $baseMoney   = MoneyMother::create(1999);
        $value       = '1,999.00';
        $moneyString = implode(' ', [$value, $baseMoney->currency()->value()]);

        $money = Money::fromString($moneyString);

        self::assertTrue($money->equals($baseMoney));
    }
}
