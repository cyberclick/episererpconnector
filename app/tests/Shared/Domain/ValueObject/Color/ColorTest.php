<?php
declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Color;

use Cyberclick\Shared\Domain\ValueObject\Color\Color;
use Cyberclick\Shared\Domain\ValueObject\Color\ColorMaxValueOvercome;
use Cyberclick\Shared\Domain\ValueObject\Color\ColorMinValueOvercome;
use Cyberclick\Shared\Domain\ValueObject\Color\ColorValueObject;
use Cyberclick\Tests\Shared\Domain\ColorHexMother;
use Cyberclick\Tests\Shared\Domain\ColorRgbMother;
use Cyberclick\Tests\Shared\Domain\IntegerMother;
use PHPUnit\Framework\TestCase;

class ColorTest extends TestCase
{
    /** @test */
    public function it_should_create_a_valid_color_from_values(): void
    {
        [$red, $green, $blue] = ColorRgbMother::random();
        $color = Color::fromRGB([
            'red'   => $red,
            'green' => $green,
            'blue'  => $blue
        ]);
        self::assertEquals($red, $color->red()->value());
        self::assertEquals($blue, $color->blue()->value());
        self::assertEquals($green, $color->green()->value());
    }

    /** @test */
    public function it_should_create_a_valid_color_from_hex(): void
    {
        $hexColor = ColorHexMother::random();
        $color    = Color::fromHex($hexColor);
        self::assertEquals($hexColor, $color->toHex());
    }

    /** @test */
    public function it_should_fail_if_color_max_overcomed(): void
    {
        $values = [
            'red'   => IntegerMother::moreThan(ColorValueObject::MAX_COLOR_VALUE),
            'green' => IntegerMother::between(ColorValueObject::MIN_COLOR_VALUE, ColorValueObject::MAX_COLOR_VALUE),
            'blue'  => IntegerMother::between(ColorValueObject::MIN_COLOR_VALUE, ColorValueObject::MAX_COLOR_VALUE),
        ];

        $this->expectException(ColorMaxValueOvercome::class);
        Color::fromRGB($values);
    }

    /** @test */
    public function it_should_fail_if_color_min_overcomed(): void
    {
        $values = [
            'red'   => ColorValueObject::MIN_COLOR_VALUE - 1,
            'green' => IntegerMother::between(ColorValueObject::MIN_COLOR_VALUE, ColorValueObject::MAX_COLOR_VALUE),
            'blue'  => IntegerMother::between(ColorValueObject::MIN_COLOR_VALUE, ColorValueObject::MAX_COLOR_VALUE),
        ];

        $this->expectException(ColorMinValueOvercome::class);
        Color::fromRGB($values);
    }
}
