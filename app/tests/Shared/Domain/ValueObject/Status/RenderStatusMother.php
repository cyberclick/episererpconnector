<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Status;


use Cyberclick\Tests\Shared\Domain\RandomElementPicker;

final class RenderStatusMother
{
    private const PUBLISHED   = 1;
    private const UNPUBLISHED = 0;
    private const DELETED     = 2;
    private const DRAFT       = 3;


    public static function create(?int $value = null): int
    {
        return $value ?? RandomElementPicker::from(... [self::PUBLISHED(), self::DELETED(), self::UNPUBLISHED(), self::DRAFT()]);
    }

    public static function PUBLISHED(): int
    {
        return self::PUBLISHED;
    }

    public static function UNPUBLISHED(): int
    {
        return self::UNPUBLISHED;
    }

    public static function DELETED(): int
    {
        return self::DELETED;
    }

    public static function DRAFT(): int
    {
        return self::DRAFT;
    }
}
