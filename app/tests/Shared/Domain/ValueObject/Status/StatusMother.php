<?php
declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\ValueObject\Status;

use Cyberclick\Tests\Shared\Domain\RandomElementPicker;

final class StatusMother
{
    private const ACTIVE   = 1;
    private const DISABLED = 0;
    private const DELETED  = 2;

    public static function create(?int $value = null): int
    {
        return $value ?? RandomElementPicker::from(... [self::ACTIVE(), self::DELETED(), self::DISABLED()]);
    }

    public static function ACTIVE(): int
    {
        return self::ACTIVE;
    }

    public static function DELETED(): int
    {
        return self::DELETED;
    }

    public static function DISABLED(): int
    {
        return self::DISABLED;
    }
}
