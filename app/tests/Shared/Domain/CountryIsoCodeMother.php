<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class CountryIsoCodeMother
{
    public static function random(): string
    {
        return MotherCreator::random()->countryCode;
    }
}
