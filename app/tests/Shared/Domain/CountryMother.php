<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class CountryMother
{
    public static function randomLocale(): string
    {
        return MotherCreator::random()->locale;
    }
}
