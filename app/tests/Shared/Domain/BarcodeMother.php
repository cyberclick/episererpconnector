<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class BarcodeMother
{
    public static function randomSku(): string
    {
        return MotherCreator::random()->ean8;
    }

    public static function randomGtin(): string
    {
        return MotherCreator::random()->ean13;
    }

    public static function randomEan(): string
    {
        return MotherCreator::random()->ean13;
    }
}
