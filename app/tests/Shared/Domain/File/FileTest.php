<?php
declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\File;

use Cyberclick\Shared\Domain\File\File;
use Cyberclick\Tests\Shared\Domain\FilePathMother;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    /** @test */
    public function it_should_create_a_valid_file(): void
    {
        $path = FilePathMother::create();
        $file = new File($path);

        self::assertEquals($file->filename(), pathinfo($path, PATHINFO_FILENAME));
        self::assertEquals($file->extension()->value(), pathinfo($path, PATHINFO_EXTENSION));
        self::assertEquals($file->path(), $path);
        self::assertEquals($file->dirname(), pathinfo($path, PATHINFO_DIRNAME));
    }
}
