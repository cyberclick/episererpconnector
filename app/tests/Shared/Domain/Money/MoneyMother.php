<?php
declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Money;


use Cyberclick\Shared\Domain\ValueObject\Money\Currency;
use Cyberclick\Shared\Domain\ValueObject\Money\Money;
use Cyberclick\Tests\Shared\Domain\IntegerMother;

final class MoneyMother
{
    private const MIN = 0;
    private const MAX = 2000;

    public static function create(
        ?float $amount = null,
        ?Currency $currency = null
    ): Money
    {
        return Money::create(
            $amount ?? (float)IntegerMother::between(self::MIN, self::MAX),
            $currency ?? CurrencyMother::create(),
        );
    }
}
