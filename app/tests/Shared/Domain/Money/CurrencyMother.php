<?php
declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Money;


use Cyberclick\Shared\Domain\ValueObject\Money\Currency;
use Cyberclick\Tests\Shared\Domain\CurrencyCodeMother;

final class CurrencyMother
{
    public static function create(?string $value = null): Currency
    {
        return new Currency($value ?? CurrencyCodeMother::create());
    }
}
