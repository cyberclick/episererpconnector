<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class UrlMother
{
    public static function create(): string
    {
        return MotherCreator::random()->url;
    }
    public static function domain(): string
    {
        return MotherCreator::random()->domainName;
    }
}
