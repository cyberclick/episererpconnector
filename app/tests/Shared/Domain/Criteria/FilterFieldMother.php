<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Criteria\FilterField;
use Cyberclick\Tests\Shared\Domain\WordMother;

final class FilterFieldMother
{
    public static function create(?string $fieldName = null): FilterField
    {
        return new FilterField($fieldName ?? WordMother::create());
    }
}
