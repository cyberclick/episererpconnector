<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Criteria\Filter;
use Cyberclick\Shared\Domain\Criteria\FilterField;
use Cyberclick\Shared\Domain\Criteria\FilterOperator;
use Cyberclick\Shared\Domain\Criteria\FilterValue;

final class FilterMother
{
    public static function create(
        ?FilterField $field = null,
        ?FilterOperator $operator = null,
        ?FilterValue $value = null
    ): Filter
    {
        return new Filter(
            $field ?? FilterFieldMother::create(),
            $operator ?? FilterOperator::random(),
            $value ?? FilterValueMother::create()
        );
    }

    /** @param string[] $values */
    public static function fromValues(array $values): Filter
    {
        return Filter::fromValues($values);
    }

    public static function toPrimitives(): array
    {
        $filter = self::create();
        return [
            'field'    => $filter->field()->value(),
            'operator' => $filter->operator()->value(),
            'value'    => $filter->value()->value()
        ];
    }
}
