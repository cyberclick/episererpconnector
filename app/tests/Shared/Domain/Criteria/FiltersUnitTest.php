<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;


use Cyberclick\Shared\Domain\Criteria\AndFilter;
use Cyberclick\Shared\Domain\Criteria\Filter;
use Cyberclick\Shared\Domain\Criteria\Filters;
use Cyberclick\Shared\Domain\Criteria\OrFilter;
use PHPUnit\Framework\TestCase;

final class FiltersUnitTest extends TestCase
{

    /** @test */
    public function it_should_generate_criteria_from_values(): void
    {
        $filter1 = FilterMother::toPrimitives();
        $filter2 = FilterMother::toPrimitives();
        $filter3 = FilterMother::toPrimitives();
        $filter4 = FilterMother::toPrimitives();

        $andFilter = [
            'left' => $filter3,
            'right' => $filter4,
            'type' => AndFilter::type()
        ];

        $orFilter = [
            'left'  => $filter2,
            'right' => $andFilter,
            'type'  => OrFilter::type()
        ];

        $arrayValues = [
            $filter1,
            $orFilter
        ];

        $filters         = Filters::fromValues($arrayValues);
        $filtersExpected = new Filters(
            [
                Filter::fromValues($filter1),
                new OrFilter(
                    Filter::fromValues($filter2),
                    new AndFilter(
                        Filter::fromValues($filter3),
                        Filter::fromValues($filter4)
                    )
                )
            ]
        );

        self::assertEquals($filters, $filtersExpected);
    }
}
