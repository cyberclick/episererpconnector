<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Criteria\Criteria;
use Cyberclick\Shared\Domain\Criteria\Filters;
use Cyberclick\Shared\Domain\Criteria\Order;

final class CriteriaMother
{
    public static function empty(): Criteria
    {
        return self::create(FiltersMother::blank(), OrderMother::none());
    }

    public static function create(
        Filters $filters,
        Order $order = null,
        int $offset = null,
        int $limit = null
    ): Criteria
    {
        return new Criteria($filters, $order ?: OrderMother::none(), $offset, $limit);
    }
}
