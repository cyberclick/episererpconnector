<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Criteria\Filter;
use Cyberclick\Shared\Domain\Criteria\Filters;

final class FiltersMother
{
    public static function createOne(Filter $filter): Filters
    {
        return self::create([$filter]);
    }

    /** @param Filter[] $filters */
    public static function create(array $filters): Filters
    {
        return new Filters($filters);
    }

    public static function blank(): Filters
    {
        return self::create([]);
    }
}
