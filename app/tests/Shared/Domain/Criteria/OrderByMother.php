<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Criteria\OrderBy;
use Cyberclick\Tests\Shared\Domain\WordMother;

final class OrderByMother
{
    public static function create(?string $fieldName = null): OrderBy
    {
        return new OrderBy($fieldName ?? WordMother::create());
    }
}
