<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain\Criteria;

use Cyberclick\Shared\Domain\Criteria\Order;
use Cyberclick\Shared\Domain\Criteria\OrderBy;
use Cyberclick\Shared\Domain\Criteria\OrderType;

final class OrderMother
{
    public static function create(?OrderBy $orderBy = null, ?OrderType $orderType = null): Order
    {
        return new Order($orderBy ?? OrderByMother::create(), $orderType ?? OrderType::random());
    }

    public static function none(): Order
    {
        return Order::none();
    }
}
