<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class FileExtensionMother
{
    public static function random(): string
    {
        return MotherCreator::random()->fileExtension;
    }
}
