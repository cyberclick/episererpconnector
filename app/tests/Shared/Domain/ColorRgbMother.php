<?php

declare(strict_types=1);

namespace Cyberclick\Tests\Shared\Domain;

final class ColorRgbMother
{
    public static function random(): array
    {
        return MotherCreator::random()->rgbColorAsArray;
    }
}
